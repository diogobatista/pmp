﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ChildMaster.master" CodeBehind="Proposals.aspx.vb" Inherits="pmp_V3.WebForm9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ul class="nav" id="side-menu">
        <li class="sidebar-search">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            <!-- /input-group -->
        </li>
        <li role="presentation" class="divider"></li>
        <li><a href="#">Criar</a></li>
        <li><a href="#">...</a></li>
        <li><a href="#">...</a></li>
        <li><a href="#">...</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Propostas</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <small>
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table " DataSourceID="srcProposalsList" GridLines="None" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Id" SortExpression="Id">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Id") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>

                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "ProposalDetails.aspx?PI=" & Eval("Id") & "&OI=" & Eval("OpportunityId") %>' Text='<%# Eval("TxtProposalId") %>'></asp:HyperLink>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CustomerName" HeaderText="Cliente" SortExpression="CustomerName" />
                                    <asp:BoundField DataField="TxtEntityName" HeaderText="Entidade" SortExpression="TxtEntityName" />
                                    <asp:BoundField DataField="TxtPropTypeName" HeaderText="Tipo" SortExpression="TxtPropTypeName" />
                                    <asp:BoundField DataField="ProposalName" HeaderText="Descrição" SortExpression="ProposalName" />
                                    <asp:BoundField DataField="TxtPropStatusName" HeaderText="Estado" SortExpression="TxtPropStatusName" />
                                    <asp:BoundField DataField="TxtCreateDate" HeaderText="Data" ReadOnly="True" SortExpression="TxtCreateDate" />
                                    <asp:BoundField DataField="ProposalEndDate" HeaderText="Validade" SortExpression="ProposalEndDate" />
                                </Columns>
                            </asp:GridView>
                            <telerik:RadGrid ID="GdOpportunitiesList" CssClass="table table-striped table-bordered table-hover" Visible="false" runat="server" AutoGenerateColumns="False" DataSourceID="srcProposalsList" EnableEmbeddedSkins="False" Width="100%" EnableAjaxSkinRendering="False" EnableEmbeddedBaseStylesheet="False" EnableEmbeddedScripts="False" EnableTheming="True">
                                <ClientSettings>
                                    <Scrolling AllowScroll="True" ScrollHeight="" />
                                    <Resizing AllowResizeToFit="True" />
                                </ClientSettings>
                                <ExportSettings>
                                    <Pdf PageWidth="">
                                    </Pdf>
                                </ExportSettings>
                                <MasterTableView AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="srcProposalsList">
                                    <Columns>
                                        <telerik:GridTemplateColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter Id column" HeaderText="Id" SortExpression="Id" UniqueName="Id">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="IdTextBox" runat="server" Text='<%# Bind("Id") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "ProposalDetails.aspx?PI=" & Eval("Id") & "&OI=" & Eval("OpportunityId")%>' Text='<%# Eval("Id") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="TenantId" DataType="System.Int32" FilterControlAltText="Filter TenantId column" HeaderText="TenantId" SortExpression="TenantId" UniqueName="TenantId" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtEntityName" FilterControlAltText="Filter TxtEntityName column" HeaderText="Entidade" SortExpression="TxtEntityName" UniqueName="TxtEntityName" ItemStyle-Wrap="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtPropTypeName" FilterControlAltText="Filter TxtPropTypeName column" HeaderText="Tipo" SortExpression="TxtPropTypeName" UniqueName="TxtPropTypeName">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="OpportunityId" DataType="System.Int32" FilterControlAltText="Filter OpportunityId column" HeaderText="OpportunityId" SortExpression="OpportunityId" UniqueName="OpportunityId" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtOpportunityName" FilterControlAltText="Filter TxtOpportunityName column" HeaderText="Oportunidade" SortExpression="TxtOpportunityName" UniqueName="TxtOpportunityName">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn DataField="TxtProposalName" FilterControlAltText="Filter TxtProposalName column" HeaderText="Proposta" SortExpression="TxtProposalName" UniqueName="TxtProposalName">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtUserSubmit" FilterControlAltText="Filter TxtUserSubmit column" HeaderText="Submeteu" SortExpression="TxtUserSubmit" UniqueName="TxtUserSubmit" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtPropStatusName" FilterControlAltText="Filter TxtPropStatusName column" HeaderText="Estado" SortExpression="TxtPropStatusName" UniqueName="TxtPropStatusName">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ProposalEndDate" DataType="System.Int32" FilterControlAltText="Filter ProposalEndDate column" HeaderText="Data Fim" SortExpression="ProposalEndDate" UniqueName="ProposalEndDate" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtOwnerUserName" FilterControlAltText="Filter TxtOwnerUserName column" HeaderText="Owner" SortExpression="TxtOwnerUserName" UniqueName="TxtOwnerUserName" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtMotiveName" FilterControlAltText="Filter TxtMotiveName column" HeaderText="Motivo" SortExpression="TxtMotiveName" UniqueName="TxtMotiveName" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Comments" FilterControlAltText="Filter Comments column" HeaderText="Comentario" SortExpression="Comments" UniqueName="Comments" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtCreateDate" FilterControlAltText="Filter TxtCreateDate column" HeaderText="TxtCreateDate" ReadOnly="True" SortExpression="TxtCreateDate" UniqueName="TxtCreateDate" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtModifiedDate" FilterControlAltText="Filter TxtModifiedDate column" HeaderText="TxtModifiedDate" ReadOnly="True" SortExpression="TxtModifiedDate" UniqueName="TxtModifiedDate" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" UniqueName="TemplateColumn">
                                        </telerik:GridTemplateColumn>
                                    </Columns>

                                    <EditFormSettings>
                                        <EditColumn InsertImageUrl="Update.gif" UpdateImageUrl="Update.gif" CancelImageUrl="Cancel.gif"></EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>

                                <FilterMenu EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" EnableEmbeddedBaseStylesheet="False"></FilterMenu>

                                <HeaderContextMenu EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" EnableEmbeddedBaseStylesheet="False"></HeaderContextMenu>
                            </telerik:RadGrid>

                            <asp:SqlDataSource ID="srcProposalsList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [VwProposals] WHERE ([TenantId] = @TenantId) AND (StatusId&lt;&gt; 9) Order By Id Desc">
                                <SelectParameters>
                                    <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </small>

                    </div>
                </div>
            </div>
        </div>
    </div>



</asp:Content>
