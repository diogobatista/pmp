﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Telerik.Web.UI
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

Public Class WebForm11
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim strQuerystring As String = Request.QueryString("OI")
            Dim strFormOpportunity As FormView = Me.FrmOpportunity

            If strQuerystring = 0 Then

                strFormOpportunity.DefaultMode = FormViewMode.Insert

                'LoadCustomerTypeBox()

                Me.LiAddProduct.Visible = False
                Me.LiAddProposal.Visible = False
                Me.OppProductsPanel.Visible = False


            Else

                strFormOpportunity.DefaultMode = FormViewMode.ReadOnly

                Me.LiAddProposal.Visible = True
                Dim sqlProducts As String = "SELECT COUNT(Id) as OppCountProd FROM OpportunityDetails WHERE OpportunityId=" & strQuerystring & " AND TenantId=" & Getdata.TenantId

                If Getdata.retrieve_sql_field_value(sqlProducts, "OppCountProd") = 0 Then

                    Me.LiAddProduct.Visible = True
                    Me.LiAddProposal.Visible = False
                    Me.OppProductsPanel.Visible = False
                    GdOppProducts.MasterTableView.IsItemInserted = True
                    GdOppProducts.Rebind()

                Else

                    Me.LiAddProduct.Visible = False

                    'Me.OppProductsPanel.Visible = True
                    'GdOppProducts.MasterTableView.IsItemInserted = True
                    'GdOppProducts.Rebind()


                End If

                Dim sqlOpp As String = "SELECT * FROM Opportunities WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strQuerystring

                Dim strBlock As Boolean = Getdata.retrieve_sql_field_value(sqlOpp, "IsEditable")


                If strBlock = False Then

                    Dim strBtnEdit As LinkButton = DirectCast(FrmOpportunity.FindControl("BtnEdit"), LinkButton)

                    strBtnEdit.Visible = False



                End If

            End If

            Me.AlertDiv.Visible = False
            Me.AlertDivProd.Visible = False


        End If
    End Sub



    '################################################################ ALERTS AND HIGHLIGHT FIELDS 
    Private Sub ShowAlertPanelError(strType As String, strDivId As Object, LbErrorAlert As Label) ' Show Alert Panel Error

        strDivId.Visible = True
        'Me.AlertDatePicker.Visible = False


        strDivId.Attributes("Class") = "alert alert-danger alert-dismissable"

        LbErrorAlert.Text = strType

    End Sub
    Private Sub ShowAlertPanelValid(strType As String)  ' Show Alert Panel Valid

        Me.AlertDiv.Visible = True

        Me.AlertDiv.Attributes("Class") = "alert alert-success alert-dismissable"

        Me.LbAlert.Text = strType


    End Sub
    Private Sub ShowAlertPanelWarning(strType As String)  ' Show Alert Panel Valid

        Me.AlertDiv.Visible = True

        Me.AlertDiv.Attributes("Class") = "alert alert-warning alert-dismissable"


        Select Case strType

            Case 6
                Me.LbAlert.Text = "Por favor indique a data em que o contrato foi assinado e tente novamente"
                'Me.DpDate.Visible = True


            Case 7
                Me.LbAlert.Text = "Por favor indique a data em que o contrato foi recusado"
                'Me.DpDate.Visible = True
                'Me.TxtMotive.Visible = True



            Case 4




        End Select





    End Sub
    Private Sub HighlightFieldError(strField As Object, strbol As Boolean)


        If strbol = True Then

            strField.BorderColor = Drawing.Color.Green
            strField.BorderWidth = Unit.Pixel(1)

        Else

            strField.BorderColor = Drawing.Color.Red
            strField.BorderWidth = Unit.Pixel(2)

        End If


    End Sub

    '################################################################ VALIDATIONS 

    Private Function ValidateFrmOpportunity() As Boolean

        ' ################# FIELD Validator
        Dim strField As TextBox

        ValidateFrmOpportunity = True


        '################  Contest Description validator 
        strField = CType(FrmOpportunity.FindControl("TxtDescription"), TextBox)

        If Len(strField.Text) > 0 Then

            HighlightFieldError(strField, True)


        Else

            ShowAlertPanelError("O campo descrição não se encontra preenchido. Por favor tente novamente.", AlertDiv, LbAlert)
            ValidateFrmOpportunity = False
            HighlightFieldError(strField, False)

            Exit Function


        End If

        '################  Contest Description validator 
        strField = CType(FrmOpportunity.FindControl("TxtPublicCode"), TextBox)

        If Len(strField.Text) > 0 Then

            HighlightFieldError(strField, True)


        Else

            ShowAlertPanelError("O campo Código não se encontra preenchido. Por favor tente novamente.", AlertDiv, LbAlert)
            ValidateFrmOpportunity = False
            HighlightFieldError(strField, False)

            Exit Function


        End If

        '################# Contest End Date Picker
        'Dim strDateField As RadDatePicker

        'strDateField = CType(FrmOpportunity.FindControl("RadDatePicker1"), RadDatePicker)

        'If Len(strDateField.DbSelectedDate) > 0 Then

        '    HighlightFieldError(strField, True)


        'Else

        '    ShowAlertPanelError("A Data de Limite de apresentação da proposta não foi preenchida, por favor tente novamente", AlertDiv, LbAlert)
        '    ValidateFrmOpportunity = False
        '    HighlightFieldError(strField, False)
        '    Exit Function

        'End If






    End Function

    Private Function OppProdValidateInsert(strGrid As GridEditableItem
                                           ) As Boolean

        OppProdValidateInsert = True


        Dim strFieldName As TextBox

        strFieldName = DirectCast(strGrid.FindControl("PositionTextBox"), TextBox)
        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("A Posição do produto não foi preenchida por favor tente novamente.", AlertDivProd, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            OppProdValidateInsert = False
            Exit Function


        Else

            Dim SqlSelect As String = "SELECT Position FROM OpportunityDetails WHERE TenantId=" & Getdata.TenantId & " AND OpportunityId=" & Request.QueryString("OI") & "AND Position=" & strFieldName.Text

            Dim strPos As String = Getdata.retrieve_sql_field_value(SqlSelect, "Position")

            If strPos = "?" Then

                HighlightFieldError(strFieldName, True)

            Else

                ShowAlertPanelError("Posição já existente!", AlertDivProd, LbProdAlert)
                HighlightFieldError(strFieldName, False)
                OppProdValidateInsert = False
                Exit Function


            End If



        End If


        strFieldName = DirectCast(strGrid.FindControl("ExternalCode"), TextBox)


        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("O Codigo do produto não foi preenchido por favor tente novamente.", AlertDivProd, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            OppProdValidateInsert = False
            Exit Function
        Else
            HighlightFieldError(strFieldName, True)

        End If

        'strFieldName = DirectCast(strGrid.FindControl("TxtDescription"), TextBox)


        'If Len(strFieldName.Text) = 0 Then

        '    ShowAlertPanelError("A Descrição do produto não foi preenchido por favor tente novamente.", AlertDivProd, LbProdAlert)
        '    HighlightFieldError(strFieldName, False)
        '    OppProdValidateInsert = False
        '    Exit Function
        'Else
        '    HighlightFieldError(strFieldName, True)
        'End If

        Dim strComboBox As DropDownList
        strComboBox = DirectCast(strGrid.FindControl("CbProducts"), DropDownList)
        strFieldName = DirectCast(strGrid.FindControl("TxtDescription"), TextBox)

        If strComboBox.SelectedValue = "" Then

            ShowAlertPanelError("Selecione pelo menos um produto.", AlertDivProd, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            OppProdValidateInsert = False
            Exit Function

        Else

            HighlightFieldError(strComboBox, True)

        End If

        strFieldName = DirectCast(strGrid.FindControl("QuantityTextBox"), TextBox)


        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("A Quantidade do produto não foi preenchida por favor tente novamente.", AlertDivProd, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            OppProdValidateInsert = False
            Exit Function
        Else
            HighlightFieldError(strFieldName, True)

        End If




    End Function

    '################################################################ LEFT NAVBAR
    Protected Sub OppAddProducts_Click(sender As Object, e As EventArgs) Handles OppAddProducts.Click
        Me.OppProductsPanel.Visible = True

    End Sub

    Protected Sub OppAddProposal_Click(sender As Object, e As EventArgs) Handles OppAddProposal.Click

        Dim strOpportunityId As String = Request.QueryString("OI")

        Response.Redirect("ProposalDetails.aspx?PI=0" & "&OI=" & strOpportunityId)


    End Sub

    '################################################################ OPPORTUNITY FORM 

    Protected Sub FrmOpportunity_ItemInserting(sender As Object, e As FormViewInsertEventArgs) Handles FrmOpportunity.ItemInserting

        If ValidateFrmOpportunity() = False Then


            e.Cancel = True

        Else

        End If

    End Sub

    Protected Sub FrmOpportunity_DataBound(sender As Object, e As EventArgs) Handles FrmOpportunity.DataBound
        Select Case FrmOpportunity.CurrentMode
            Case FormViewMode.Edit
                Dim dr = DirectCast(FrmOpportunity.DataItem, DataRowView)
                Dim strTxtPublicDate As TextBox = DirectCast(FrmOpportunity.FindControl("TxtEditPublicDate"), TextBox)
                Dim strdate As Date = strTxtPublicDate.Text

                strTxtPublicDate.Text = strdate.ToString("yyyy-MM-dd")


                Dim strTxtEndDate As TextBox = DirectCast(FrmOpportunity.FindControl("TxtEndDate"), TextBox)
                Dim strEndDate As Date = strTxtEndDate.Text

                strTxtEndDate.Text = strEndDate.ToString("yyyy-MM-dd")


            Case FormViewMode.Insert






        End Select
    End Sub

    Protected Sub FrmOpportunity_ItemUpdating(sender As Object, e As FormViewUpdateEventArgs) Handles FrmOpportunity.ItemUpdating

        If ValidateFrmOpportunity() = False Then


            e.Cancel = True

        Else

        End If




    End Sub

    Protected Sub FrmOpportunity_ItemCommand(sender As Object, e As FormViewCommandEventArgs) Handles FrmOpportunity.ItemCommand

        If e.CommandName = "Edit" Then

            'FormViewMode.Edit = True

            FrmOpportunity.DefaultMode = FormViewMode.Edit

        End If

        If e.CommandName = "Cancel" Then

            'FormViewMode.Edit = True

            FrmOpportunity.DefaultMode = FormViewMode.ReadOnly



        End If




    End Sub

    Protected Sub srcOpportunityDetails_Updated(sender As Object, e As SqlDataSourceStatusEventArgs) Handles srcOpportunityDetails.Updated


        FrmOpportunity.DefaultMode = FormViewMode.ReadOnly


    End Sub

    Protected Sub srcOpportunityDetails_Inserted(sender As Object, e As SqlDataSourceStatusEventArgs) Handles srcOpportunityDetails.Inserted


        'Dim strOppId As String = Request.QueryString("OI")


        Dim sqlSelect As String = "SELECT TOP 1 * FROM Opportunities Where TenantId=" & Getdata.TenantId & " AND CreateUserId=" & Getdata.UserId & " ORDER BY 1 DESC"

        Dim strOppId As String = Getdata.retrieve_sql_field_value(sqlSelect, "Id")

        Response.Redirect("OpportunityDetails.aspx?OI=" & strOppId)


    End Sub

    Protected Sub srcOpportunityDetails_Inserting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles srcOpportunityDetails.Inserting

        Dim ddl As DropDownList = DirectCast(FrmOpportunity.FindControl("CbCustomer"), DropDownList)
        Dim strRadioButton As RadioButtonList = DirectCast(FrmOpportunity.FindControl("OptionCustomerSelection"), RadioButtonList)
        Dim strTotalValue As TextBox = DirectCast(FrmOpportunity.FindControl("TxtTotalValue"), TextBox)

        If strRadioButton.SelectedValue = 0 Then

            'e.Command.Parameters("@CustomerId").Value = Nothing
            e.Command.Parameters("@CustomerGroupId").Value = ddl.SelectedValue


        End If

        If strRadioButton.SelectedValue = 1 Then

            e.Command.Parameters("@CustomerId").Value = ddl.SelectedValue.ToString()
            'e.Command.Parameters("@CustomerGroupId").Value = Nothing


        End If

        If Len(strTotalValue.Text) = 0 Then

            e.Command.Parameters("@TotalValue").Value = 0

        Else
            e.Command.Parameters("@TotalValue").Value = strTotalValue.Text

        End If




    End Sub

    '######## FORM SUBS
    Protected Sub CbCustomer_Init(sender As Object, e As EventArgs)
        Dim SqlCustomers As String = "SELECT * FROM Customers WHERE TenantId=" & Getdata.TenantId & "ORDER BY Name"
        Dim ddl As DropDownList = DirectCast(FrmOpportunity.FindControl("CbCustomer"), DropDownList)

        ddl.DataSource = ""

        ddl.DataSource = Getdata.retrive_tablelist(SqlCustomers)

        ddl.DataTextField = "Name"
        ddl.DataValueField = "Id"
        'ddl.SelectedValue = "Id"
        ddl.DataBind()
    End Sub
    Protected Sub OptionCustomerSelection_SelectedIndexChanged(sender As Object, e As EventArgs)


        LoadCustomerTypeBox()



    End Sub


    '################################################################ OPPORTUNITY PRODUCTS
    Protected Sub GdOppProducts_InsertCommand(sender As Object, e As GridCommandEventArgs) Handles GdOppProducts.InsertCommand

        Dim item As GridEditableItem = DirectCast(e.Item, GridEditableItem)


        If OppProdValidateInsert(item) = False Then

            e.Canceled = True


        Else

            Me.AlertDivProd.Visible = False

            Dim strOppId As String = Request.QueryString("OI")
            Dim strPosition As String = CType(item.FindControl("PositionTextBox"), TextBox).Text
            Dim strCode As String = CType(item.FindControl("ExternalCode"), TextBox).Text


            'Dim strDescription As String = CType(item.FindControl("TxtDescription"), TextBox).Text


            Dim strProductId As String = CType(item.FindControl("CbProducts"), DropDownList).SelectedValue


            'Dim strUnitType As String = CType(item.FindControl("CbProductTypes"), DropDownList).SelectedValue
            Dim strQuantity As String = CType(item.FindControl("QuantityTextBox"), TextBox).Text


            Dim sqlInsert As String = "INSERT INTO OpportunityDetails (OpportunityId, TenantId, Position,ExternalCode,ProductId, Quantity,  CreateDate, CreateUserId, ModifiedDate, ModifiedUserId) VALUES " & _
                "(" & strOppId & "," & Getdata.TenantId & "," & strPosition & ",'" & strCode & "'," & strProductId & "," & strQuantity & ", GETDATE()," & Getdata.UserId & ", GETDATE(), " & Getdata.UserId & ")"

            Getdata.run_sql_query(sqlInsert)

            srcOppProducts.DataBind()
            GdOppProducts.DataBind()



        End If




    End Sub

    Protected Sub BtnProdAdd_Click(sender As Object, e As EventArgs)

        GdOppProducts.MasterTableView.IsItemInserted = True
        ' Check for the !IsPostBack also for initial page load
        srcOppProducts.DataBind()
        GdOppProducts.DataBind()
        GdOppProducts.Rebind()

    End Sub

    Protected Sub GdOppProducts_DataBound(sender As Object, e As EventArgs) Handles GdOppProducts.DataBound

        Dim stredititems As String = GdOppProducts.EditItems.Count

        If stredititems > 0 Then
            For Each item As GridDataItem In GdOppProducts.MasterTableView.Items

                If item.IsInEditMode = True Then

                    Dim CommandItem As GridCommandItem = TryCast(GdOppProducts.MasterTableView.GetItems(GridItemType.CommandItem)(0), GridCommandItem)
                    Dim BtnAdd As Button = CType(CommandItem.FindControl("BtnProdAdd"), Button)
                    BtnAdd.Visible = False


                Else


                End If
            Next

        End If



    End Sub

    Protected Sub GdOppProducts_ItemCreated(sender As Object, e As GridItemEventArgs) Handles GdOppProducts.ItemCreated

        Dim strQuerystring As String = Request.QueryString("OI")

        Dim sqlOpp As String = "SELECT * FROM Opportunities WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strQuerystring

        Dim strBlock As Boolean = Getdata.retrieve_sql_field_value(sqlOpp, "IsEditable")


        If strBlock = False Then

            Dim CommandItem As GridCommandItem = TryCast(GdOppProducts.MasterTableView.GetItems(GridItemType.CommandItem)(0), GridCommandItem)
            Dim BtnAdd As Button = CType(CommandItem.FindControl("BtnProdAdd"), Button)
            BtnAdd.Visible = False

        End If


        If GdOppProducts.MasterTableView.IsItemInserted = True Then

            Dim CommandItem As GridCommandItem = TryCast(GdOppProducts.MasterTableView.GetItems(GridItemType.CommandItem)(0), GridCommandItem)
            Dim BtnAdd As Button = CType(CommandItem.FindControl("BtnProdAdd"), Button)
            BtnAdd.Visible = False

        End If


    End Sub

    Private Sub OpportunityProductSelect(strItem As GridEditableItem, strGrid As RadGrid)

        Dim strAIMCode As TextBox = DirectCast(strItem.FindControl("TxtAIMSearch"), TextBox)

        Dim strSAPCode As TextBox = DirectCast(strItem.FindControl("TxtSAPSearch"), TextBox)
        Dim strDescription As TextBox = DirectCast(strItem.FindControl("TxtDescription"), TextBox)

        Dim CbProductList As DropDownList = DirectCast(strItem.FindControl("CbProducts"), DropDownList)

        Dim SqlProduct As String = "SELECT * FROM Products WHERE TenantId=" & Getdata.TenantId & "AND AIMCode LIKE '%" & strAIMCode.Text & "%' AND ERPCode LIKE '%" & strSAPCode.Text & "%' AND Name LIKE '%" & strDescription.Text & "%'"

        Dim dt As DataTable = Getdata.retrive_tablelist(SqlProduct)

        If dt.Rows.Count > 0 Then

            'DirectCast(item.FindControl("UpdProdPanel"), UpdatePanel).Visible = True
            DirectCast(strItem.FindControl("TxtDescription"), TextBox).Visible = False
            DirectCast(strItem.FindControl("BtnNameSearch"), LinkButton).Visible = False
            'DirectCast(strItem.FindControl("CbProductTypes"), DropDownList).Enabled = False

            CbProductList.Visible = True

            CbProductList.DataSource = dt
            CbProductList.DataTextField = "Name"
            CbProductList.DataValueField = "Id"
            'ddl.SelectedValue = "Id"
            CbProductList.DataBind()


        Else

            'DirectCast(item.FindControl("UpdProdPanel"), UpdatePanel).Visible = False
            DirectCast(strItem.FindControl("TxtDescription"), TextBox).Visible = True

            CbProductList.Visible = False


        End If





    End Sub

    Protected Sub GdOppProducts_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles GdOppProducts.ItemCommand

        If e.CommandName = "ProductSearch" Then

            Dim strGrid As RadGrid = GdOppProducts
            Dim item As GridEditableItem = DirectCast(e.Item, GridEditableItem)

            OpportunityProductSelect(item, strGrid)

        End If

        If e.CommandName = "Cancel" Then

            e.Canceled = True
            Me.AlertDivProd.Visible = False
            GdOppProducts.MasterTableView.IsItemInserted = True
            'DirectCast(GdOppProducts.FindControl("CbProductTypes"), DropDownList).Enabled = True
            ' Check for the !IsPostBack also for initial page load
            GdOppProducts.Rebind()

        End If

        If e.CommandName = "Delete" Then

            Dim strRowId As String = GdOppProducts.MasterTableView.Items(0).GetDataKeyValue("Id").ToString()

            Dim SqlDelete As String = "DELETE FROM OpportunityDetails WHERE Id=" & strRowId & "AND TenantId=" & Getdata.TenantId


            Try
                Getdata.run_sql_query(SqlDelete)
            Catch ex As Exception

            End Try

            GdOppProducts.Rebind()



        End If

    End Sub

    Protected Sub LinkButton3_Click(sender As Object, e As EventArgs)

        GdOppProducts.MasterTableView.IsItemInserted = False
        Me.AlertDivProd.Visible = False
        GdOppProducts.Rebind()

    End Sub

    Private Sub LoadCustomerTypeBox()

        Dim strRadioButton As RadioButtonList = DirectCast(FrmOpportunity.FindControl("OptionCustomerSelection"), RadioButtonList)

        Dim ddl As DropDownList = DirectCast(FrmOpportunity.FindControl("CbCustomer"), DropDownList)


        If strRadioButton.SelectedItem.Value = 0 Then ' Selected value Groups! 

            Dim SqlGroups As String = "SELECT * FROM CustomerGroups WHERE TenantId=" & Getdata.TenantId

            ddl.DataSource = Getdata.retrive_tablelist(SqlGroups)

            ddl.DataTextField = "Description"
            ddl.DataValueField = "Id"
            'ddl.SelectedValue = "Id"
            ddl.DataBind()


        End If

        If strRadioButton.SelectedItem.Value = 1 Then ' Selected value Customers! 

            Dim SqlCustomers As String = "SELECT * FROM Customers WHERE TenantId=" & Getdata.TenantId

            ddl.DataSource = Getdata.retrive_tablelist(SqlCustomers)

            ddl.DataTextField = "Name"
            ddl.DataValueField = "Id"
            'ddl.SelectedValue = "Id"
            ddl.DataBind()



        End If
        ddl.Visible = True


    End Sub


    Protected Sub srcOpportunityDetails_Updating(sender As Object, e As SqlDataSourceCommandEventArgs) Handles srcOpportunityDetails.Updating

        Dim strTotalValue As TextBox = DirectCast(FrmOpportunity.FindControl("TxtTotalValue"), TextBox)

        If Len(strTotalValue.Text) = 0 Then

            e.Command.Parameters("@TotalValue").Value = 0

        Else
            e.Command.Parameters("@TotalValue").Value = strTotalValue.Text

        End If


    End Sub

    Protected Sub GdOppProducts_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles GdOppProducts.ItemDataBound

        If TypeOf e.Item Is GridDataItem Then
            Dim dataItem As GridDataItem = CType(e.Item, GridDataItem)

            Dim strQuerystring As String = Request.QueryString("OI")

            Dim sqlOpp As String = "SELECT * FROM Opportunities WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strQuerystring

            Dim strBlock As Boolean = Getdata.retrieve_sql_field_value(sqlOpp, "IsEditable")


            If strBlock = False Then

                Dim strButton As LinkButton = CType(e.Item.FindControl("LinkButton1"), LinkButton)
                strButton.Visible = False


            End If


        End If


    End Sub
End Class