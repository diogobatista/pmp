﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Dashboard

    '''<summary>
    '''ddlProducts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProducts As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlCustomers control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCustomers As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlDates control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDates As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlOpportunities control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlOpportunities As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlStatus As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Chart1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Chart1 As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''gvTopCustomers control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvTopCustomers As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''gvTopProducts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvTopProducts As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''srcOpportunities control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcOpportunities As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''srcStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcStatus As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''srcDates control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcDates As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''srcProducts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcProducts As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''srcCustomers control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcCustomers As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''srcGraphTopProducts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcGraphTopProducts As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''srcGraphTopCustomers control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcGraphTopCustomers As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''gvMaster control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvMaster As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''srcData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcData As Global.System.Web.UI.WebControls.SqlDataSource
End Class
