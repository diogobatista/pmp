﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ChildMaster.master" CodeBehind="Opportunities.aspx.vb" Inherits="pmp_V3.WebForm8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ul class="nav" id="side-menu">
        <li class="sidebar-search">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            <!-- /input-group -->
        </li>
        <li role="presentation" class="divider"></li>
        <li><a href="OpportunityDetails.aspx?OI=0">Criar</a></li>
        <li><a href="#">...</a></li>
        <li><a href="#">...</a></li>
        <li><a href="#">...</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Oportunidades</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <small>
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table" DataKeyNames="Id" DataSourceID="srcOpportunitiesList" GridLines="None" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Id" SortExpression="Id">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "OpportunityDetails.aspx?OI=" & Eval("Id") %>' Text='<%# Eval("TxtOpportunityId")%>'></asp:HyperLink>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="InternalId" HeaderText="Código" SortExpression="InternalId" />
                                    <asp:TemplateField HeaderText="Cliente" SortExpression="TxtCustomerName">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("TxtCustomerName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("TxtCustomerName") %>'></asp:Label>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("TxtGroupName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TxtOppTypeName" HeaderText="Tipo" SortExpression="TxtOppTypeName" />
                                    <asp:BoundField DataField="TxtOppName" HeaderText="Descrição" SortExpression="TxtOppName" />
                                    
                                    <asp:BoundField DataField="TxtCreateDate" HeaderText="Data" ReadOnly="True" SortExpression="TxtCreateDate" />
                                    <asp:BoundField DataField="TotalValue" HeaderText="Valor" SortExpression="TotalValue" />
                                </Columns>
                            </asp:GridView>
                            <telerik:RadGrid ID="GdOpportunitiesList" Visible="false" CssClass="table table-striped table-bordered table-hover" runat="server" AutoGenerateColumns="False" DataSourceID="srcOpportunitiesList" EnableEmbeddedSkins="False" Width="100%" EnableAjaxSkinRendering="False" EnableEmbeddedBaseStylesheet="False" EnableEmbeddedScripts="False" EnableTheming="True">
                                <ClientSettings>
                                    <Scrolling AllowScroll="True" ScrollHeight="" />
                                    <Resizing AllowResizeToFit="False" />
                                </ClientSettings>
                                <ExportSettings>
                                    <Pdf PageWidth="">
                                    </Pdf>
                                </ExportSettings>
                                <MasterTableView AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="srcOpportunitiesList">
                                    <Columns>
                                        <telerik:GridTemplateColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter Id column" HeaderText="Id" SortExpression="Id" UniqueName="Id" Visible="False">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="IdTextBox" runat="server" Text='<%# Bind("Id") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "OpportunityDetails.aspx?OI=" & Eval("Id") %>' Text='<%# Eval("Id") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="Teste" UniqueName="TemplateColumn" Visible="False">
                                            <ItemTemplate>
                                                <div class="row">

                                                    <div class="col-sm-3">
                                                        <h5><strong>
                                                            <p class="form-control-static">
                                                                #
                                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "OpportunityDetails.aspx?OI=" & Eval("Id") %>' Text='<%# Eval("InternalId")%>'></asp:HyperLink>
                                                                <%--<%# Eval("InternalId")%>--%>
                                                                <small><%# Eval("TxtOppName")%></small>
                                                            </p>
                                                        </strong>

                                                        </h5>

                                                        <span class="label label-default"><%# Eval("TxtOppTypeName")%></span>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <h6><strong>
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtCustomerName")%>
                                                                <%# Eval("TxtGroupName")%>
                                                                <small><%# Eval("TxtOppOriginName")%></small>
                                                            </p>
                                                        </strong>
                                                        </h6>

                                                    </div>
                                                    <div class="col-sm-5">
                                                        <div class="col-sm-6">
                                                            <h6>
                                                                <p class="form-control-static">
                                                                    <strong>Publicação
                                                                    </strong>
                                                                    <span><%# Eval("TxtOppPublicDate")%></span>

                                                                </p>
                                                            </h6>

                                                            <h6>
                                                                <p class="form-control-static">
                                                                    <strong>Validade
                                                                    </strong>
                                                                    <span><%# Eval("EndDate")%> dias</span>

                                                                </p>
                                                            </h6>

                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h6>
                                                                <p class="form-control-static">
                                                                    <strong>Nº Produtos
                                                                    </strong>

                                                                    <span class="label label-primary">2</span>

                                                                </p>
                                                            </h6>

                                                            <h6>
                                                                <p class="form-control-static">
                                                                    <strong>Nº Propostas
                                                                    </strong>

                                                                    <span class="label label-primary">2</span>

                                                                </p>
                                                            </h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="TenantId" DataType="System.Int32" FilterControlAltText="Filter TenantId column" HeaderText="TenantId" SortExpression="TenantId" UniqueName="TenantId" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn DataField="InternalId" FilterControlAltText="Filter InternalId column" HeaderText="Código" SortExpression="InternalId" UniqueName="InternalId">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="InternalIdTextBox" runat="server" Text='<%# Bind("InternalId") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# "OpportunityDetails.aspx?OI=" & Eval("Id") %>' Text='<%# Eval("InternalId") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="CustomerId" FilterControlAltText="Filter CustomerId column" HeaderText="CustomerId" SortExpression="CustomerId" UniqueName="CustomerId" DataType="System.Int32" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn DataField="TxtCustomerName" FilterControlAltText="Filter TxtCustomerName column" HeaderText="Cliente" SortExpression="TxtCustomerName" UniqueName="TxtCustomerName">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TxtCustomerNameTextBox" runat="server" Text='<%# Bind("TxtCustomerName") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="TxtCustomerNameLabel" runat="server" Text='<%# Eval("TxtCustomerName") %>'></asp:Label>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("TxtGroupName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="TxtOppTypeName" FilterControlAltText="Filter TxtOppTypeName column" HeaderText="Tipo" SortExpression="TxtOppTypeName" UniqueName="TxtOppTypeName" ItemStyle-Wrap="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtOppOriginName" FilterControlAltText="Filter TxtOppOriginName column" HeaderText="Origem" SortExpression="TxtOppOriginName" UniqueName="TxtOppOriginName">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtOppName" FilterControlAltText="Filter TxtOppName column" HeaderText="Descrição" SortExpression="TxtOppName" UniqueName="TxtOppName">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtOppPublicDate" FilterControlAltText="Filter TxtOppPublicDate column" HeaderText="Data" SortExpression="TxtOppPublicDate" UniqueName="TxtOppPublicDate" ReadOnly="True">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                            <ItemStyle Wrap="False" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TotalValue" FilterControlAltText="Filter TotalValue column" HeaderText="Valor" SortExpression="TotalValue" UniqueName="TotalValue" DataType="System.Decimal">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                    </Columns>

                                    <EditFormSettings>
                                        <EditColumn InsertImageUrl="Update.gif" UpdateImageUrl="Update.gif" CancelImageUrl="Cancel.gif"></EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>

                                <FilterMenu EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" EnableEmbeddedBaseStylesheet="False"></FilterMenu>

                                <HeaderContextMenu EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" EnableEmbeddedBaseStylesheet="False"></HeaderContextMenu>
                            </telerik:RadGrid>

                            <asp:SqlDataSource ID="srcOpportunitiesList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [VwOpportunities] WHERE ([TenantId] = @TenantId) Order By Id Desc">
                                <SelectParameters>
                                    <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </small>
                    </div>


                </div>

            </div>
        </div>
    </div>



</asp:Content>
