﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="pmp_V3.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%: Page.Title %> - PMP365</title>

    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>

    <webopt:BundleReference runat="server" Path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">PMP365 | Procurement Management Portal</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" runat="server">
                            <fieldset>
                                <div class="row text-center">
                                <div class="form-group">
                                    <input id="TxtEmail" runat="server" class="form-control" placeholder="E-mail" name="email" type="email" autofocus="autofocus" />
                                </div>
                                <div class="form-group">
                                    <input id="TxtPassword" runat="server" class="form-control" placeholder="Password" name="password" type="password" value="" />
                                </div>
                                <div id="DivAlert" runat="server"  class="alert alert-danger">Email ou Password errada ou inexistente.Tente novamente!</div>
                                <%--<div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="RLembrar"/>Remember Me
                                    </label>
                                </div>--%>
                                <!-- Change this to a button or input when using this as a form -->
                               <%-- <asp:Button ID="BtnLogin" runat="server" Text="Login" class="btn btn-lg btn-success btn-block" />--%>
                                <%--<button id="BtnLogin2" runat="server" class="btn btn-lg btn-success btn-block" aria-checked="undefined" onclick="BtnLoginClick" formmethod="post" >Login</button>--%>
                                <asp:Button ID="BtnLogin" class="btn btn-lg btn-primary btn-block" runat="server" Text="Login" />
                                <%--<asp:Button ID="Button1" runat="server" Text="Button" />--%>
                                    </div>
                            </fieldset>

                        </form>
                                     

                    </div>

                    

                </div>
            </div>
        </div>
    </div>
</body>
</html>
