﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ChildMaster.master" CodeBehind="ProposalDetails.aspx.vb" Inherits="pmp_V3.ProposalDetails" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <ul class="nav">
                <li>
                    <h5 id="HPdf" class="text-muted" runat="server">PDF</h5>
                </li>
                <li id="LiPdf" runat="server">
                    <asp:LinkButton ID="BtnPdf" runat="server">Pdf</asp:LinkButton>
                </li>

            </ul>


        </div>


    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav" id="side-menu1">
                <li>
                    <h5 id="ActionsTitle" class="text-muted" runat="server">Acções</h5>
                </li>
                <li id="Li6" runat="server">
                    <asp:LinkButton ID="PropToEdit" runat="server">Edição</asp:LinkButton>
                </li>
                <li id="Li0" runat="server">
                    <asp:LinkButton ID="PropToValidate" runat="server">P/Validação</asp:LinkButton>
                </li>
                <li id="Li1" runat="server">
                    <asp:LinkButton ID="PropValidate" runat="server">Validar</asp:LinkButton>
                </li>
                <li id="Li2" runat="server">
                    <asp:LinkButton ID="PropSubmit" runat="server">Submetida</asp:LinkButton>
                </li>
                <li id="Li3" runat="server">
                    <asp:LinkButton ID="PropInvalid" runat="server">Anulada</asp:LinkButton>
                </li>
                <li id="Li4" runat="server">
                    <asp:LinkButton ID="PropContract" runat="server">Contrato</asp:LinkButton>
                </li>
                <li id="Li5" runat="server">
                    <asp:LinkButton ID="PropForsake" runat="server">Recusada</asp:LinkButton>
                </li>
                <li id="Li7" runat="server">
                    <asp:LinkButton ID="PropClose" runat="server">Fechar</asp:LinkButton>
                </li>

            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-pills nav-stacked">
                <li>
                    <h5 class="text-muted" id="TaskCountTitle" runat="server">Tarefas</h5>
                </li>
                <li id="LiScheduled" runat="server">
                    <a href="#">
                        <span class="badge pull-right">
                            <asp:Label ID="LbScheduled" runat="server" Text="Label"></asp:Label>
                        </span>
                        Agendadas
                    </a>
                </li>
                <li id="LiOnGoing" runat="server">
                    <a href="#">
                        <span class="badge pull-right">
                            <asp:Label ID="LbOnGoing" runat="server" Text="Label"></asp:Label>

                        </span>
                        Em Execução
                    </a>
                </li>
                <li id="LiOverdue" runat="server">
                    <a href="#">
                        <span class="badge pull-right">
                            <asp:Label ID="LbOverdue" runat="server" Text="Label"></asp:Label>
                        </span>
                        Overdue
                    </a>
                </li>
                <li id="LiClosed" runat="server">
                    <a href="#">
                        <span class="badge pull-right">
                            <asp:Label ID="LbClosed" runat="server" Text="Label"></asp:Label>
                        </span>
                        Fechadas
                    </a>
                </li>
                <li id="LiInvalid" runat="server">
                    <a href="#">
                        <span class="badge pull-right">
                            <asp:Label ID="LbInvalid" runat="server" Text="Label"></asp:Label>
                        </span>
                        Anuladas
                    </a>
                </li>
                <li class="" id="LiTotal" runat="server">
                    <a href="#">
                        <span class="badge pull-right">
                            <asp:Label ID="LbTotal" runat="server" Text="Label"></asp:Label>
                        </span>
                        Total
                    </a>
                </li>

            </ul>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <div class="row" id="TitleNewRow" runat="server">
        <h4 class="page-header">
            <asp:Label ID="LbTitleNewProposal" runat="server" Text="Proposta" Visible="true"></asp:Label></h4>
    </div>

    <div class="row">
        <div class="row page-header" id="TitleRow" runat="server">
            <div class="col-md-8">

                <h3 class="text-left">REF: 
               <asp:Label ID="LbProposalId" runat="server" Text="Id"></asp:Label>

                    |
                
               <asp:Label ID="LbCustomerName" runat="server" Text="Customer"></asp:Label>
                </h3>
                <h4><small>
                    <asp:Label ID="LbPropDescription" runat="server" Text="Descrição"></asp:Label></small></h4>
                <h4><small>Registo: <strong>
                    <asp:Label ID="LbCreateDate" runat="server" Text="19/03/2014"></asp:Label>
                </strong>
                    | Validade:
                <strong>
                    <asp:Label ID="LbEndDate" runat="server" Text="Label"></asp:Label></strong>

                </small>

                </h4>

            </div>
            <div class="col-md-4">

                <h4 class="text-right"><span class="label label-default">
                    <asp:Label ID="LbHeaderStatus" runat="server" Text="Status"></asp:Label>
                </span>
                </h4>
            </div>

        </div>
    </div>
    <div class="col-xs-12">
        <p>
        </p>
    </div>
    <div class="row">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="col-md-12">
                    <div id="AlertDiv" runat="server" class="alert alert-warning">
                        <div class="nav nav-pills nav-stacked col-md-12">
                            <div id="AlertLabel" class="col-md-12">
                                <asp:Label ID="LbAlert" runat="server" Text="Label"></asp:Label>
                                <strong>
                                    <asp:Label ID="LbConfirmLabel" runat="server" Text="Label" Visible="False"></asp:Label></strong>
                            </div>
                            <div id="DivConfirm" class="col-md-12">
                                <asp:CheckBox ID="CbxConfirm" runat="server" Text="Confirme para avançar  " TextAlign="Left" Visible="False" />
                            </div>
                            <div id="DivInformation" class="col-md-12">

                                <asp:Label ID="LbStatusId" runat="server" Text="Label" Visible="False"></asp:Label>

                                <asp:TextBox ID="TxtVariableDate" CssClass="form-control input-sm txtMediumBox" runat="server" Text="" TextMode="Date" Visible="False"></asp:TextBox>
                                <asp:DropDownList ID="CbForsakeMotives" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" DataSourceID="srcForsakeMotives" DataTextField="Description" DataValueField="Id" Visible="False"></asp:DropDownList>
                                <asp:SqlDataSource ID="srcForsakeMotives" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT 0 AS Id, '(Escolha um)' AS Description UNION SELECT Id, Description FROM ProposalMotives WHERE (TenantId = @TenantId) ORDER BY Id">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:DropDownList ID="CbUserAproval" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" DataSourceID="srcUserAprovalList" DataTextField="Name" DataValueField="Id" Visible="False">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="srcUserAprovalList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [Users] WHERE (([IsAproval] = @IsAproval) AND ([TenantId] = @TenantId))">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="IsAproval" Type="Boolean" />
                                        <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:Button ID="BtnActionOk" runat="server" CssClass="Btn btn-link btn-sm" Text="Ok" Visible="False" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div class="row">
        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
            <ContentTemplate>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <small>
                                <asp:LinkButton ID="PropShowCaret" CssClass="text-rigth" runat="server">Informações<span class="caret"></span></asp:LinkButton>
                                <asp:LinkButton ID="PropHideCaret" CssClass="text-rigth" runat="server">    
                                Informações                
                        <span class="dropup">
                        <span class="caret">
                        </span>
                    </span>
                                </asp:LinkButton>

                            </small>
                        </div>
                        <div class="panel-body" id="ProposalPanel" runat="server">
                            <div class="col-md-6">
                                <asp:FormView ID="FrmProposal" runat="server" CssClass="form-horizontal" DataKeyNames="Id" DataSourceID="srcProposalDetails" Width="100%">
                                    <EditItemTemplate>
                                        <small>
                                            <form class="form-horizontal" role="form">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Entidade</label>
                                                    <div class="col-sm-9">
                                                        <telerik:RadComboBox ID="CbEntitiesList" runat="server" DataSourceID="srcEntitiesList" DataTextField="Description" DataValueField="Id" EnableTheming="True" Skin="Metro" Width="300px" SelectedValue='<%# Bind("EntityId") %>'></telerik:RadComboBox>
                                                        <asp:SqlDataSource ID="srcEntitiesList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [Entities] WHERE ([TenantId] = @TenantId)">
                                                            <SelectParameters>
                                                                <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Tipo</label>
                                                    <div class="col-sm-9">
                                                        <telerik:RadComboBox ID="CbPropTypeList" runat="server" DataSourceID="srcPropTypesList" Skin="Metro" Width="200px" DataTextField="Name" DataValueField="Id" SelectedValue='<%# Bind("ProposalTypeId") %>'></telerik:RadComboBox>
                                                        <asp:SqlDataSource ID="srcPropTypesList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [ProposalTypes] WHERE ([TenantId] = @TenantId)">
                                                            <SelectParameters>
                                                                <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Descrição</label>
                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="TxtPropDescription" CssClass="form-control input-sm" runat="server" Text='<%# Bind("TxtProposalName") %>'></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Validade</label>
                                                    <div class="col-sm-9">

                                                        <asp:TextBox ID="TxtEndDate" CssClass="form-control input-sm smallBox" runat="server" Text='<%# Bind("ProposalEndDate") %>'></asp:TextBox>
                                                        <small>(dias)</small>

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Owner</label>
                                                    <%--                                                <asp:TextBox ID="TextBox1" CssClass="form-control input-sm smallBox" runat="server" Text='<%# Bind("ProposalEndDate")%>'></asp:TextBox>--%>
                                                    <div class="col-sm-9">
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("TxtOwnerUserName") %>'></asp:Label>
                                                        <%--<telerik:RadComboBox ID="CbUserOwner" runat="server" DataSourceID="srcUsersList" DataTextField="Name" DataValueField="Id" Skin="Metro" Width="185px" SelectedValue='<%# Bind("OwnerUserId") %>'></telerik:RadComboBox>
                                                    <asp:SqlDataSource ID="srcUsersList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [Users] WHERE ([TenantId] = @TenantId)">
                                                        <SelectParameters>
                                                            <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>--%>
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Comentario</label>
                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="TextBox2" CssClass="form-control input-sm" Rows="3" runat="server" Text='<%# Bind("Comments") %>'></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2">
                                                        <asp:Button ID="BtnInsert" CssClass="btn btn-primary btn-sm" runat="server" Text="Guardar" CommandName="Update" />
                                                        <asp:Button ID="BtnCancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancelar" CommandName="Cancel" />
                                                    </div>
                                                </div>
                                            </form>
                                            </div>
                                        </small>
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        <small>
                                            <div class="row col-md-12">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Entidade</label>
                                                        <div class="col-sm-9">
                                                            <telerik:RadComboBox ID="CbEntitiesList" runat="server" DataSourceID="srcEntitiesList" DataTextField="Description" DataValueField="Id" EnableTheming="True" Skin="Metro" Width="300px" SelectedValue='<%# Bind("EntityId") %>'></telerik:RadComboBox>
                                                            <asp:SqlDataSource ID="srcEntitiesList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [Entities] WHERE ([TenantId] = @TenantId)">
                                                                <SelectParameters>
                                                                    <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Tipo</label>
                                                        <div class="col-sm-9">
                                                            <telerik:RadComboBox ID="CbPropTypeList" runat="server" DataSourceID="srcPropTypesList" Skin="Metro" Width="200px" DataTextField="Name" DataValueField="Id" SelectedValue='<%# Bind("ProposalTypeId") %>'></telerik:RadComboBox>
                                                            <asp:SqlDataSource ID="srcPropTypesList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [ProposalTypes] WHERE ([TenantId] = @TenantId)">
                                                                <SelectParameters>
                                                                    <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Descrição</label>
                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="TxtPropDescription" CssClass="form-control input-sm" runat="server" Text='<%# Bind("TxtProposalName") %>'></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Validade</label>
                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="TxtEndDate" CssClass="form-control input-sm smallBox" runat="server" Text='<%# Bind("ProposalEndDate") %>'></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <%--<div class="form-group">
                                                    <label class="col-sm-3 control-label">Owner</label>
                                                    <div class="col-sm-9">
                                                        <telerik:RadComboBox ID="CbUserOwner" runat="server" DataSourceID="srcUsersList" DataTextField="Name" DataValueField="Id" Skin="Metro" Width="185px" SelectedValue='<%# Bind("OwnerUserId") %>'></telerik:RadComboBox>
                                                        <asp:SqlDataSource ID="srcUsersList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [Users] WHERE ([TenantId] = @TenantId)">
                                                            <SelectParameters>
                                                                <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </div>

                                                </div>--%>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Comentario</label>
                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="TextBox2" CssClass="form-control input-sm" Rows="3" runat="server" Text='<%# Bind("Comments") %>'></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2">
                                                            <asp:Button ID="BtnInsert" CssClass="btn btn-primary btn-sm" runat="server" Text="Guardar" CommandName="Insert" />
                                                            <asp:Button ID="BtnCancel" CssClass="btn btn-default btn-sm" runat="server" Text="Limpar" CommandName="Cancel" OnClick="BtnCancel_Click" />
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </small>
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                        <small>
                                            <form class="" role="form">
                                                <div class="col-md-12">
                                                    <div class="row text-left">
                                                        <h4>Proposta</h4>
                                                    </div>
                                                    <%--<div class="row">
                                                        <label class="col-md-5 control-label">
                                                            Id</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static">
                                                                <%# Eval("Id")%>
                                                            </p>
                                                        </div>
                                                    </div>--%>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">
                                                            Entidade</label>
                                                        <div class="col-sm-9">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtEntityName")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">
                                                            Tipo</label>
                                                        <div class="col-sm-9">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtPropTypeName")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">
                                                            Responsável</label>
                                                        <div class="col-sm-9">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtOwnerUserName")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">
                                                            Comentários</label>
                                                        <div class="col-sm-9">
                                                            <p class="form-control-static">
                                                                <%# Eval("Comments")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <%--<div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Descrição</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtProposalName")%>
                                                            </p>
                                                        </div>
                                                    </div>--%>
                                                    <%--<div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Data Fim</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("ProposalEndDate")%> 
                                                            </p>
                                                        </div>
                                                    </div>--%>
                                                    <%--<div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Status</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtPropStatusName")%>
                                                            </p>
                                                        </div>
                                                    </div>--%>


                                                    <div class="row">
                                                        <span class="col-sm-3 control-label">
                                                            <asp:Button ID="BtnEdit" CssClass="btn btn-primary btn-sm" runat="server" Text="Editar" CommandName="Edit" /></span>
                                                        <div class="col-sm-9">
                                                        </div>
                                                    </div>
                                                </div>
                                                <%-- <div class="col-md-6">
                                                   
                                                    
                                                    <%--<div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Criado por</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtUserSubmit")%>
                                                            </p>
                                                        </div>
                                                    </div>--%>
                                                <%--                                    <div class="row">
                                        <label class="col-sm-4 control-label">
                                            Data Sistema</label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static">
                                                <%# Eval("TxtCreateDate")%>
                                            </p>
                                        </div>
                                    </div>--%>
                                                <%--<div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Motivo</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtMotiveName")%>
                                                            </p>
                                                        </div>
                                                    </div>

                                                </div>--%>
                                            </form>
                                        </small>
                                    </ItemTemplate>
                                </asp:FormView>
                                <asp:SqlDataSource ID="srcProposalDetails" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [VwProposals] WHERE (([TenantId] = @TenantId) AND ([Id] = @Id))" InsertCommand="INSERT INTO Proposals (TenantId, EntityId,CustomerId,CustomerGroupId, ProposalTypeId, OpportunityId, Name, StatusId, ProposalEndDate, OwnerUserId, Comments, CreateDate, CreateUserId, ModifiedDate, ModifiedUserId) 
                                        VALUES (@TenantId, @EntityId,@CustomerId,@CustomerGroupId, @ProposalTypeId, @OpportunityId, @TxtProposalName,1,@ProposalEndDate, @OwnerUserId,  @Comments, GETDATE(), @CreateUserId,GETDATE(), @CreateUserId)"
                                    UpdateCommand="UPDATE Proposals SET EntityId=@EntityId, ProposalTypeId=@ProposalTypeId,Name=@TxtProposalName,ProposalEndDate=@ProposalEndDate, Comments=@Comments, ModifiedDate=GETDATE(), ModifiedUserId=@ModifiedUserId WHERE Id=@Id AND TenantId=@TenantId">
                                    <InsertParameters>
                                        <asp:SessionParameter Name="TenantId" SessionField="TenantId" />
                                        <asp:Parameter Name="EntityId" />
                                        <asp:Parameter Name="ProposalTypeId" />
                                        <asp:QueryStringParameter Name="OpportunityId" QueryStringField="OI" />
                                        <asp:Parameter Name="ProposalEndDate" />
                                        <asp:SessionParameter Name="OwnerUserId" SessionField="UserId" />
                                        <asp:Parameter Name="Comments" />
                                        <asp:SessionParameter Name="CreateUserId" SessionField="UserId" />
                                        <asp:Parameter Name="TxtProposalName" />
                                        <asp:Parameter Name="CustomerId" />
                                        <asp:Parameter Name="CustomerGroupId" />
                                    </InsertParameters>
                                    <SelectParameters>
                                        <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                        <asp:QueryStringParameter Name="Id" QueryStringField="PI" Type="Int32" />
                                    </SelectParameters>
                                    <UpdateParameters>
                                        <asp:Parameter Name="EntityId" />
                                        <asp:Parameter Name="ProposalTypeId" />
                                        <asp:Parameter Name="ProposalEndDate" />
                                        <asp:Parameter Name="Comments" />
                                        <asp:SessionParameter Name="ModifiedUserId" SessionField="UserId" />
                                        <asp:Parameter Name="Id" />
                                        <asp:SessionParameter Name="TenantId" SessionField="TenantId" />
                                        <asp:Parameter Name="TxtProposalName" />
                                    </UpdateParameters>
                                </asp:SqlDataSource>

                            </div>
                            <div class="col-md-6">


                                <asp:FormView ID="FrmOpportunity" runat="server" CssClass="form-horizontal" DataKeyNames="Id" DataSourceID="srcPropOpportunityDetails" Width="100%">
                                    <ItemTemplate>
                                        <small>
                                            <div class="col-md-12">
                                                <form class="form-horizontal" role="form">
                                                    <div class="row text-left">
                                                        <h4>Oportunidades</h4>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">
                                                            Codigo</label>
                                                        <div class="col-sm-9">
                                                            <p class="form-control-static">
                                                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# "OpportunityDetails.aspx?OI=" & Eval("Id") %>'><%# Eval("InternalId")%></asp:HyperLink>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">
                                                            Tipo</label>
                                                        <div class="col-sm-9">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtOppTypeName")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">
                                                            Origem</label>
                                                        <div class="col-sm-9">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtOppOriginName")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">
                                                            Descrição</label>
                                                        <div class="col-sm-9">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtOppName")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">
                                                            Publicação</label>
                                                        <div class="col-sm-9">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtOppPublicDate")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">
                                                            Validade</label>
                                                        <div class="col-sm-9">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtEndDate")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <%--<div class="row">
                                                    <label class="col-sm-3 control-label">
                                                        Valor</label>
                                                    <div class="col-sm-9">
                                                        <p class="form-control-static">
                                                            <%# Eval("TotalValue")%>
                                                        </p>
                                                    </div>
                                                </div>--%>
                                                    <%--<div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Codigo</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("InternalId") %>
                                                            </p>
                                                        </div>
                                                    </div>--%>
                                            </div>
                                            <%--<div class="col-md-6">
                                                
                                                <%--<div class="row">
                                                    <label class="col-sm-4 control-label">
                                                        Cliente</label>
                                                    <div class="col-sm-8">
                                                        <p class="form-control-static">
                                                            <%# Eval("TxtCustomerName")%>
                                                        </p>
                                                    </div>
                                                </div>--%>



                                            <%--                                    <div class="row">
                                        <label class="col-sm-4 control-label">
                                            Data Sistema</label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static">
                                                <%# Eval("TxtCreateDate")%>
                                            </p>
                                        </div>
                                    </div>--%>
                                            <%--</div>--%>
                                            </form>
                                        </small>
                                    </ItemTemplate>
                                </asp:FormView>

                                <asp:SqlDataSource ID="srcPropOpportunityDetails" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [VwOpportunities] WHERE (([TenantId] = @TenantId) AND ([Id] = @Id))">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                        <asp:QueryStringParameter Name="Id" QueryStringField="OI" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>

                            </div>



                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--    <div class="row">
        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
            <ContentTemplate>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <small>

                                <asp:LinkButton ID="OppShowPanel" CssClass="text-rigth" runat="server">Dados Oportunidade<span class="caret"></span></asp:LinkButton>
                                <asp:LinkButton ID="OppHidePanel" CssClass="text-rigth" runat="server"> 
                                Dados Oportunidade                   
                        <span class="dropup">
                        <span class="caret">
                        </span>
                                </span>
                                </asp:LinkButton>

                            </small>
                        </div>
                        <div class="panel-body" id="OpportunityPanel" runat="server">
                            <div class="col-md-12">


                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>--%>


    <div class="row">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="col-md-12">
                    <div id="DivProdAlert" runat="server" class="alert alert-warning">
                        <div class="nav nav-pills nav-stacked col-md-12">
                            <div id="ProdAlert" class="col-md-12">
                                <asp:Label ID="LbProdAlert" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="row" id="DivProductsList" runat="server">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Produtos
                </div>
                <div class="panel-body">
                    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Width="100%" LoadingPanelID="RadAjaxLoadingPanel1">
                        <telerik:RadGrid ID="GdPropProducts" runat="server" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AutoGenerateColumns="False" CssClass="table table-striped" DataSourceID="srcPropProducts" EnableEmbeddedBaseStylesheet="False" EnableEmbeddedSkins="False" Style="outline: none;" Width="100%" Visible="False">
                            <ExportSettings>
                                <Pdf PageWidth="">
                                </Pdf>
                            </ExportSettings>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" ScrollHeight="" />
                            </ClientSettings>
                            <MasterTableView Name="parent" ExpandCollapseColumn-Visible="False" AllowAutomaticInserts="True" AllowAutomaticUpdates="False" CommandItemDisplay="Bottom" DataKeyNames="OppDetailId,PropDetailId,ProductId" DataSourceID="srcPropProducts" EditMode="InPlace" EnableHeaderContextAggregatesMenu="True" EnableHeaderContextFilterMenu="True" EnableHeaderContextMenu="True" HorizontalAlign="Left">

                                <DetailTables>

                                    <telerik:GridTableView Name="FirstChild" ExpandCollapseColumn-Visible="False" DataKeyNames="Id,ProductId" runat="server" EditMode="InPlace" DataSourceID="srcOrderRequests" CssClass="detailTable bg-info" HorizontalAlign="Left">
                                        <DetailTables>
                                            <telerik:GridTableView Name="SecondChild" ShowHeader="False" runat="server" DataKeyNames="Id,OrderRequestId" DataSourceID="srcInvoices" CssClass="detailTable" HorizontalAlign="Left">
                                                <ParentTableRelation>
                                                    <telerik:GridRelationFields DetailKeyField="OrderRequestId" MasterKeyField="Id" />
                                                </ParentTableRelation>
                                                <Columns>
                                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" UniqueName="InvoiceInformation">
                                                        <ItemTemplate>
                                                            <div class="row secondDetailTable">
                                                                <small>
                                                                    <div class="col-md-4 text-left">
                                                                        <span><%# Eval("OrderExternalId")%></span>- <strong>
                                                                            <asp:Label ID="LbRequestDescription" runat="server" Text='<%# Eval("cliente_pagador_temp")%>'></asp:Label>
                                                                        </strong>
                                                                    </div>
                                                                    <div class="col-xs-4">
                                                                        <p class="text-left">
                                                                            <strong>Quantidade: </strong><span class="text-right"><%# Eval("InvoiceQuantity")%></span>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-xs-4">
                                                                        <strong>Total : </strong><span><%# Eval("InvoiceTotal")%>€ </span>
                                                                    </div>
                                                                </small>
                                                            </div>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn DataField="Id" FilterControlAltText="Filter column column" HeaderText="Id" UniqueName="column" Visible="False">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="IdTextBox" runat="server" Text='<%# Bind("Id") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn DataField="cliente_name_temp" FilterControlAltText="Filter column1 column" HeaderText="Cliente" UniqueName="column1" Visible="False">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="cliente_name_tempTextBox" runat="server" Text='<%# Bind("cliente_name_temp") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="cliente_name_tempLabel" runat="server" Text='<%# Eval("cliente_name_temp") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn DataField="product_name_temp" FilterControlAltText="Filter column2 column" HeaderText="Produto" UniqueName="column2" Visible="False">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="product_name_tempTextBox" runat="server" Text='<%# Bind("product_name_temp") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="product_name_tempLabel" runat="server" Text='<%# Eval("product_name_temp") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn DataField="InvoiceQuantity" FilterControlAltText="Filter column3 column" HeaderText="Quantidade" UniqueName="column3" Visible="False">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="InvoiceQuantityTextBox" runat="server" Text='<%# Bind("InvoiceQuantity") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="InvoiceQuantityLabel" runat="server" Text='<%# Eval("InvoiceQuantity") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                </Columns>
                                                <EditFormSettings>
                                                    <EditColumn CancelImageUrl="Cancel.gif" InsertImageUrl="Update.gif" UpdateImageUrl="Update.gif">
                                                    </EditColumn>
                                                </EditFormSettings>
                                                <ItemStyle BorderColor="White" BorderStyle="None" BorderWidth="0px" />
                                                <AlternatingItemStyle BorderColor="White" BorderStyle="None" />
                                            </telerik:GridTableView>
                                        </DetailTables>
                                        <RowIndicatorColumn Visible="False">
                                        </RowIndicatorColumn>
                                        <ParentTableRelation>
                                            <telerik:GridRelationFields DetailKeyField="ProductId" MasterKeyField="ProductId" />
                                        </ParentTableRelation>
                                        <ExpandCollapseColumn Created="True">
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn1 column" UniqueName="ExpandChild">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="BtnShowSecondChild" runat="server" CommandName="ShowSecondChild" CssClass="text-rigth"><span class="glyphicon glyphicon-collapse-down"></span></asp:LinkButton>
                                                    <asp:LinkButton ID="BtnHideSecondChild" runat="server" CommandName="HideSecondChild" CssClass="text-rigth"><span class="glyphicon glyphicon-collapse-up"></span></asp:LinkButton>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter RequestInfo column" HeaderText="Pedido" UniqueName="RequestInfo">
                                                <ItemTemplate>
                                                    <div class="row">
                                                        <small>
                                                            <div class="col-md-6">
                                                                <asp:Label ID="LbRequestMaster" runat="server" Text='<%# Eval("RequestMaster") %>'></asp:Label>
                                                                - <strong>
                                                                    <asp:Label ID="LbRequestDescription" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                                                    |
                                                                    <strong>Ref : </strong><span><%# Eval("RequestSlave") %></span>
                                                                    |
                                                                    <strong>Quantidade : </strong><span><%# Eval("Quantity")%></span>
                                                                </strong>
                                                            </div>
                                                            <div class="col-md-6">


                                                                <%--        <h7>
                                                                        <p class="form-control-static">
                                                                            
                                                                        </p>
                                                                     
                                                                    </h7>--%>
                                                            </div>
                                                        </small>
                                                    </div>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn DataField="RequestMaster" FilterControlAltText="Filter column column" HeaderText="Nº Pedido" UniqueName="RequestMasterId" Visible="False">
                                                <HeaderStyle Font-Size="Small" />
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="RequestMasterTextBox" runat="server" CssClass="form-control input-sm gridMediumbox" Text='<%# Bind("RequestMaster") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="RequestMasterLabel" runat="server" Text='<%# Eval("RequestMaster") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn DataField="RequestSlave" FilterControlAltText="Filter column1 column" HeaderText="Referente a" UniqueName="RequestSlaveId" Visible="False">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="RequestSlaveTextBox" runat="server" CssClass="form-control input-sm gridMediumbox" Text='<%# Bind("RequestSlave") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="RequestSlaveLabel" runat="server" Text='<%# Eval("RequestSlave") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Size="Small" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn DataField="TxtReqTypeName" FilterControlAltText="Filter column2 column" HeaderText="TxtReqTypeName" UniqueName="column2" ReadOnly="True" Visible="False">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TxtReqTypeNameTextBox" CssClass="form-control input-sm gridMediumbox" runat="server" Text='<%# Bind("TxtReqTypeName") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="TxtReqTypeNameLabel" runat="server" Text='<%# Eval("TxtReqTypeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn DataField="Description" FilterControlAltText="Filter column3 column" HeaderText="Descrição" UniqueName="RequestDescription" Visible="False">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="DescriptionTextBox" CssClass="form-control input-sm txtBigBox" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Size="Small" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn DataField="Quantity" FilterControlAltText="Filter column4 column" HeaderText="Qtd" UniqueName="RequestQuantity" Visible="False">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="QuantityTextBox" CssClass="form-control input-sm gridMediumbox" runat="server" Text='<%# Bind("Quantity") %>' min="0" TextMode="Number"></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="QuantityLabel" runat="server" Text='<%# Eval("Quantity") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Size="Small" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" UniqueName="TemplateColumn">
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="BtnEditSave" CssClass="btn btn-link btn-xs" runat="server" CommandName="Update"><span class="glyphicon glyphicon-floppy-save"></span></asp:LinkButton>
                                                    <asp:LinkButton ID="BtnEditCancel" CssClass="btn btn-link btn-xs" runat="server" CommandName="Cancel"><span class="glyphicon glyphicon-ban-circle"></span></asp:LinkButton>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:LinkButton ID="BtnInsert" CssClass="btn btn-link btn-xs" runat="server" CommandName="PerformInsert"><span class="glyphicon glyphicon-save"></span></asp:LinkButton>
                                                    <asp:LinkButton ID="BtnInsCancel" CssClass="btn btn-link btn-xs" runat="server" CommandName="Cancel"><span class="glyphicon glyphicon-ban-circle"></span></asp:LinkButton>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="BtnEdit" CssClass="btn btn-link btn-xs" runat="server" CommandName="Edit" Text="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton>

                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <EditFormSettings>
                                            <EditColumn CancelImageUrl="Cancel.gif" InsertImageUrl="Update.gif" UpdateImageUrl="Update.gif">
                                            </EditColumn>
                                        </EditFormSettings>
                                    </telerik:GridTableView>
                                </DetailTables>

                                <CommandItemSettings ShowAddNewRecordButton="False" ShowRefreshButton="False" />
                                <RowIndicatorColumn Visible="False">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Created="True">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn1 column" UniqueName="ExpandParent">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="BtnShowChild" runat="server" CommandName="ShowChild" CssClass="text-rigth"><span class="glyphicon glyphicon-collapse-down"></span></asp:LinkButton>
                                            <asp:LinkButton ID="BtnHideChild" runat="server" CommandName="HideChild" CssClass="text-rigth"><span class="glyphicon glyphicon-collapse-up"></span></asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="OppDetailId" DataType="System.Int32" FilterControlAltText="Filter OppDetailId column" HeaderText="OppDetailId" SortExpression="OppDetailId" UniqueName="OppDetailId" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="OppDetailIdTextBox" runat="server" Text='<%# Bind("OppDetailId") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="OppDetailIdLabel" runat="server" Text='<%# Eval("OppDetailId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="OppId" DataType="System.Int32" FilterControlAltText="Filter OppId column" HeaderText="OppId" ReadOnly="True" SortExpression="OppId" UniqueName="OppId" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TenantId" DataType="System.Int32" FilterControlAltText="Filter TenantId column" HeaderText="TenantId" ReadOnly="True" SortExpression="TenantId" UniqueName="TenantId" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TxtOpportunityName" FilterControlAltText="Filter TxtOpportunityName column" HeaderText="TxtOpportunityName" ReadOnly="True" SortExpression="TxtOpportunityName" UniqueName="TxtOpportunityName" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Position" DataType="System.Int32" FilterControlAltText="Filter Position column" HeaderText="Pos" ReadOnly="True" SortExpression="Position" UniqueName="Position">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="Produto" ReadOnly="True" UniqueName="TemplateColumn">

                                        <ItemTemplate>
                                            <div class="row">
                                                <small>
                                                    <div class="col-md-12">
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <asp:Label ID="LbExternalCode" runat="server" Text='<%# Eval("ExternalCode") %>'></asp:Label>
                                                                - <strong>

                                                                    <asp:Label ID="LbProductId" runat="server" Text='<%# Eval("TxtOppProductId")%>' Visible="false"></asp:Label>

                                                                    <asp:Label ID="LbPropDetailId" runat="server" Text='<%# Eval("PropDetailId")%>' Visible="false"></asp:Label>
                                                                    <asp:LinkButton ID="ItemProductShow" runat="server" CommandName="ShowProductInfo" CssClass="text-rigth">
                                                                        <asp:Label ID="LbOppProdNameShow" runat="server" Text='<%# Eval("TxtProductName") %>'></asp:Label>

                                                                        <span class="caret"></span>
                                                                    </asp:LinkButton>
                                                                    <asp:LinkButton ID="ItemProductHide" runat="server" CommandName="HideProductInfo" CssClass="text-rigth">

                                                                        <asp:Label ID="LbOppProdNameHide" runat="server" Text='<%# Eval("TxtProductName") %>'></asp:Label>
                                                                        <span class="dropup">
                                                                            <span class="caret">
                                                                    </asp:LinkButton>
                                                                </strong>
                                                            </div>
                                                            <div id="ItemProductPanel" runat="server" class="panel-body">
                                                                <div class="col-md-12">
                                                                    <small>
                                                                        <div class="col-md-6">
                                                                            <h7>
                                                                                    <p class="form-control-static">
                                                                                        <strong>Principio : </strong><span><%# Eval("ActiveIngredientName")%></span>
                                                                                    </p>
                                                                                </h7>
                                                                            <h7>
                                                                                    <p class="form-control-static">
                                                                                        <strong>ERP : </strong><span><%# Eval("ERPCode")%></span>
                                                                                    </p>
                                                                                </h7>
                                                                            <h7>
                                                                                    <p class="form-control-static">
                                                                                        <strong>AIM : </strong><span><%# Eval("AIMCode")%></span>
                                                                                    </p>
                                                                                </h7>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <h7>
                                                                                    <p class="form-control-static">
                                                                                        <strong>Apresentação : </strong><span><%# Eval("SmallDescription")%></span>
                                                                                    </p>
                                                                                </h7>
                                                                            <h7>
                                                                                    <p class="form-control-static">
                                                                                        <strong>Preço : </strong><span><%# Eval("TxtProductPrice")%></span>
                                                                                    </p>
                                                                                </h7>
                                                                            <h7>
                                                                                    <p class="form-control-static">
                                                                                        <strong>Qt : </strong><span><%# Eval("TxtOppQuantity")%></span>
                                                                                    </p>
                                                                                    <%--                                                            <p class="form-control-static">
                                                                                    <span class="label label-primary">Qt :
                                                                                    <asp:Label ID="Label2" runat="server" Text=''></asp:Label>
                                                                                    </span>

                                                                                </p>--%>
                                                                                </h7>
                                                                        </div>
                                                                    </small>
                                                                </div>
                                                            </div>
                                                            <div class="panel-footer">
                                                                <div class="row">
                                                                    <div class="col-lg-7 text-left">
                                                                        <small>
                                                                            <strong>Total AUE : </strong>
                                                                            <asp:Label ID="LbProdTotalAUE" runat="server" Text=""></asp:Label>
                                                                            <strong>Total SAP : </strong>
                                                                            <asp:Label ID="LbProdTotalSAP" runat="server" Text=""></asp:Label>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-lg-5 text-right">
                                                                        <small>
                                                                            <strong>Total Vendido : </strong>
                                                                            <asp:Label ID="LbTotalSale" runat="server" Text=""></asp:Label>
                                                                        </small>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </small>
                                            </div>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="ExternalCode" DataType="System.Int32" FilterControlAltText="Filter ExternalCode column" HeaderText="Code" ReadOnly="True" SortExpression="ExternalCode" UniqueName="ExternalCode" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TxtOppProductName" FilterControlAltText="Filter TxtOppProductName column" HeaderText="Name" ReadOnly="True" SortExpression="TxtOppProductName" UniqueName="TxtOppProductName" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TxtOppQuantity" DataType="System.Int32" FilterControlAltText="Filter TxtOppQuantity column" HeaderText="OppQt" ReadOnly="True" SortExpression="TxtOppQuantity" UniqueName="TxtOppQuantity" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductTypeId" DataType="System.Int32" FilterControlAltText="Filter ProductTypeId column" HeaderText="ProductTypeId" ReadOnly="True" SortExpression="ProductTypeId" UniqueName="ProductTypeId" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SmallDescription" FilterControlAltText="Filter SmallDescription column" HeaderText="Desc" ReadOnly="True" SortExpression="SmallDescription" UniqueName="SmallDescription" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="UnitValue" DataType="System.Decimal" FilterControlAltText="Filter UnitValue column" HeaderText="UnitValue" ReadOnly="True" SortExpression="UnitValue" UniqueName="UnitValue" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PropId" DataType="System.Int32" FilterControlAltText="Filter PropId column" HeaderText="PropId" ReadOnly="True" SortExpression="PropId" UniqueName="PropId" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PropDetailId" DataType="System.Int32" FilterControlAltText="Filter PropDetailId column" HeaderText="PropDetailId" ReadOnly="True" SortExpression="PropDetailId" UniqueName="PropDetailId" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductId" DataType="System.Int32" FilterControlAltText="Filter ProductId column" HeaderText="ProductId" ReadOnly="True" SortExpression="ProductId" UniqueName="ProductId" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn DataField="TxtProductName" FilterControlAltText="Filter TxtProductName column" HeaderText="Produto" ReadOnly="True" SortExpression="TxtProductName" UniqueName="TxtProductName" Visible="False">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="CbProductList" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" DataSourceID="srcProductsList" DataTextField="Name" DataValueField="Id" SelectedValue='<%# Bind("ProductId") %>'>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="TxtProductNameLabel" runat="server" Text='<%# Eval("TxtProductName") %>'></asp:Label>
                                            <asp:DropDownList ID="CbProductList" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" DataSourceID="srcProductsList" DataTextField="Name" DataValueField="Id" Visible="False">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="TxtPropQuantity" DataType="System.Int32" FilterControlAltText="Filter TxtPropQuantity column" HeaderText="Qt" SortExpression="TxtPropQuantity" UniqueName="TxtPropQuantity">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtPropQt" runat="server" CssClass="form-control input-sm gridMediumbox" min="0" Text='<%# Bind("TxtPropQuantity") %>' TextMode="Number"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LbPropQt" runat="server" Text='<%# Eval("TxtPropQuantity") %>'></asp:Label>
                                            <asp:TextBox ID="TxtPropQt" runat="server" CssClass="form-control input-sm gridMediumbox" min="0" Text='<%# Bind("TxtPropQuantity") %>' TextMode="Number"></asp:TextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter ProductPriceSAP column" HeaderText="Preço SAP" UniqueName="ProductPriceSAP">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("TxtProductPrice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="Price " FilterControlAltText="Filter Price column" HeaderText="Preço" SortExpression="Price " UniqueName="Price">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtPropPrice" runat="server" CssClass="form-control input-sm gridsmallbox" min="0" step="any" Text='<%# Bind("TxtProposalPrice")%>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LbPropPrice" runat="server" Text='<%# Eval("TxtProposalPrice", "{0:C4}") %>'></asp:Label>
                                            <asp:TextBox ID="TxtPropPrice" runat="server" CssClass="form-control input-sm gridsmallbox" min="0" step="any" Text='<%# Bind("TxtProposalPrice")%>' TextMode="Number"></asp:TextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="FreeQty" DataType="System.Int32" FilterControlAltText="Filter FreeQty column" HeaderText="FreeQty" ReadOnly="True" SortExpression="FreeQty" UniqueName="FreeQty" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtFreeQty" runat="server" CssClass="form-control input-sm gridsmallbox" Text='<%# Bind("FreeQty") %>' TextMode="Number"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LbFreeQty" runat="server" Text='<%# Eval("FreeQty") %>'></asp:Label>
                                            <asp:TextBox ID="TxtFreeQty" runat="server" CssClass="form-control input-sm gridsmallbox" min="0" Text='<%# Bind("FreeQty") %>' TextMode="Number"></asp:TextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="PackageQty" DataType="System.Int32" FilterControlAltText="Filter PackageQty column" HeaderText="Pack" ReadOnly="True" SortExpression="PackageQty" UniqueName="PackageQty" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtPackageQty" runat="server" CssClass="form-control input-sm gridsmallbox" min="0" Text='<%# Bind("PackageQty") %>' TextMode="Number"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LbPackageQty" runat="server" Text='<%# Eval("PackageQty") %>'></asp:Label>
                                            <asp:TextBox ID="TxtPackageQty" runat="server" CssClass="form-control input-sm gridsmallbox" min="0" Text='<%# Bind("PackageQty") %>' TextMode="Number"></asp:TextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="Discount" DataType="System.Int32" FilterControlAltText="Filter Discount column" HeaderText="Disc" ReadOnly="True" SortExpression="Discount" UniqueName="Discount" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtDiscount" runat="server" CssClass="form-control input-sm gridsmallbox" min="0" Text='<%# Bind("Discount") %>' TextMode="Number"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LbDiscount" runat="server" Text='<%# Eval("Discount") %>'></asp:Label>
                                            <asp:TextBox ID="TxtDiscount" runat="server" CssClass="form-control input-sm gridsmallbox" min="0" Text='<%# Bind("Discount") %>' TextMode="Number"></asp:TextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="Vat" DataType="System.Decimal" FilterControlAltText="Filter Vat column" HeaderText="Vat" SortExpression="Vat" UniqueName="Vat" Visible="False">
                                        <ItemTemplate>
                                            <asp:TextBox ID="VatTextBox" runat="server" CssClass="form-control input-sm gridsmallbox" Text='<%# Bind("Vat") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="DeliveryMonths" DataType="System.Int32" FilterControlAltText="Filter DeliveryMonths column" HeaderText="Delivery" ReadOnly="True" SortExpression="DeliveryMonths" UniqueName="DeliveryMonths" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtDeliveryMonths" runat="server" CssClass="form-control input-sm gridsmallbox" min="0" Text='<%# Bind("TxtPropDelivery") %>' TextMode="Number"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LbDeliveryMonths" runat="server" Text='<%# Eval("TxtPropDelivery")%>'></asp:Label>
                                            <asp:TextBox ID="TxtDeliveryMonths" runat="server" CssClass="form-control input-sm gridsmallbox" min="0" Text='<%# Bind("TxtPropDelivery")%>' TextMode="Number"></asp:TextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="Validity" FilterControlAltText="Filter Validity column" HeaderText="Validade" SortExpression="Validity" UniqueName="Validity">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtPropValidity" runat="server" CssClass="form-control input-sm gridsmallbox" min="0" Text='<%# Bind("Validity") %>' TextMode="Number"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LbPropValidity" runat="server" Text='<%# Eval("Validity") %>'></asp:Label>
                                            <asp:TextBox ID="TxtPropValidity" runat="server" CssClass="form-control input-sm gridsmallbox" min="0" Text='<%# Bind("Validity") %>' TextMode="Number"></asp:TextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn1 column" UniqueName="Buttons">
                                        <EditItemTemplate>
                                            <asp:Button ID="BtnRowUpdate" runat="server" CommandName="Update" CssClass="btn btn-success btn-xs" Text="Guardar" />
                                            <asp:Button ID="BtnRowCancel" runat="server" CommandName="Cancel" CssClass="btn btn-link btn-xs" Text="Cancelar" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <%--<asp:Button ID="BtnRowEdit" runat="server" CommandName="Edit" CssClass="btn btn-link btn-xs" Text="Edit" />--%>
                                            <asp:LinkButton ID="BtnRowEdit" runat="server" CommandName="Edit" CssClass="btn btn-link btn-xs" Text="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton>
                                            <asp:Button ID="BtnAddAUE" runat="server" CommandName="AddNewChild" CssClass="btn btn-link btn-xs" Text="Add AUE" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn CancelImageUrl="Cancel.gif" InsertImageUrl="Update.gif" UpdateImageUrl="Update.gif">
                                    </EditColumn>
                                </EditFormSettings>
                                <CommandItemTemplate>
                                    <p class="pull-right">
                                        <asp:Button ID="BtnProdAdd" runat="server" class="btn btn-success btn-sm" CommandName="PerformInsert" Text="Guardar" />
                                        <asp:Button ID="BtnClean" runat="server" class="btn btn-default btn-sm" CommandName="Cancel" Text="Limpar" />
                                    </p>
                                </CommandItemTemplate>
                            </MasterTableView>
                            <FilterMenu EnableEmbeddedBaseStylesheet="False" EnableEmbeddedSkins="False">
                            </FilterMenu>
                            <HeaderContextMenu EnableEmbeddedBaseStylesheet="False" EnableEmbeddedSkins="False">
                            </HeaderContextMenu>
                        </telerik:RadGrid>
                    </telerik:RadAjaxPanel>


                    <asp:SqlDataSource ID="srcPropProducts" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM VwProposalDetails WHERE (TenantId = @TenantId) AND (OppId = @OpportunityId) AND (PropId IS NULL OR PropId = @PropId)" UpdateCommand="UPDATE [ProposalDetails] SET  ProductId=@ProductId, Quantity=@TxtPropQuantity, FreeQty=@FreeQty, PackageQty=@PackageQty, Discount=@Discount, DeliveryMonths=@DeliveryMonths, ModifiedDate= GETDATE(), ModifiedUserId=@ModifiedUserId Where TenandId = @TenantId AND Id=@Id">
                        <SelectParameters>
                            <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                            <asp:QueryStringParameter Name="OpportunityId" QueryStringField="OI" Type="Int32" />
                            <asp:QueryStringParameter Name="PropId" QueryStringField="PI" />
                        </SelectParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="ProductId" />
                            <asp:Parameter Name="FreeQty" />
                            <asp:Parameter Name="PackageQty" />
                            <asp:Parameter Name="Discount" />
                            <asp:Parameter Name="DeliveryMonths" />
                            <asp:SessionParameter Name="TenantId" SessionField="TenantId" />
                            <asp:Parameter Name="Id" />
                            <asp:SessionParameter Name="ModifiedUserId" SessionField="UserId" />
                            <asp:Parameter Name="TxtPropQuantity" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="srcProductsList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT 0 AS Id, '(Choose one)' AS Name UNION SELECT Id, Name FROM Products WHERE (TenantId = @TenantId)  ORDER BY Name">
                        <SelectParameters>
                            <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>



                    <asp:SqlDataSource ID="srcOrderRequests" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM VwOrderRequests WHERE (TenantId = @TenantId) AND (ProposalId = @ProposalId) AND ProductId=@ProductId">
                        <SelectParameters>
                            <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                            <asp:QueryStringParameter Name="ProposalId" QueryStringField="PI" />
                            <asp:ControlParameter ControlID="GdPropProducts" Name="ProductId" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>



                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" SelectCommand="SELECT * FROM [VwProductDetails] WHERE (([TenantId] = @TenantId) AND ([ProposalId] = @ProposalId) AND ([ProductId] = @ProductId))" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>">
                        <SelectParameters>
                            <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                            <asp:QueryStringParameter Name="ProposalId" QueryStringField="PI" Type="Int32" />
                            <asp:Parameter Name="ProductId" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>



                    <asp:SqlDataSource ID="srcProductShow" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM VwProposalDetails WHERE (TenantId = @TenantId) AND (OppId = @OpportunityId) AND (PropId IS NULL OR PropId = @PropId) AND ProductId&gt;0">
                        <SelectParameters>
                            <asp:SessionParameter Name="TenantId" SessionField="TenantId" />
                            <asp:QueryStringParameter Name="OpportunityId" QueryStringField="OI" />
                            <asp:QueryStringParameter Name="PropId" QueryStringField="PI" />
                        </SelectParameters>
                    </asp:SqlDataSource>



                    <asp:SqlDataSource ID="srcInvoices" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [Invoices] WHERE (([TenantId] = @TenantId) AND ([ProposalId] = @ProposalId) AND ([OrderRequestId] = @OrderRequestId))">
                        <SelectParameters>
                            <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                            <asp:QueryStringParameter Name="ProposalId" QueryStringField="PI" Type="Int32" />
                            <asp:ControlParameter ControlID="GdPropProducts" Name="OrderRequestId" PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>

                    <small>

                        <asp:SqlDataSource ID="srcDetailStatus" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT 0 AS Id, '(Estado)' AS Description UNION
SELECT Id,Description FROM [ProposalDetailStatus]
Order By Id"></asp:SqlDataSource>

                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GdEditProducts" runat="server" AutoGenerateColumns="False" CssClass="table" DataSourceID="srcPropProducts" GridLines="None" ShowFooter="True" Width="100%" DataKeyNames="OppDetailId,PropDetailId">
                                    <Columns>
                                        <asp:BoundField DataField="Position" HeaderText="Pos" SortExpression="Position" ReadOnly="True" />
                                        <asp:BoundField DataField="ExternalCode" HeaderText="Ref. Cliente" SortExpression="ExternalCode" ReadOnly="True" />
                                        <asp:TemplateField HeaderText="Produto" SortExpression="ProductName" InsertVisible="False">
                                            <ItemTemplate>
                                                <p>
                                                    <strong>
                                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("ProductName") %>'></asp:Label>
                                                        <asp:Label ID="LbProductId" Visible="false" runat="server" Text='<%# Bind("TxtOppProductId")%>'></asp:Label>
                                                        <asp:Label ID="LbOppPropDetailId" Visible="false" runat="server" Text='<%# Bind("PropDetailId")%>'></asp:Label>

                                                    </strong>
                                                </p>
                                                <p>
                                                    <h6>
                                                        <small>
                                                            <small>
                                                                <span>AIM <%# Eval("AIMCode")%></span>
                                                                |
                                                            <span>SAP <%# Eval("ERPCode")%></span>
                                                            </small>
                                                        </small>
                                                    </h6>
                                                </p>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qt Oportunidade" SortExpression="TxtOppQuantity">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("TxtOppQuantity") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Preço Oportunidade" SortExpression="TxtProductPrice">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("TxtProductPrice") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Quantidade" SortExpression="TxtPropQuantity">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TxtEditPropQuantity" runat="server" Text='<%# Bind("TxtPropQuantity")%>' CssClass="form-control input-sm gridMediumbox" TextMode="Number" min="0"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="LbPropQuantity" runat="server" Text='<%# Bind("TxtPropQuantity") %>'></asp:Label>
                                                <asp:TextBox ID="TxtPropQuantity" runat="server" CssClass="form-control input-sm gridMediumbox" TextMode="Number" min="0"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Preço" SortExpression="TxtProposalPrice">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TxtEditPropPrice" runat="server" Text='<%# Bind("TxtProposalPrice") %>' CssClass="form-control input-sm gridMediumbox" min="0" step="any"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="LbPropPrice" runat="server" Text='<%# Bind("TxtProposalPrice") %>'></asp:Label>
                                                <asp:TextBox ID="TxtPropPrice" runat="server" CssClass="form-control input-sm gridMediumbox" TextMode="Number" min="0"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Validade" SortExpression="Validity">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TxtEditPropValidity" runat="server" Text='<%# Bind("Validity") %>' CssClass="form-control input-sm gridsmallbox" TextMode="Number" min="0"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <span class="text-right">
                                                            <asp:LinkButton ID="BtnSave" runat="server" class="btn btn-success btn-xs" CommandName="BulkSave" Text="Guardar" />
                                                            <asp:LinkButton ID="BtnClean" runat="server" class="btn btn-default btn-xs" CommandName="Cancel" Text="Limpar" />
                                                        </span>
                                                    </div>

                                                </div>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="LbPropValidity" runat="server" Text='<%# Bind("Validity") %>'></asp:Label>
                                                <asp:TextBox ID="TxtPropValidity" runat="server" CssClass="form-control input-sm gridsmallbox" TextMode="Number" min="0"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <FooterTemplate>
                                                <div class="col-xs-12">
                                                    <span>
                                                        <asp:LinkButton ID="BtnBulkRowSave" runat="server" class="btn btn-primary btn-xs" CommandName="BulkStatusSave">Guardar</asp:LinkButton>
                                                        <asp:LinkButton ID="BtnCancelRow" runat="server" class="btn btn-default btn-xs">Limpar</asp:LinkButton>
                                                    </span>
                                                </div>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <div class="col-md-6">
                                                    <asp:Label ID="LbDetailStatusDesc" runat="server" Text='<%# Eval("PropDetailStatusDesc") %>'></asp:Label>
                                                    <asp:DropDownList ID="CbDetailStatus" runat="server" CssClass=" form-control input-smbtn btn-default dropdown-toggle txtMediumBox" DataSourceID="srcDetailStatus" DataTextField="Description" DataValueField="Id">
                                                    </asp:DropDownList>
                                                </div>
                                                <%--<div class="col-xs-6">
                                                    <asp:LinkButton ID="BtnAddRowStatus" runat="server" CommandName="RowStatusUpdate" CommandArgument='<%# Eval("PropDetailId")%>' OnClick="BtnAddRowStatus_Click" Visible="False">Add</asp:LinkButton>
                                                </div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <EditItemTemplate>
                                                <asp:LinkButton ID="BtnRowSave" runat="server" CausesValidation="True" CommandName="Update" Text=""><span class="glyphicon glyphicon-floppy-save"></span></asp:LinkButton>
                                                <asp:LinkButton ID="BtnRowClean" runat="server" CausesValidation="False" CommandName="Cancel" Text=""><span class="glyphicon glyphicon-ban-circle"></asp:LinkButton>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="BtnEdit" runat="server" CausesValidation="False" CommandName="Edit" Text=""><span class="glyphicon glyphicon-pencil"></asp:LinkButton>
                                                <span id="RowImgOk" runat="server" visible="false" class="glyphicon glyphicon-ok green"></span>
                                                <span id="RowImgProblem" runat="server" visible="false" class="glyphicon glyphicon-remove red"></span>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>



                            </ContentTemplate>
                        </asp:UpdatePanel>


                    </small>


                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="GdProdContract" runat="server" AutoGenerateColumns="False" CssClass="table" DataSourceID="srcProductShow" Width="100%" BorderStyle="None" GridLines="None" DataKeyNames="TxtOppProductId,PropDetailId" Visible="False">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="col-md-12">

                                                        <p>

                                                            <strong>
                                                                <span><%# Eval("Position")%></span>
                                                                |
                                                                    <span><%# Eval("TxtProductName")%></span>

                                                                <h6><small><strong>ERP: <span><%# Eval("ERPCode")%></span> | AIM: <span><%# Eval("AIMCode")%></span>| External: <span><%# Eval("ExternalCode")%></span></strong></small></h6>
                                                            </strong>

                                                            <p>
                                                            </p>
                                                            <p>
                                                                <h6><small><strong>Oportunidade: <span><%# Eval("TxtOppQuantity")%>Un</span></strong></small></h6>
                                                                <h6><small><strong>Proposta Un: <span><%# Eval("TxtPropQuantity")%></span>Un | (<span><%# Eval("TxtProposalPrice")%></span>€) | <span><%# Eval("Validity")%></span>&nbsp;Ano(s)</strong></small></h6>
                                                                <p>
                                                                </p>
                                                                <p>
                                                                </p>
                                                                <p>
                                                                </p>
                                                            </p>

                                                        </p>


                                                    </div>
                                                    <div class="col-md-12">

                                                        <small>
                                                            <asp:GridView ID="GdProdRequests" runat="server" CssClass="table" AutoGenerateColumns="False" Width="100%" GridLines="None" OnRowDataBound="GridView2_RowDataBound" OnRowCommand="GdProdRequests_RowCommand" OnDataBound="GdProdRequests_DataBound" OnRowCancelingEdit="GdProdRequests_RowCancelingEdit">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Tipo">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label5" runat="server" Text='<%# Eval("lineType") %>'></asp:Label>
                                                                            <asp:Label ID="LbProductId" runat="server" Text="Label" Visible="False"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Id">
                                                                        <FooterTemplate>
                                                                            <small>
                                                                                <asp:TextBox ID="TxtRequestMaster" runat="server" CssClass="form-control input-sm gridsmallbox"></asp:TextBox>
                                                                            </small>
                                                                        </FooterTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label8" runat="server" Text='<%# Eval("RequestMaster") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%--<asp:TemplateField HeaderText="Slave">
                                                                        <FooterTemplate>
                                                                            <small>
                                                                                <asp:TextBox ID="TxtRequestSlave" runat="server" CssClass="form-control input-sm gridsmallbox"></asp:TextBox>
                                                                            </small>
                                                                        </FooterTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label7" runat="server" Text='<%# Eval("RequestSlave") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                                    <asp:TemplateField HeaderText="Descrição">
                                                                        <FooterTemplate>
                                                                            <asp:TextBox ID="TxtRequestDescription" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                                        </FooterTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("RequestDescription") %>'></asp:Label>
                                                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("CustomerName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="RequestDescription" HeaderText="RequestDescription" SortExpression="RequestDescription" Visible="False" />
                                                                    <asp:BoundField DataField="CustomerName" HeaderText="CustomerName" SortExpression="CustomerName" Visible="False" />
                                                                    <asp:TemplateField HeaderText="Quantidade" SortExpression="Quantity">
                                                                        <FooterTemplate>

                                                                            <asp:TextBox ID="TxtRequestQuantity" runat="server" CssClass="form-control input-sm gridsmallbox" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                                                                        </FooterTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Data">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label9" runat="server" Text='<%# Eval("TxtDate") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="#">
                                                                        <FooterTemplate>
                                                                            <small>
                                                                                <asp:LinkButton ID="BtnProdAdd" runat="server" CommandName="InsertRequest" class="btn btn-success btn-sm" Text="Guardar"></asp:LinkButton>
                                                                                <%--<asp:LinkButton ID="BtnProdAdd1" runat="server" class="btn btn-success btn-sm" CommandName="InsertRequest" Text="Guardar" />--%>
                                                                                <asp:LinkButton ID="BtnClean" runat="server" class="btn btn-default btn-sm" CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                                                                                <%--<asp:Button ID="BtnClean" runat="server" class="btn btn-default btn-sm" CommandName="Cancel" Text="Limpar" />--%>
                                                                            </small>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            <span class="text-left">
                                                                <asp:LinkButton ID="BtnNewRequest" runat="server" CssClass="btn btn-primary btn-outline btn-sm btn-block" OnClick="BtnNewRequest_Click">Novo Pedido</asp:LinkButton>

                                                            </span>
                                                        </small>
                                                    </div>


                                                </div>
                                                <div class="col-md-2">
                                                    <small>

                                                        <%--<h6 class="text-center"><strong>Informações Produto</strong></h6>--%>
                                                        <%--<p class="form-control-static">
                                                            <strong>ERP: </strong>
                                                            <span><%# Eval("ERPCode")%></span>
                                                        </p>
                                                        <p class="form-control-static">
                                                            <strong>AIM: </strong>
                                                            <span><%# Eval("AIMCode")%></span>
                                                        </p>--%>

                                                        <%-- OUT --%>

                                                        <div class="row">
                                                            <div class="row" id="DivProdOut" runat="server">
                                                                <h6 id="RequestBlock" runat="server"><strong>Qt AUE: </strong>
                                                                    <asp:Label ID="LbTotalRequests" runat="server" Text="Label"></asp:Label>
                                                                    Un</h6>
                                                                <%--<h6 class="text-center"><strong>Saidas</h6>--%>

                                                                <h6 id="InvoiceBlock" runat="server"><strong>Qt Saidas : </strong><span class="">
                                                                    <asp:Label ID="LbTotalOut" runat="server" Text="Label"></asp:Label>
                                                                    Un</span></h6>
                                                                <h6 id="BlockStock" runat="server"><strong>Stock AUE: </strong>
                                                                    <asp:Label ID="LbStockRequests" runat="server" Text="Label"></asp:Label>
                                                                    Un</h6>
                                                                <div class="progress" id="Progress1" runat="server">
                                                                    <div id="ProgressBar1" runat="server" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="150" aria-valuemin="0" aria-valuemax="300" style="width: 50%;">
                                                                        <strong><small>
                                                                            <asp:Label ID="LbTotalOutRequests" runat="server" Text="Label"></asp:Label>
                                                                            Un</small></strong>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <%-- End Block Out --%>

                                                            <%-- VALUES --%>
                                                            <%--<div class="row" id="DivProdValues" runat="server">
                                                                <h6 class="text-center"><strong>Valores</strong></h6>
                                                                <h6><strong>Oportunidade :</strong>
                                                                    <asp:Label ID="LbOppTotal" runat="server" Text="Label"></asp:Label>
                                                                    €</h6>
                                                                <h6><strong>Proposto :</strong>
                                                                    <asp:Label ID="LbPropTotal" runat="server" Text="Label"></asp:Label>
                                                                    €</h6>
                                                                <div class="progress" id="Progress2" runat="server">
                                                                    <div id="ProgressBar2" runat="server" class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="1200" aria-valuemin="0" aria-valuemax="2400" style="width: 50%;">
                                                                        <strong><small>
                                                                            <asp:Label ID="LbPropOutTotal" runat="server" Text="Label"></asp:Label>
                                                                            €</small></strong>
                                                                    </div>
                                                                </div>
                                                                <h6><strong>Margem : </strong><span class="">
                                                                    <asp:Label ID="LbOppVsProp" runat="server" Text="Label"></asp:Label></span></h6>
                                                                <h6><strong>Diferença: </strong><span class="">
                                                                    <asp:Label ID="LbPropVsOut" runat="server" Text="Label"></asp:Label>
                                                                    €</span></h6>

                                                            </div>--%>
                                                            <div class="row" id="DivRefusalInfo" runat="server">

                                                                <p>
                                                                    <h6 class="text-center"><strong>Valor Recusa :  <span>
                                                                        <asp:Label ID="LbRefusalPrice" runat="server" Text='<%# Eval("RefusalPrice") %>'></asp:Label></strong>
                                                                        <asp:LinkButton ID="BtnEditPrice" runat="server" CssClass="btn btn-link btn-xs" Text="" OnClick="BtnEditPrice_Click"><span class="glyphicon glyphicon-pencil"></asp:LinkButton></h6>
                                                                    </strong>
                                                            <p>
                                                            </p>
                                                                <p>
                                                                    <asp:TextBox ID="TxtRefusalPrice" runat="server" CssClass="form-control input-sm" Text='<%# Bind("RefusalPrice") %>'></asp:TextBox>
                                                                </p>
                                                                <p>
                                                                    <asp:LinkButton ID="BtnRefusalSave" runat="server" class="btn btn-primary btn-sm btn-block" CommandName="RowSave">Guardar</asp:LinkButton>
                                                                </p>

                                                            </div>




                                                        </div>
                                                        <%-- End Block Values --%>

                                                    </small>
                                                </div>
                                            </div>


                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <div class="col-md-12">
                    <div id="DivPropTask" runat="server" class="alert alert-warning">
                        <div class="nav nav-pills nav-stacked col-md-12">
                            <div id="TaskAlert" class="col-md-12">
                                <asp:Label ID="LbTaskMessage" runat="server" Text="Label"></asp:Label>
                                <strong>
                                    <asp:Label ID="LbTaskDescription" runat="server" Text="Label" Visible="False"></asp:Label></strong>
                            </div>
                            <div id="TaskConfirm" class="col-md-12">
                                <asp:CheckBox ID="ChkConfirmTask" runat="server" Text="Confirme para avançar  " TextAlign="Left" Visible="False" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="row">

        <div class="row" id="DivTaskList" runat="server">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Tarefas
                    </div>
                    <div class="panel-body">
                        <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" Width="100%">
                            <telerik:RadGrid ID="GdPropTask" runat="server" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AutoGenerateColumns="False" CssClass="table table-striped" DataSourceID="srcProposalTasks" ShowHeader="False" EnableEmbeddedBaseStylesheet="False" EnableEmbeddedSkins="False" Style="outline: none; margin-top: 0px;" EnableViewState="False">
                                <ExportSettings>
                                    <Pdf PageWidth="">
                                    </Pdf>
                                </ExportSettings>
                                <ClientSettings>
                                    <Scrolling AllowScroll="True" ScrollHeight="" />
                                    <Resizing AllowResizeToFit="False" />
                                </ClientSettings>
                                <MasterTableView CommandItemDisplay="Top" DataKeyNames="Id" DataSourceID="srcProposalTasks" EditMode="InPlace" AllowAutomaticInserts="False" AllowAutomaticUpdates="False" NoMasterRecordsText="">
                                    <CommandItemSettings ShowAddNewRecordButton="False" ShowRefreshButton="False" />
                                    <RowIndicatorColumn Visible="False">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Created="True">
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" UniqueName="TemplateColumn">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChSelectTask" runat="server" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="15px" />
                                            <ItemStyle Width="15px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn FilterControlAltText="Filter Teste column" UniqueName="Item">
                                            <EditItemTemplate>
                                                <div class="">
                                                    <div class="col-md-12">
                                                        <h5><strong>
                                                            <asp:DropDownList ID="CbProposalList" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" Visible="False">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="LbMode" runat="server"></asp:Label>
                                                            <div class="input-group custom-search-form">
                                                                <asp:TextBox ID="TxtEditTaskDescription" runat="server" CssClass="form-control input-sm txtBigBox" MaxLength="200" Text='<%# Bind("TxtTaskDescription") %>'></asp:TextBox>
                                                                <asp:LinkButton ID="BtnTaskSearch" runat="server" CommandName="ListSearch" CssClass="btn btn-default btn-sm"><i class="fa fa-search"></i></asp:LinkButton>
                                                            </div>
                                                            <%--<asp:TextBox ID="TxtEditTaskDescription1" runat="server" CssClass=" txtBigBox input-sm" Text='<%# Bind("TxtTaskDescription") %>'></asp:TextBox>--%>
                                                        </strong><small>
                                                            <asp:Label ID="LbStatus" runat="server" Text='<%# Eval("TaskStatusId") %>'></asp:Label>
                                                        </small>
                                                            <h5></h5>
                                                        </h5>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small>
                                                            <p class="text-muted">
                                                                Inicio:
                                                                    <asp:TextBox ID="TxtEditStartDate" runat="server" CssClass="input-sm txtMediumBox" Text='<%# Bind("TxtStartDate", "{0:yyyy-MM-dd}")%>' TextMode="Date"></asp:TextBox>
                                                                | Fim:
                                                                    <asp:TextBox ID="TxtEditEndDate" runat="server" CssClass="input-sm txtMediumBox" Text='<%# Bind("TxtEndDate")%>' TextMode="Date"></asp:TextBox>
                                                            </p>
                                                            <p class="">
                                                                <asp:Button ID="BtnUpdate" runat="server" CommandName="Update" CssClass="btn btn-outline btn-link btn-xs" Text="Guardar" />
                                                                <asp:Button ID="BtnCancel" runat="server" CommandName="Cancel" CssClass="btn btn-outline btn-link btn-xs" Text="Cancelar" />
                                                            </p>
                                                        </small>
                                                    </div>
                                                </div>
                                            </EditItemTemplate>
                                            <InsertItemTemplate>
                                                <div class="">
                                                    <div class="col-md-12">
                                                        <h5><strong>
                                                            <asp:DropDownList ID="CbProposalList" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" Visible="False">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="LbInsertMode" runat="server"></asp:Label>
                                                            <div class="input-group custom-search-form">
                                                                <asp:TextBox ID="TxtInsertTaskDescription" runat="server" CssClass="form-control input-sm txtBigBox" MaxLength="200" Text=""></asp:TextBox>

                                                                <asp:LinkButton ID="BtnTaskSearch" runat="server" CommandName="ListSearch" CssClass="btn btn-default btn-sm"><i class="fa fa-search"></i></asp:LinkButton>
                                                            </div>
                                                            <%--<asp:TextBox ID="TxtTaskDescriptionTextBox" runat="server" CssClass=" txtBigBox input-sm" Text='<%# Bind("TxtTaskDescription") %>'></asp:TextBox>--%>
                                                        </strong><small><%--<asp:Label ID="LbStatus" runat="server" Text="-"></asp:Label></small>--%></small>
                                                            <h5>&nbsp;</h5>
                                                            <h5></h5>
                                                        </h5>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small>
                                                            <p class="text-muted">
                                                                Inicio:
                                                                    <asp:TextBox ID="TxtStartDateTextBox" runat="server" CssClass="input-sm txtMediumBox" Text='<%# Bind("TxtStartDate") %>' TextMode="Date"></asp:TextBox>
                                                                | Fim:
                                                                    <asp:TextBox ID="TxtEndDateTextBox" runat="server" CssClass="input-sm txtMediumBox" Text='<%# Bind("EndDate") %>' TextMode="Date"></asp:TextBox>
                                                            </p>
                                                            <p class="">
                                                                <asp:Button ID="BtnInsert" runat="server" CommandName="PerformInsert" CssClass="btn btn-outline btn-link btn-xs" Text="Inserir" />
                                                                <asp:Button ID="BtnCancel" runat="server" CommandName="Cancel" CssClass="btn btn-outline btn-link btn-xs" Text="Cancelar" />
                                                            </p>
                                                        </small>
                                                    </div>
                                                </div>
                                            </InsertItemTemplate>
                                            <ItemTemplate>
                                                <div class="">
                                                    <div class="col-md-12">
                                                        <h5><strong>
                                                            <asp:Label ID="LbDescription" runat="server" Text='<%# Eval("TxtTaskDescription") %>'></asp:Label>
                                                            <asp:Label ID="LbListDescription" runat="server" Text='<%# Eval("TxtTaskListName") %>'></asp:Label>
                                                        </strong><small>
                                                            <asp:Label ID="LbStatus" runat="server" Text="-"></asp:Label>
                                                        </small></h5>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small>
                                                            <p class="text-muted">
                                                                Inicio:
                                                                    <asp:Label ID="LbStartDate" runat="server" Text='<%# Eval("TxtStartDate") %>'></asp:Label>
                                                                | Fim:
                                                                    <asp:Label ID="LbEndDate" runat="server" Text='<%# Eval("TxtEndDate") %>'></asp:Label>
                                                            </p>
                                                            <p class="">
                                                                <asp:Button ID="BtnInvalid" runat="server" CommandName="InvalidTask" CssClass="btn btn-outline btn-danger btn-xs" Text="Anular" />
                                                                <asp:Button ID="BtnClose" runat="server" CommandName="CloseTask" CssClass="btn btn-outline btn-success btn-xs" Text="Fechar" />
                                                                <asp:Button ID="BtnViewEdit" runat="server" CommandName="Edit" CssClass="btn btn-link btn-xs" Text="Editar" />
                                                            </p>
                                                        </small>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn FilterControlAltText="Filter LabelCode column" HeaderText="Label" UniqueName="LabelCode" Visible="False">
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter Id column" HeaderText="Id" ReadOnly="True" SortExpression="Id" UniqueName="Id" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TenantId" DataType="System.Int32" FilterControlAltText="Filter TenantId column" HeaderText="TenantId" ReadOnly="True" SortExpression="TenantId" UniqueName="TenantId" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ProposalStatusId" DataType="System.Int32" FilterControlAltText="Filter ProposalStatusId column" HeaderText="ProposalStatusId" ReadOnly="True" SortExpression="ProposalStatusId" UniqueName="ProposalStatusId" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TxtProposalStatus" FilterControlAltText="Filter TxtProposalStatus column" HeaderText="PropStatus" ReadOnly="True" SortExpression="TxtProposalStatus" UniqueName="TxtProposalStatus" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ProposalId" DataType="System.Int32" FilterControlAltText="Filter ProposalId column" HeaderText="ProposalId" ReadOnly="True" SortExpression="ProposalId" UniqueName="ProposalId" Visible="False">
                                            <ColumnValidationSettings>
                                                <ModelErrorMessage Text="" />
                                            </ColumnValidationSettings>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn DataField="TxtTaskDescription" DataType="System.Int32" FilterControlAltText="Filter TxtTaskDescription column" HeaderText="Description" SortExpression="TxtTaskDescription" UniqueName="TxtTaskDescription" Visible="False">
                                            <EditItemTemplate>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="TxtTaskDescriptionLabel" runat="server" Text='<%# Eval("TxtTaskDescription") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="TaskStatusId" DataType="System.Int32" FilterControlAltText="Filter TaskStatusId column" HeaderText="TaskStatusId" ReadOnly="True" SortExpression="TaskStatusId" UniqueName="TaskStatusId" Visible="False">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TaskStatusIdTextBox" runat="server" Text='<%# Bind("TaskStatusId") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="TaskStatusIdLabel" runat="server" Text='<%# Eval("TaskStatusId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="TxtTaskStatus" FilterControlAltText="Filter TxtTaskStatus column" HeaderText="TaskStatus" SortExpression="TxtTaskStatus" UniqueName="TxtTaskStatus" Visible="False">
                                            <InsertItemTemplate>
                                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="btn btn-default dropdown-toggle smallBox" DataSourceID="srcPropTaskStatusList" DataTextField="Description" DataValueField="Id">
                                                </asp:DropDownList>
                                            </InsertItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="TxtTaskStatusLabel" runat="server" Text='<%# Eval("TxtTaskStatus") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="TxtStartDate" FilterControlAltText="Filter TxtStartDate column" HeaderText="StartDate" SortExpression="TxtStartDate" UniqueName="TxtStartDate" Visible="False">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TxtStartDateTextBox" runat="server" CssClass="input-sm txtMediumBox" Text='<%# Bind("StartDate", "{0:d}") %>' TextMode="Date"></asp:TextBox>
                                            </EditItemTemplate>
                                            <InsertItemTemplate>
                                            </InsertItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="TxtStartDateLabel" runat="server" Text='<%# Eval("TxtStartDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="TxtEndDate" FilterControlAltText="Filter TxtEndDate column" HeaderText="EndDate" SortExpression="TxtEndDate" UniqueName="TxtEndDate" Visible="False">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TxtEndDateTextBox" runat="server" CssClass="input-sm txtMediumBox" Text='<%# Bind("EndDate", "{0:d}") %>' TextMode="Date"></asp:TextBox>
                                            </EditItemTemplate>
                                            <InsertItemTemplate>
                                            </InsertItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="TxtEndDateLabel" runat="server" Text='<%# Eval("TxtEndDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn CancelImageUrl="Cancel.gif" InsertImageUrl="Update.gif" UpdateImageUrl="Update.gif">
                                        </EditColumn>
                                    </EditFormSettings>
                                    <CommandItemTemplate>
                                        <div id="DivNewTask" runat="server">
                                            <div class="dropdown btn-sm">
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Acções <b class="caret"></b></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <asp:Button ID="BtnAddTask" runat="server" class="btn btn-link btn-sm" OnClick="BtnAddTask_Click" Text="Nova Tarefa" />
                                                    </li>
                                                    <%--<li><a href="#"></a></li>--%><%--<li><a href="#">Something else here</a></li>--%>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <asp:Button ID="BtnCloseAll" runat="server" class="btn btn-link btn-sm" Text="Fechar Selecionadas" />
                                                    </li>
                                                    <%-- <li class="divider"></li>
                                                    <li><a href="#">One more separated link</a></li>--%>
                                                </ul>
                                            </div>
                                        </div>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <FilterMenu EnableEmbeddedBaseStylesheet="False" EnableEmbeddedSkins="False">
                                </FilterMenu>
                                <HeaderContextMenu EnableEmbeddedBaseStylesheet="False" EnableEmbeddedSkins="False">
                                </HeaderContextMenu>
                            </telerik:RadGrid>
                        </telerik:RadAjaxPanel>
                        <asp:SqlDataSource ID="srcProposalTasks" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" DeleteCommand="DELETE FROM [ProposalTasks] WHERE [Id] = @Id" InsertCommand="INSERT INTO [ProposalTasks] ([TenantId], [ProposalStatusId], [ProposalId], [Description], [TaskStatusId], [StartDate], [EndDate], [CreateDate], [CreateUserId], [ModifiedDate], [ModifiedUserId]) VALUES (@TenantId, @ProposalStatusId, @ProposalId, @Description, @TaskStatusId, @StartDate, @EndDate, @CreateDate, @CreateUserId, @ModifiedDate, @ModifiedUserId)" SelectCommand="SELECT * FROM [VwProposalTasks] WHERE (([TenantId] = @TenantId) AND ([ProposalId] = @ProposalId))" UpdateCommand="UPDATE [ProposalTasks] SET  [Description] = @TxtTaskDescription, [StartDate] = @TxtStartDate, [EndDate] = @TxtEndDate, [ModifiedDate] =GETDATE(), [ModifiedUserId] = @ModifiedUserId WHERE [Id] = @Id And TenantId=@TenantId">
                            <DeleteParameters>
                                <asp:Parameter Name="Id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="TenantId" Type="Int32" />
                                <asp:Parameter Name="ProposalStatusId" Type="Int32" />
                                <asp:Parameter Name="ProposalId" Type="Int32" />
                                <asp:Parameter Name="Description" Type="Int32" />
                                <asp:Parameter Name="TaskStatusId" Type="Int32" />
                                <asp:Parameter Name="StartDate" Type="DateTime" />
                                <asp:Parameter Name="EndDate" Type="DateTime" />
                                <asp:Parameter Name="CreateDate" Type="DateTime" />
                                <asp:Parameter Name="CreateUserId" Type="Int32" />
                                <asp:Parameter Name="ModifiedDate" Type="DateTime" />
                                <asp:Parameter Name="ModifiedUserId" Type="Int32" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                <asp:QueryStringParameter Name="ProposalId" QueryStringField="PI" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                <asp:Parameter Name="ModifiedDate" Type="DateTime" />
                                <asp:SessionParameter Name="ModifiedUserId" SessionField="UserId" Type="Int32" />
                                <asp:Parameter Name="Id" Type="Int32" />
                                <asp:Parameter Name="TxtTaskDescription" />
                                <asp:Parameter Name="TxtStartDate" />
                                <asp:Parameter Name="TxtEndDate" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="srcPropTaskStatusList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [ProposalTaskStatus]"></asp:SqlDataSource>


                    </div>

                </div>
            </div>
        </div>
        <div class="row">

            <div class="row" id="PanelReports" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Reports
                        </div>
                        <div class="panel-body">
                            <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" CssClass="" AutoDataBind="true" />
                            <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
                                <Report FileName="CrystalReport1.rpt"></Report>
                            </CR:CrystalReportSource>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        </telerik:RadAjaxManagerProxy>
    </div>
</asp:Content>
