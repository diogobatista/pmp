﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ChildMaster.master" CodeBehind="Products.aspx.vb" Inherits="pmp_V3.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ul class="nav" id="side-menu">
        <li class="sidebar-search">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            <!-- /input-group -->
        </li>
        <li role="presentation" class="divider"></li>
        <li><a href="#">Novo</a></li>
        <li><a href="#">...</a></li>
        <li><a href="#">...</a></li>
        <li><a href="#">...</a></li>
    </ul>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Produtos</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <small>
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table" DataKeyNames="Id" DataSourceID="srcProductsList" GridLines="None" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                                <asp:BoundField DataField="ERPCode" HeaderText="ERP" SortExpression="ERPCode" />
                                <asp:BoundField DataField="AIMCode" HeaderText="AIM" SortExpression="AIMCode" />
                                <asp:BoundField DataField="Name" HeaderText="Nome" SortExpression="Name" />
                                <asp:BoundField DataField="ActiveIngredientName" HeaderText="Ing. Activo" SortExpression="ActiveIngredientName" />
                                <asp:BoundField DataField="TxtExpiracyDate" HeaderText="Validade" SortExpression="ExpiracyDate" />
                            </Columns>
                        </asp:GridView>
                            </small>
                        <telerik:RadGrid ID="GdProductList"  visible="False" CssClass="table table-striped table-bordered table-hover" runat="server" AutoGenerateColumns="False" DataSourceID="srcProductsList" EnableEmbeddedSkins="False" Width="100%" EnableAjaxSkinRendering="False" EnableEmbeddedBaseStylesheet="False" EnableEmbeddedScripts="False" EnableTheming="True">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" ScrollHeight="" />
                                <Resizing AllowResizeToFit="True" />
                            </ClientSettings>
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="srcProductsList">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter Id column" HeaderText="Id" ReadOnly="True" SortExpression="Id" UniqueName="Id">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TenantId" DataType="System.Int32" FilterControlAltText="Filter TenantId column" HeaderText="TenantId" SortExpression="TenantId" UniqueName="TenantId" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ERPCode" FilterControlAltText="Filter ERPCode column" HeaderText="ERP" SortExpression="ERPCode" UniqueName="ERPCode">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AIMCode" FilterControlAltText="Filter AIMCode column" HeaderText="AIM" SortExpression="AIMCode" UniqueName="AIMCode">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Name" FilterControlAltText="Filter Name column" HeaderText="Nome" SortExpression="Name" UniqueName="Name">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ActiveIngredientName" FilterControlAltText="Filter ActiveIngredientName column" HeaderText="Ing. Activo" SortExpression="ActiveIngredientName" UniqueName="ActiveIngredientName">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="VATCode" FilterControlAltText="Filter VATCode column" HeaderText="VATCode" SortExpression="VATCode" UniqueName="VATCode" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DeliveryMonths" DataType="System.Int32" FilterControlAltText="Filter DeliveryMonths column" HeaderText="DeliveryMonths" SortExpression="DeliveryMonths" UniqueName="DeliveryMonths">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" DataType="System.DateTime" FilterControlAltText="Filter CreateDate column" HeaderText="CreateDate" SortExpression="CreateDate" UniqueName="CreateDate" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreateUserId" DataType="System.Int32" FilterControlAltText="Filter CreateUserId column" HeaderText="CreateUserId" SortExpression="CreateUserId" UniqueName="CreateUserId" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ModifiedDate" DataType="System.DateTime" FilterControlAltText="Filter ModifiedDate column" HeaderText="ModifiedDate" SortExpression="ModifiedDate" UniqueName="ModifiedDate" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ModifiedUserId" DataType="System.Int32" FilterControlAltText="Filter ModifiedUserId column" HeaderText="ModifiedUserId" SortExpression="ModifiedUserId" UniqueName="ModifiedUserId" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ExpiracyDate" DataType="System.DateTime" FilterControlAltText="Filter ExpiracyDate column" HeaderText="ExpiracyDate" SortExpression="ExpiracyDate" UniqueName="ExpiracyDate">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                </Columns>

                                <EditFormSettings>
                                    <EditColumn InsertImageUrl="Update.gif" UpdateImageUrl="Update.gif" CancelImageUrl="Cancel.gif"></EditColumn>
                                </EditFormSettings>
                            </MasterTableView>

                            <FilterMenu EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" EnableEmbeddedBaseStylesheet="False"></FilterMenu>

                            <HeaderContextMenu EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" EnableEmbeddedBaseStylesheet="False"></HeaderContextMenu>
                        </telerik:RadGrid>

                        <asp:SqlDataSource ID="srcProductsList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM VwProducts WHERE ([TenantId] = @TenantId)">
                            <SelectParameters>
                                <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>

                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
