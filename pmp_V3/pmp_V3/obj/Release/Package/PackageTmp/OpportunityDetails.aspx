﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ChildMaster.master" CodeBehind="OpportunityDetails.aspx.vb" Inherits="pmp_V3.WebForm11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ul class="nav" id="side-menu">
        <li id="LiAddProduct" runat="server">
            <asp:LinkButton ID="OppAddProducts" runat="server">Adicionar Produtos</asp:LinkButton>
        </li>
        <li id="LiAddProposal" runat="server">
            <asp:LinkButton ID="OppAddProposal" runat="server">Criar Proposta</asp:LinkButton>
        </li>
    </ul>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Oportunidades</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div id="AlertDiv" runat="server" class="alert alert-warning">
                    <div class="nav nav-pills nav-stacked col-md-12">
                        <div id="AlertLabel" class="col-md-12">
                            <asp:Label ID="LbAlert" runat="server" Text="Label"></asp:Label>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div class="row">
        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
            <ContentTemplate>

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body">
                            <div class="row col-md-12">
                                <asp:FormView ID="FrmOpportunity" CssClass="form-horizontal" runat="server" Width="100%" DataKeyNames="Id" DataSourceID="srcOpportunityDetails">
                                    <EditItemTemplate>
                                        <small>
                                            <div class="row col-md-12">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Cliente</label>
                                                        <p class="form-control-static">
                                                            <%# Eval("TxtCustomerName")%>
                                                            <%# Eval("TxtGroupName")%>
                                                        </p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Codigo</label>
                                                        <asp:TextBox ID="TxtPublicCode" CssClass="form-control input-sm txtMediumBox" runat="server" Text='<%# Bind("InternalId") %>'></asp:TextBox>

                                                        <%--<input class="form-control" id="TxtInsertDescription" runat="server">--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Data de Publicação</label>
                                                        <%-- <input class="form-control" id="TxtInsertPublicDate" runat="server">--%>
                                                        <asp:TextBox ID="TxtEditPublicDate" runat="server" CssClass="form-control input-sm txtMediumBox" TextMode="Date" Text='<%# Bind("PublicDate") %>'></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Descrição</label>
                                                        <asp:TextBox ID="TxtDescription" CssClass="form-control txtBigBox input-sm" runat="server" Text='<%# Bind("TxtOppName") %>'></asp:TextBox>
                                                        <%--<input class="form-control" id="TxtInsertPublicCode" runat="server">--%>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Tipo de Procedimento</label>
                                                        <%--<input class="form-control">--%>
                                                        <asp:DropDownList ID="CbOppType" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" DataSourceID="srcOppTypes" DataTextField="Name" DataValueField="Id" SelectedValue='<%# Bind("OportunityTypeId") %>'>
                                                        </asp:DropDownList>
                                                        <asp:SqlDataSource ID="srcOppTypes" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [OpportunityTypes] WHERE ([TenantId] = @TenantId)">
                                                            <SelectParameters>
                                                                <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                        <%--<p class="help-block">Example block-level help text here.</p>--%>
                                                    </div>
                                                    <%--                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Cliente</label>
                                                        
                                                        <small>
                                                            <asp:RadioButtonList ID="OptionCustomerSelection" runat="server" AutoPostBack="True" OnSelectedIndexChanged="OptionCustomerSelection_SelectedIndexChanged" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="0">Grupos</asp:ListItem>
                                                                <asp:ListItem Selected="True" Value="1">Clientes</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </small>
                                                    </div>--%>
                                                    <%--                                                 <div class="form-group">
                                                        <label class="col-sm-2 control-label"></label>
                                                        <small>
                                                            <asp:DropDownList ID="CbCustomer" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" OnInit="CbCustomer_Init">
                                                            </asp:DropDownList>
                                                        </small>
                                                        <asp:SqlDataSource ID="srcCustomersList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [Customers] WHERE ([TenantId] = @TenantId) ORDER BY Name">
                                                            <SelectParameters>
                                                                <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </div>--%>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Destino</label>
                                                        <asp:DropDownList ID="CbOrigins" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" DataSourceID="srcOrigins" DataTextField="Description" DataValueField="Id" SelectedValue='<%# Bind("OriginId") %>'>
                                                        </asp:DropDownList>
                                                        <asp:SqlDataSource ID="srcOrigins" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [OpportunityOrigins] WHERE ([TenantId] = @TenantId)">
                                                            <SelectParameters>
                                                                <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                        <%--<p class="help-block">Example block-level help text here.</p>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Valor Total</label>
                                                        <%-- <input type="number" class="form-control" id="TxtInsertTotalValue" min="0" runat="server">--%>
                                                        <asp:TextBox ID="TxtTotalValue" CssClass="form-control input-sm smallBox" runat="server" Text='<%# Bind("TotalValue") %>'></asp:TextBox>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Data Limite</label>
                                                        <%-- <input type="number" class="form-control" min="0" id="TxtInsertEndDate" runat="server">--%>
                                                        <asp:TextBox ID="TxtEndDate" CssClass="form-control input-sm txtMediumBox" runat="server" Text='<%# Bind("EndDate") %>' TextMode="Date"></asp:TextBox>

                                                    </div>


                                                    <div class="row">
                                                        <div class="text-left">
                                                            <asp:LinkButton ID="BtnUpdate" CssClass="btn btn-primary btn-xs" runat="server" Text="Guardar" CommandName="Update" />
                                                            <asp:LinkButton ID="BtnCancel" CssClass="btn btn-default btn-xs" runat="server" Text="Cancelar" CommandName="Cancel" />
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </small>
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        <small>
                                            <div class="row col-md-12">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Codigo</label>
                                                        <asp:TextBox ID="TxtPublicCode" CssClass="form-control input-sm txtMediumBox" runat="server" Text='<%# Bind("InternalId") %>' min="0"></asp:TextBox>

                                                        <%--<input class="form-control" id="TxtInsertDescription" runat="server">--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Data de Publicação</label>
                                                        <%-- <input class="form-control" id="TxtInsertPublicDate" runat="server">--%>
                                                        <small>
                                                            <asp:TextBox ID="TxtEditPublicDate" runat="server" CssClass="form-control input-sm txtMediumBox" min="0" Text='<%# Bind("PublicDate") %>' TextMode="Date"></asp:TextBox>
                                                        </small>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Descrição</label>
                                                        <asp:TextBox ID="TxtDescription" CssClass="form-control txtBigBox input-sm" runat="server" Text='<%# Bind("TxtOppName") %>'></asp:TextBox>
                                                        <%--<input class="form-control" id="TxtInsertPublicCode" runat="server">--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Tipo de Procedimento</label>
                                                        <%--<input class="form-control">--%>

                                                        <small>
                                                            <asp:DropDownList ID="CbOppType" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" DataSourceID="srcOppTypes" DataTextField="Name" DataValueField="Id" SelectedValue='<%# Bind("OportunityTypeId") %>'>
                                                            </asp:DropDownList>
                                                        </small>

                                                        <asp:SqlDataSource ID="srcOppTypes" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [OpportunityTypes] WHERE ([TenantId] = @TenantId)">
                                                            <SelectParameters>
                                                                <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Cliente</label>
                                                        <asp:RadioButtonList ID="OptionCustomerSelection" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="OptionCustomerSelection_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">Grupos</asp:ListItem>
                                                            <asp:ListItem Selected="True" Value="1">Clientes</asp:ListItem>
                                                        </asp:RadioButtonList>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"></label>
                                                        <small>
                                                            <asp:DropDownList ID="CbCustomer" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" OnInit="CbCustomer_Init">
                                                            </asp:DropDownList>
                                                        </small>
                                                        <asp:SqlDataSource ID="srcCustomersList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [Customers] WHERE ([TenantId] = @TenantId) ORDER BY Name">
                                                            <SelectParameters>
                                                                <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Destino</label>
                                                        <asp:SqlDataSource ID="srcOrigins" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [OpportunityOrigins] WHERE ([TenantId] = @TenantId)">
                                                            <SelectParameters>
                                                                <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                        <small>
                                                            <asp:DropDownList ID="CbOrigins" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" DataSourceID="srcOrigins" DataTextField="Description" DataValueField="Id" SelectedValue='<%# Bind("OriginId") %>'>
                                                            </asp:DropDownList>
                                                        </small>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Valor Total</label>
                                                        <%-- <input type="number" class="form-control" id="TxtInsertTotalValue" min="0" runat="server">--%>
                                                        <asp:TextBox ID="TxtTotalValue" CssClass="form-control input-sm smallBox" runat="server" Text='<%# Bind("TotalValue") %>' TextMode="Number" min="0" step="any"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Data Limite</label>
                                                        <%-- <input type="number" class="form-control" min="0" id="TxtInsertEndDate" runat="server">--%>
                                                        <asp:TextBox ID="TxtEndDate" CssClass="form-control input-sm txtMediumBox" runat="server" Text='<%# Bind("EndDate") %>' min="0" TextMode="Date"></asp:TextBox>

                                                    </div>
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <asp:LinkButton ID="BtnInsert" CssClass="btn btn-primary btn-xs" runat="server" Text="Guardar" CommandName="Insert" />
                                                            <asp:LinkButton ID="BtnInsCancel" CssClass="btn btn-default btn-xs" runat="server" Text="Limpar" CommandName="Cancel" />
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </small>
                                    </InsertItemTemplate>
                                    <ItemTemplate>

                                        <form class="form-horizontal" role="form">

                                            <small>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Id</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("Id")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Codigo</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("InternalId") %>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Data de Publicação</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtOppPublicDate")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Descrição</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtOppName")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Tipo de Procedimento</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtOppTypeName")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Cliente</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtCustomerName")%>
                                                                <%# Eval("TxtGroupName")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Destino</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtOppOriginName")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Valor Total</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("TotalValue")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Data Limite</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("EndDate")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-4 control-label">
                                                            Data Sistema</label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static">
                                                                <%# Eval("TxtCreateDate")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="text-left">
                                                        <asp:LinkButton ID="BtnEdit" CssClass="btn btn-primary btn-xs" runat="server" Text="Editar" CommandName="Edit" />
                                                    </div>
                                                </div>
                                            </small>
                                        </form>

                                    </ItemTemplate>
                                </asp:FormView>
                            </div>
                            <div class="row">
                                <asp:SqlDataSource ID="srcOpportunityDetails" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" DeleteCommand="DELETE FROM [Opportunities] WHERE [Id] = @Id" InsertCommand="INSERT INTO [Opportunities] ([TenantId], [InternalId], [CustomerId],[CustomerGroupId], [OportunityTypeId], [OriginId], [Name],[PublicDate], [EndDate], [TotalValue], [CreateDate], [CreateUserId], [ModifiedDate], [ModifiedUserId]) VALUES (@TenantId, @InternalId, @CustomerId,@CustomerGroupId,@OportunityTypeId, @OriginId, @TxtOppName,@PublicDate,@EndDate, @TotalValue, GETDATE(), @CreateUserId, GETDATE(), @ModifiedUserId)" SelectCommand="SELECT * FROM [VwOpportunities] WHERE (([TenantId] = @TenantId) AND ([Id] = @Id))" UpdateCommand="UPDATE [Opportunities] SET [InternalId] = @InternalId, [OportunityTypeId] = @OportunityTypeId, [OriginId] = @OriginId,[PublicDate]=@PublicDate,[EndDate]=@EndDate, [Name] = @TxtOppName, [TotalValue] = @TotalValue,ModifiedDate=GETDATE(), ModifiedUserId=@UserId WHERE [Id] = @Id AND TenantId=@TenantId">
                                    <DeleteParameters>
                                        <asp:Parameter Name="Id" Type="Int32" />
                                    </DeleteParameters>
                                    <InsertParameters>
                                        <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                                        <asp:Parameter Name="InternalId" Type="String" />
                                        <asp:Parameter Name="CustomerId" Type="Int32" />
                                        <asp:Parameter Name="CustomerGroupId" />
                                        <asp:Parameter Name="OportunityTypeId" Type="Int32" />
                                        <asp:Parameter Name="OriginId" Type="Int32" />
                                        <asp:Parameter Name="TotalValue" Type="Decimal" />
                                        <asp:Parameter Name="TxtOppName" />
                                        <asp:Parameter Name="PublicDate" />
                                        <asp:Parameter Name="EndDate" />
                                        <asp:SessionParameter Name="ModifiedUserId" SessionField="UserId" />
                                        <asp:SessionParameter Name="CreateUserId" SessionField="UserId" />
                                    </InsertParameters>
                                    <SelectParameters>
                                        <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                                        <asp:QueryStringParameter Name="Id" QueryStringField="OI" Type="Int32" />
                                    </SelectParameters>
                                    <UpdateParameters>
                                        <asp:Parameter Name="InternalId" Type="String" />
                                        <asp:Parameter Name="OportunityTypeId" Type="Int32" />
                                        <asp:Parameter Name="OriginId" Type="Int32" />
                                        <asp:Parameter Name="TotalValue" Type="Decimal" />
                                        <asp:Parameter Name="Id" Type="Int32" />
                                        <asp:SessionParameter Name="TenantId" SessionField="TenantId" />
                                        <asp:Parameter Name="PublicDate" />
                                        <asp:Parameter Name="EndDate" />
                                        <asp:SessionParameter Name="UserId" SessionField="UserId" />
                                        <asp:Parameter Name="TxtOppName" />
                                    </UpdateParameters>
                                </asp:SqlDataSource>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <div class="row" id="OppProductsPanel" runat="server">

                    <div class="col-md-12">
                        <h3 class="page-header">Produtos</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-md-12">

                        <div id="AlertDivProd" runat="server" class="alert alert-warning">
                            <div class="nav nav-pills nav-stacked col-md-12">
                                <div id="AlertLabelProd" class="col-md-12">
                                    <asp:Label ID="LbProdAlert" runat="server" Text="Label"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                            </div>
                            <div class="panel-body" style="min-height: 400px;">
                                <small>
                                    <telerik:RadGrid ID="GdOppProducts" runat="server" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AutoGenerateColumns="False" BorderStyle="None" CellSpacing="-1" CssClass="table table-striped " Style="outline: none;" DataSourceID="srcOppProducts" EnableEmbeddedBaseStylesheet="False" EnableEmbeddedSkins="False" EnableTheming="True" Width="100%">
                                        <ClientSettings>
                                            <Scrolling AllowScroll="True" ScrollHeight="" />
                                            <Resizing AllowResizeToFit="False" />
                                        </ClientSettings>
                                        <ExportSettings>
                                            <Pdf PageWidth="">
                                            </Pdf>
                                        </ExportSettings>
                                        <MasterTableView DataKeyNames="Id" DataSourceID="srcOppProducts" EditMode="InPlace" CommandItemDisplay="Top">
                                            <CommandItemSettings ShowAddNewRecordButton="False" ShowRefreshButton="False" />
                                            <RowIndicatorColumn Visible="False">
                                            </RowIndicatorColumn>
                                            <ExpandCollapseColumn Created="True">
                                            </ExpandCollapseColumn>
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter Id column" HeaderText="Id" ReadOnly="True" SortExpression="Id" UniqueName="Id" Visible="False">
                                                    <ColumnValidationSettings>
                                                        <ModelErrorMessage Text="" />
                                                    </ColumnValidationSettings>
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="OpportunityId" DataType="System.Int32" FilterControlAltText="Filter OpportunityId column" HeaderText="OpportunityId" SortExpression="OpportunityId" UniqueName="OpportunityId" Visible="False" ReadOnly="True">
                                                    <ColumnValidationSettings>
                                                        <ModelErrorMessage Text="" />
                                                    </ColumnValidationSettings>
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="TenantId" DataType="System.Int32" FilterControlAltText="Filter TenantId column" HeaderText="TenantId" SortExpression="TenantId" UniqueName="TenantId" Visible="False" ReadOnly="True">
                                                    <ColumnValidationSettings>
                                                        <ModelErrorMessage Text="" />
                                                    </ColumnValidationSettings>
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn DataField="Position" DataType="System.Int32" FilterControlAltText="Filter Position column" HeaderText="Pos. Item" SortExpression="Position" UniqueName="Position">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="PositionTextBox" runat="server" CssClass="form-control input-sm gridxsbox" MaxLength="4" Text='<%# Bind("Position") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="PositionLabel" runat="server" Text='<%# Eval("Position") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn DataField="ExternalCode" DataType="System.Int32" FilterControlAltText="Filter ExternalCode column" HeaderText="Ref. Cliente" SortExpression="ExternalCode" UniqueName="ExternalCode">
                                                    <InsertItemTemplate>
                                                        <asp:TextBox ID="ExternalCode" runat="server" CssClass="form-control input-sm smallBox" Text='<%# Bind("ExternalCode")%>'></asp:TextBox>
                                                    </InsertItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("ExternalCode") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="SAP" UniqueName="SAPSearch">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("ERPCode") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="input-group custom-search-form">
                                                            <asp:TextBox ID="TxtSAPSearch" runat="server" CssClass="form-control input-sm smallBox" Text='<%# Bind("AIMCode") %>'></asp:TextBox>
                                                            <asp:LinkButton ID="BtnSAPSearch" runat="server" CommandName="ProductSearch" CssClass="btn btn-default btn-sm"><i class="fa fa-search"></i></asp:LinkButton>
                                                        </div>
                                                    </InsertItemTemplate>
                                                    <ItemStyle Wrap="False" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn FilterControlAltText="Filter Description column" HeaderText="AIM" UniqueName="AIMCode" DataField="AIMCode" SortExpression="AIMCode">
                                                    <InsertItemTemplate>
                                                        <div class="input-group">
                                                            <div class="input-group custom-search-form">
                                                                <asp:TextBox ID="TxtAIMSearch" runat="server" CssClass="form-control input-sm smallBox" Text='<%# Bind("AIMCode") %>'></asp:TextBox>
                                                                <asp:LinkButton ID="BtnAIMSearch" runat="server" CommandName="ProductSearch" CssClass="btn btn-default btn-sm"><i class="fa fa-search"></i></asp:LinkButton>
                                                            </div>
                                                    </InsertItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="LbAIMCode" runat="server" Text='<%# Eval("AIMCode") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Wrap="False" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn DataField="Description" FilterControlAltText="Filter Description column" HeaderText="Descrição" SortExpression="Description" UniqueName="Description">
                                                    <InsertItemTemplate>
                                                        <asp:DropDownList ID="CbProducts" runat="server" CssClass="btn btn-default dropdown-toggle txtBigBox" Visible="False">
                                                        </asp:DropDownList>
                                                        <div class="input-group custom-search-form">
                                                            <asp:TextBox ID="TxtDescription" runat="server" CssClass="form-control input-sm txtBigBox" MaxLength="200" Text='<%# Bind("TxtProductName") %>'></asp:TextBox>
                                                            <asp:LinkButton ID="BtnNameSearch" runat="server" CommandName="ProductSearch" CssClass="btn btn-default btn-sm"><i class="fa fa-search"></i></asp:LinkButton>
                                                        </div>
                                                    </InsertItemTemplate>
                                                    <ItemTemplate>
                                                        <strong>
                                                            <asp:Label ID="LbProductName" runat="server" Text='<%# Eval("TxtProductName") %>'></asp:Label>
                                                        </strong>
                                                    </ItemTemplate>
                                                    <ItemStyle Wrap="False" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn DataField="UnitType" FilterControlAltText="Filter UnitType column" HeaderText="Apresentação" SortExpression="UnitType" UniqueName="UnitType" DataType="System.Int32" ReadOnly="True" Visible="False">

                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="CbProductTypes" runat="server" CssClass="btn btn-default dropdown-toggle smallBox" DataSourceID="srcProductTypes" DataTextField="SmallDescription" DataValueField="Id">
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="LbProdType" runat="server" Text='<%# Eval("TxtProdTypeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn FilterControlAltText="Filter Quantity column" HeaderText="Quantidade" UniqueName="Quantity" DataField="Quantity" DataType="System.Int32" SortExpression="Quantity">
                                                    <EditItemTemplate>
                                                        </td>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <asp:TextBox ID="QuantityTextBox" runat="server" CssClass="form-control input-sm gridsmallbox" MaxLength="7" min="0" Text='<%# Bind("Quantity") %>' TextMode="Number"></asp:TextBox>
                                                    </InsertItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="QuantityLabel" runat="server" Text='<%# Eval("Quantity") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>

                                                <telerik:GridTemplateColumn FilterControlAltText="Filter ButtonsCollum column" HeaderText="#" UniqueName="ButtonsCollum">
                                                    <EditItemTemplate>
                                                        <asp:Button ID="Button2" runat="server" CommandName="Update" CssClass="btn btn-success btn-xs" Text="Gravar" />
                                                        <asp:Button ID="Button3" runat="server" CommandName="Cancel" CssClass="btn btn-link btn-xs" Text="Cancelar" />
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-link btn-xs" runat="server" CommandName="PerformInsert"><span class="glyphicon glyphicon-plus"></span></asp:LinkButton>
                                                        <%--<asp:Button ID="Button2" runat="server" CommandName="PerformInsert" CssClass="btn btn-success btn-xs" Text="Adicionar" />--%>

                                                        <asp:Button ID="BtnClean" runat="server" CommandName="Cancel" CssClass="btn btn-link btn-xs" Text="Limpar" />
                                                        <asp:LinkButton ID="LinkButton3" CssClass="btn btn-link btn-xs" runat="server" OnClick="LinkButton3_Click"><span class="glyphicon glyphicon-remove"></span></asp:LinkButton>
                                                    </InsertItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-link btn-xs" CommandName="Delete"><span class="glyphicon glyphicon-remove"></span></asp:LinkButton>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                            <EditFormSettings>
                                                <EditColumn CancelImageUrl="Cancel.gif" InsertImageUrl="Update.gif" UpdateImageUrl="Update.gif">
                                                </EditColumn>
                                            </EditFormSettings>
                                            <CommandItemTemplate>
                                                <p class="pull-left">
                                                    <asp:Button ID="BtnProdAdd" runat="server" CssClass="btn btn-primary btn-xs" Text="Novo Produto" OnClick="BtnProdAdd_Click" />
                                                </p>
                                            </CommandItemTemplate>
                                        </MasterTableView>
                                        <FilterMenu EnableEmbeddedBaseStylesheet="False" EnableEmbeddedScripts="False" EnableEmbeddedSkins="False">
                                        </FilterMenu>
                                        <HeaderContextMenu EnableEmbeddedBaseStylesheet="False" EnableEmbeddedScripts="False" EnableEmbeddedSkins="False">
                                        </HeaderContextMenu>
                                    </telerik:RadGrid>

                                    <asp:SqlDataSource ID="srcOppProducts" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [VwOpportunityDetails] WHERE ([TenantId] = @TenantId) AND OpportunityId=@OpportunityId
ORDER BY Position" UpdateCommand="UPDATE [OpportunityDetails] SET [OpportunityId] = @OpportunityId, [TenantId] = @TenantId, [Position] = @Position, [ExternalCode] = @ExternalCode, [ProductId] = @ProductId, [Quantity] = @Quantity, [UnitType] = @UnitType, [UnitValue] = @UnitValue, [CreateDate] = @CreateDate, [CreateUserId] = @CreateUserId, [ModifiedDate] = @ModifiedDate, [ModifiedUserId] = @ModifiedUserId WHERE [Id] = @Id">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                            <asp:QueryStringParameter Name="OpportunityId" QueryStringField="OI" />
                                        </SelectParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="OpportunityId" Type="Int32" />
                                            <asp:Parameter Name="TenantId" Type="Int32" />
                                            <asp:Parameter Name="Position" Type="Int32" />
                                            <asp:Parameter Name="ExternalCode" Type="Int32" />
                                            <asp:Parameter Name="ProductId" Type="Int32" />
                                            <asp:Parameter Name="Quantity" Type="Int32" />
                                            <asp:Parameter Name="UnitType" Type="Int32" />
                                            <asp:Parameter Name="UnitValue" Type="Decimal" />
                                            <asp:Parameter Name="CreateDate" Type="DateTime" />
                                            <asp:Parameter Name="CreateUserId" Type="Int32" />
                                            <asp:Parameter Name="ModifiedDate" Type="DateTime" />
                                            <asp:Parameter Name="ModifiedUserId" Type="Int32" />
                                            <asp:Parameter Name="Id" Type="Int32" />
                                        </UpdateParameters>
                                    </asp:SqlDataSource>
                                    <asp:SqlDataSource ID="srcProductTypes" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [ProductTypes] WHERE ([TenantId] = @TenantId)">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                            </div>
                            </small>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>
