﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ChildMaster.master" CodeBehind="OpportunitiesAdmin.aspx.vb" Inherits="pmp_V3.WebForm7" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header">Oportunidades</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Origens
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <telerik:RadGrid ID="GdEntitiesList" CssClass="table table-striped table-bordered table-hover" runat="server" AutoGenerateColumns="False" DataSourceID="srcOriginsList" EnableEmbeddedSkins="False" Width="100%" EnableAjaxSkinRendering="False" EnableEmbeddedBaseStylesheet="False" EnableEmbeddedScripts="False" EnableTheming="True">
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="srcOriginsList">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter Id column" HeaderText="Id" ReadOnly="True" SortExpression="Id" UniqueName="Id" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TenantId" DataType="System.Int32" FilterControlAltText="Filter TenantId column" HeaderText="TenantId" SortExpression="TenantId" UniqueName="TenantId" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Description" FilterControlAltText="Filter Description column" HeaderText="Description" SortExpression="Description" UniqueName="Description">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                </Columns>

                                <EditFormSettings>
                                    <EditColumn InsertImageUrl="Update.gif" UpdateImageUrl="Update.gif" CancelImageUrl="Cancel.gif"></EditColumn>
                                </EditFormSettings>
                            </MasterTableView>

                            <FilterMenu EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" EnableEmbeddedBaseStylesheet="False"></FilterMenu>

                            <HeaderContextMenu EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" EnableEmbeddedBaseStylesheet="False"></HeaderContextMenu>
                        </telerik:RadGrid>

                        <asp:SqlDataSource ID="srcOriginsList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [OpportunityOrigins] WHERE ([TenantId] = @TenantId)">
                            <SelectParameters>
                                <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de Tipos
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <telerik:RadGrid ID="GdOppTypesList" CssClass="table table-striped table-bordered table-hover" runat="server" AutoGenerateColumns="False" DataSourceID="srcOpportunitiesTypesList" EnableEmbeddedSkins="False" Width="100%" EnableAjaxSkinRendering="False" EnableEmbeddedBaseStylesheet="False" EnableEmbeddedScripts="False" EnableTheming="True">
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="srcOpportunitiesTypesList">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter Id column" HeaderText="Id" ReadOnly="True" SortExpression="Id" UniqueName="Id" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TenantId" FilterControlAltText="Filter TenantId column" HeaderText="TenantId" SortExpression="TenantId" UniqueName="TenantId" DataType="System.Int32" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Name" FilterControlAltText="Filter Name column" HeaderText="Name" SortExpression="Name" UniqueName="Name">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                </Columns>

                                <EditFormSettings>
                                    <EditColumn InsertImageUrl="Update.gif" UpdateImageUrl="Update.gif" CancelImageUrl="Cancel.gif"></EditColumn>
                                </EditFormSettings>
                            </MasterTableView>

                            <FilterMenu EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" EnableEmbeddedBaseStylesheet="False"></FilterMenu>

                            <HeaderContextMenu EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" EnableEmbeddedBaseStylesheet="False"></HeaderContextMenu>
                        </telerik:RadGrid>

                        <asp:SqlDataSource ID="srcOpportunitiesTypesList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [OpportunityTypes] WHERE ([TenantId] = @TenantId)">
                            <SelectParameters>
                                <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
     
    </div>

</asp:Content>
