﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Dashboard.aspx.vb" Inherits="pmp_V3.Dashboard" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<%--    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Métricas</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>--%>
    <div class="MasterHeader">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Filtros</div>
                <div class="panel-body">
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlProducts" runat="server" AutoPostBack="True" DataSourceID="srcProducts" DataTextField="ProductName" DataValueField="ProductId" class="btn btn-default dropdown-toggle" Width="100%">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlCustomers" runat="server" AutoPostBack="True" DataSourceID="srcCustomers" DataTextField="CustomerName" DataValueField="CustomerId" class="btn btn-default dropdown-toggle" Width="100%">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlDates" runat="server" AutoPostBack="True" DataSourceID="srcDates" DataTextField="StatusDateDesc" DataValueField="StatusDate" class="btn btn-default dropdown-toggle" Width="100%">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlOpportunities" runat="server" AutoPostBack="True" DataSourceID="srcOpportunities" DataTextField="Name" DataValueField="Id" class="btn btn-default dropdown-toggle" Width="100%">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" DataSourceID="srcStatus" DataTextField="Description" DataValueField="Id" class="btn btn-default dropdown-toggle" Width="100%">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Indicadores</div>
                <div class="panel-body">
                    <div class="col-md-4">
                        <h4 class="text-center">TOP Customers</h4>
                        <asp:Chart ID="Chart1" runat="server" DataSourceID="srcGraphTopCustomers">
                            <Series>
                                <asp:Series ChartType="Pie" Name="Series1" XValueMember="CustomerName" YValueMembers="total_line">
                                </asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1">
                                </asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                    <div class="col-md-4">
                        <h4 class="text-center">TOP Customers</h4>
                        <asp:GridView ID="gvTopCustomers" runat="server" AutoGenerateColumns="False" DataSourceID="srcGraphTopCustomers" CssClass="table-bordered table-condensed" Width="300px" ShowHeader="False">
                            <Columns>
                                <asp:BoundField DataField="CustomerName" HeaderText="CustomerName" SortExpression="CustomerName" />
                                <asp:BoundField DataField="total_line" HeaderText="total_line" SortExpression="total_line" Visible="False">
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="col-md-4">
                        <h4 class="text-center">TOP Products</h4>
                        <asp:GridView ID="gvTopProducts" runat="server" AutoGenerateColumns="False" DataSourceID="srcGraphTopProducts" CssClass="table-bordered table-condensed" Width="300px" ShowHeader="False">
                            <Columns>
                                <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
                                <asp:BoundField DataField="total_line" HeaderText="total_line" SortExpression="total_line" Visible="False">
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
        <asp:SqlDataSource ID="srcOpportunities" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT 0 as Id, '(Oportunidades)' as Name
UNION 
SELECT Id, Name + ' | '+rtrim(InternalId) AS OppDescription  from Opportunities
WHERE TenantId=@TenantId
ORDER BY Name">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="srcStatus" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT 9 as Id, '(Estado)' as Description UNION SELECT Id, Description from ProposalDetailStatus

  ORDER BY Description "></asp:SqlDataSource>
        <asp:SqlDataSource ID="srcDates" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT '(Periodo)' StatusDateDesc, '9' as StatusDate UNION SELECT DISTINCT convert(nvarchar(7),StatusDate,111) as StatusDateDesc, convert(nvarchar(7),StatusDate,111) as StatusDate FROM ProposalDetails 
  where StatusDate is not null AND TenantId=@TenantId
  order by StatusDate DESC">
            <SelectParameters>
                <asp:SessionParameter Name="TenantId" SessionField="TenantId" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="srcProducts" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT 0 AS ProductId, '(Produtos)' AS ProductName UNION SELECT DISTINCT [ProductId], [ProductName] FROM [VwProposalProducts] WHERE TenantId=@TenantId ORDER BY [ProductName]">
            <SelectParameters>
                <asp:SessionParameter Name="TenantId" SessionField="TenantId" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="srcCustomers" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT 0 AS CustomerId, '(Cliente)' AS CustomerName UNION  SELECT DISTINCT [CustomerId], [CustomerName] FROM [VwProposalProducts] WHERE TenantId=@TenantId ORDER BY [CustomerName]">
            <SelectParameters>
                <asp:SessionParameter Name="TenantId" SessionField="TenantId" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="srcGraphTopProducts" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT TOP 5 ProductId, ProductName, sum(quantity*price) as total_line FROM VwProposalProducts 
where (CustomerId=@CustomerId OR @CustomerId=0) AND TenantId=@TenantId
GROUP BY ProductId, ProductName
ORDER BY total_line DESC">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlCustomers" Name="CustomerId" PropertyName="SelectedValue" />
                <asp:QueryStringParameter Name="TenantId" QueryStringField="TenantId" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="srcGraphTopCustomers" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT CustomerId, CustomerName, sum(quantity*price) as total_line FROM VwProposalProducts 
where (ProductId=@ProductId OR @ProductId=0) AND TenantId=@TenantId
group by CustomerId, CustomerName
order by total_line desc">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlProducts" Name="ProductId" PropertyName="SelectedValue" />
                <asp:SessionParameter Name="TenantId" SessionField="TenantId" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="gvMaster" runat="server" CssClass="table " AutoGenerateColumns="False" DataKeyNames="Id,ProposalId" DataSourceID="srcData" Width="100%" GridLines="None">
                <Columns>
                    <asp:BoundField DataField="ProposalId" HeaderText="ProposalId" />
                    <asp:BoundField DataField="ProductId" HeaderText="ProductId" SortExpression="ProductId" />
                    <asp:BoundField DataField="TxtStatusDate" HeaderText="Data">
                        <ItemStyle Width="100px" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductName" HeaderText="Produto" SortExpression="ProductName">
                        <ItemStyle Width="300px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Price" HeaderText="Preço" SortExpression="Price" DataFormatString="{0:c}">
                        <ItemStyle HorizontalAlign="Right" Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Quantity" HeaderText="Unidades" SortExpression="Quantity">
                        <ItemStyle HorizontalAlign="Center" Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CustomerName" HeaderText="Cliente" SortExpression="CustomerName"></asp:BoundField>
                    <asp:BoundField DataField="StatusDesc" HeaderText="Estado" SortExpression="StatusDesc">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="OpportunityDesc" HeaderText="Oportunidade" SortExpression="OpportunityDesc" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:GridView ID="gvDetail" runat="server" AutoGenerateColumns="False" DataSourceID="srcProductDetails" CssClass="table" GridLines="None">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" Visible="False" />
                                    <asp:BoundField DataField="ProposalId" HeaderText="ProposalId" SortExpression="ProposalId" Visible="False" />
                                    <asp:BoundField DataField="ProductId" HeaderText="ProductId" SortExpression="ProductId" Visible="False" />
                                    <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" Visible="False" />
                                    <asp:BoundField DataField="Quantity" HeaderText="Qtd" SortExpression="Quantity" />
                                    <asp:BoundField DataField="Price" HeaderText="Preço" SortExpression="Price" DataFormatString="{0:c}" />
                                    <asp:BoundField DataField="StatusId" HeaderText="StatusId" SortExpression="StatusId" Visible="False" />
                                    <asp:BoundField DataField="TxtStatusDate" HeaderText="Data" SortExpression="StatusDate" Visible="True" />
                                    <asp:BoundField DataField="StatusDesc" HeaderText="StatusDesc" SortExpression="StatusDesc" Visible="False" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="srcProductDetails" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [VwProposalProducts] WHERE ( ([StatusId] = @StatusId) AND ([ProposalId] = @ProposalId) AND ([ProductId] = @ProductId) )">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="0" Name="StatusId" Type="Int32" />
                                    <asp:Parameter Name="ProposalId" Type="Int32" />
                                    <asp:Parameter Name="ProductId" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="srcData" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [VwProposalProducts] 
WHERE (ProductId = @ProductId OR @ProductId=0) AND (CONVERT(nvarchar(7),StatusDate,111) = @Date or @Date='9')
AND (CustomerId = @CustomerId OR @CustomerId = 0) AND (OpportunityId = @OpportunityId OR @OpportunityId = 0)  
AND (StatusId = @StatusId OR @StatusId=9)
AND TenantId=@TenantId AND StatusId IN (1,2) AND CustomerId is not null 
ORDER BY ProductId, StatusDesc">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlProducts" Name="ProductId" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="ddlDates" Name="Date" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="ddlCustomers" Name="CustomerId" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="ddlOpportunities" Name="OpportunityId" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="ddlStatus" Name="StatusId" PropertyName="SelectedValue" />
                    <asp:SessionParameter Name="TenantId" SessionField="TenantId" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </div>


</asp:Content>
