﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ChildMaster.master" CodeBehind="CustomerList.aspx.vb" Inherits="pmp_V3.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ul class="nav" id="side-menu">
        <li class="sidebar-search">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            <!-- /input-group -->
        </li>
        <li role="presentation" class="divider"></li>
        <li><a href="#">Novo</a></li>
        <li><a href="#">...</a></li>
        <li><a href="#">...</a></li>
        <li><a href="#">...</a></li>
    </ul>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Clientes</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="table-responsive">

                        <small>
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table" DataKeyNames="Id" DataSourceID="srcCustomerList" GridLines="None" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                                <asp:BoundField DataField="ERPCode" HeaderText="ERP" SortExpression="ERPCode" />
                                <asp:BoundField DataField="Name" HeaderText="Nome" SortExpression="Name" />
                                <asp:BoundField DataField="Address" HeaderText="Morada" SortExpression="Address" />
                                <asp:BoundField DataField="PostalCode" HeaderText="Cod. Postal" SortExpression="PostalCode" >
                                <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="City" HeaderText="Localidade" SortExpression="City" />
                                <asp:BoundField DataField="Country" HeaderText="Pais" SortExpression="Country" />
                            </Columns>
                        </asp:GridView>
                        </small>

                        <telerik:RadGrid ID="GdCustomers" CssClass="table table-striped table-bordered table-hover" Visible="false" runat="server" AutoGenerateColumns="False" DataSourceID="srcCustomerList" EnableEmbeddedSkins="False" Width="100%" EnableAjaxSkinRendering="False" EnableEmbeddedBaseStylesheet="False" EnableEmbeddedScripts="False" EnableTheming="True">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" ScrollHeight="" />
                            </ClientSettings>
                            <MasterTableView DataKeyNames="Id" DataSourceID="srcCustomerList">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter Id column" HeaderText="Id" ReadOnly="True" SortExpression="Id" UniqueName="Id" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TenantId" DataType="System.Int32" FilterControlAltText="Filter TenantId column" HeaderText="TenantId" SortExpression="TenantId" UniqueName="TenantId" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ERPCode" DataType="System.Int32" FilterControlAltText="Filter ERPCode column" HeaderText="Código" SortExpression="ERPCode" UniqueName="ERPCode">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Name" FilterControlAltText="Filter Name column" HeaderText="Nome" SortExpression="Name" UniqueName="Name">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Address" FilterControlAltText="Filter Address column" HeaderText="Morada" SortExpression="Address" UniqueName="Address">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Address2" FilterControlAltText="Filter Address2 column" HeaderText="Address2" SortExpression="Address2" UniqueName="Address2" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="City" FilterControlAltText="Filter City column" HeaderText="Cidade" SortExpression="City" UniqueName="City">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="City2" FilterControlAltText="Filter City2 column" HeaderText="City2" SortExpression="City2" UniqueName="City2" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PostalCode" FilterControlAltText="Filter PostalCode column" HeaderText="Código Postal" SortExpression="PostalCode" UniqueName="PostalCode">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Country" FilterControlAltText="Filter Country column" HeaderText="País" SortExpression="Country" UniqueName="Country">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn CancelImageUrl="Cancel.gif" InsertImageUrl="Update.gif" UpdateImageUrl="Update.gif">
                                    </EditColumn>
                                </EditFormSettings>
                            </MasterTableView>

                            <FilterMenu EnableEmbeddedSkins="False"></FilterMenu>

                            <HeaderContextMenu EnableEmbeddedSkins="False"></HeaderContextMenu>
                        </telerik:RadGrid>


                        <asp:SqlDataSource ID="srcCustomerList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [Customers] WHERE ([TenantId] = @TenantId)">
                            <SelectParameters>
                                <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>


                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
