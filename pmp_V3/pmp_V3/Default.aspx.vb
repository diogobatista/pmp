﻿Public Class _Default
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not IsPostBack Then

            'startup load

            lblContracts.Text = Getdata.retrieve_sql_field_value("SELECT COUNT(Id) as TOTAL FROM Proposals WHERE StatusId=9 AND TenantId=" & Getdata.TenantId & "", "TOTAL")
            lblOpportunities.Text = Getdata.retrieve_sql_field_value("SELECT COUNT(Id) as TOTAL FROM Opportunities WHERE IsActive=1 AND IsCanceled=0 AND EndDate>getdate() AND TenantId=" & Getdata.TenantId & "", "TOTAL")
            lblProposals.Text = Getdata.retrieve_sql_field_value("SELECT COUNT(Id) as TOTAL FROM Proposals WHERE StatusId IN (1,2,3,5) AND TenantId=" & Getdata.TenantId & "", "TOTAL")

            lblCustomers.Text = Getdata.retrieve_sql_field_value("SELECT COUNT(Id) AS TOTAL FROM Customers WHERE TenantId=" & Getdata.TenantId & "", "TOTAL")
            lblProducts.Text = Getdata.retrieve_sql_field_value("SELECT COUNT(Id) AS TOTAL FROM Products WHERE TenantId=" & Getdata.TenantId & "", "TOTAL")
            lblTasks.Text = Getdata.retrieve_sql_field_value("SELECT COUNT(Id) AS TOTAL FROM ProposalTasks WHERE TenantId=" & Getdata.TenantId & "", "TOTAL")

        End If


    End Sub
End Class