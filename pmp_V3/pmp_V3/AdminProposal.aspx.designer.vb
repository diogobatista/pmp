﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class WebForm6

    '''<summary>
    '''GdPropStatusList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GdPropStatusList As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''srcProposalStatusList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcProposalStatusList As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''GdPropTypesList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GdPropTypesList As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''srcProposalTypesList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcProposalTypesList As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''GdPropTaskList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GdPropTaskList As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''srcProposalTaskList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcProposalTaskList As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''GdPropTaskStatusList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GdPropTaskStatusList As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''srcPropTaskStatusList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcPropTaskStatusList As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''GdMotiveList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GdMotiveList As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''srcMotiveList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents srcMotiveList As Global.System.Web.UI.WebControls.SqlDataSource
End Class
