﻿Imports Telerik.Web.UI
Imports CrystalDecisions.CrystalReports.Engine

Public Class ProposalDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim strQuerystring As String = Request.QueryString("PI")
            Dim strFormProposal As FormView = Me.FrmProposal

            If strQuerystring = 0 Then

                strFormProposal.DefaultMode = FormViewMode.Insert

                Me.DivTaskList.Visible = False
                Me.DivProductsList.Visible = False
                Me.PropHideCaret.Visible = False
                'Me.PropShowCaret.Visible = False

                'Me.OppShowPanel.Visible = False
                'Me.OppHidePanel.Visible = True

                Me.TitleNewRow.Visible = True
                Me.TitleRow.Visible = False

                Me.PanelReports.Visible = False ' Reports Panel 

                Me.HPdf.Visible = False
                Me.LiPdf.Visible = False



                ProposalStatusRules()

                ProposalTaskCounters()

            Else

                strFormProposal.DefaultMode = FormViewMode.ReadOnly

                ShowReportOnLoad()

                Me.PanelReports.Visible = False ' Reports Panel 

                Me.DivProdAlert.Visible = True
                Me.DivPropTask.Visible = True

                'Me.OpportunityPanel.Visible = False
                'Me.OppHidePanel.Visible = False

                Me.ProposalPanel.Visible = False
                Me.PropHideCaret.Visible = False

                Me.TitleNewRow.Visible = False
                Me.TitleRow.Visible = True

                ProposalStatusRules()

                ProposalTaskCounters()

                LabelsOnLoad()

            End If

            Me.AlertDiv.Visible = False
            Me.DivProdAlert.Visible = False
            Me.DivPropTask.Visible = False


        Else



        End If



    End Sub
#Region "REPORTS AND ALERTS"

    Protected Sub BtnPdf_Click(sender As Object, e As EventArgs) Handles BtnPdf.Click

        ExportPdf()

    End Sub

    Private Sub ShowReportOnLoad()

        Dim strProposalId As String = Request.QueryString("PI")


        Dim crystalReport As New ReportDocument()

        crystalReport.Load(Server.MapPath("CrystalReport1.rpt"))
        crystalReport.SetDatabaseLogon("sa", "Password2013", "zzt2013.no-ip.biz", "pmp")
        CrystalReportViewer1.ReportSource = crystalReport
        CrystalReportViewer1.SelectionFormula = "{VwProposals.TenantId} = " & Getdata.TenantId & " and {VwProposals.Id} = " & strProposalId & " and {VwProposalDetails.TxtPropQuantity} > 0"



    End Sub

    Private Sub ExportPdf()

        Dim strProposalId As String = Request.QueryString("PI")


        Dim crystalReport As New ReportDocument()

        crystalReport.Load(Server.MapPath("CrystalReport1.rpt"))
        crystalReport.SetDatabaseLogon("sa", "Password2013", "zzt2013.no-ip.biz", "pmp")
        CrystalReportViewer1.ReportSource = crystalReport
        CrystalReportViewer1.SelectionFormula = "{VwProposals.TenantId} = " & Getdata.TenantId & " and {VwProposals.Id} = " & strProposalId & " and {VwProposalDetails.TxtPropQuantity} > 0"

        crystalReport.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, True, "Proposta_" & Right("000000" & strProposalId, 6))


    End Sub


#End Region

#Region "ALERTS AND VALIDATORS"
    Private Sub ShowAlertPanelError(strType As String, strDivId As Object, LbErrorAlert As Label) ' Show Alert Panel Error

        strDivId.Visible = True
        'Me.AlertDatePicker.Visible = False

        LbConfirmLabel.Visible = False

        strDivId.Attributes("Class") = "alert alert-danger alert-dismissable"

        LbErrorAlert.Text = strType


    End Sub

    Private Sub ShowAlertPanelWarning(strMessage As String, strDescription As String, strDivId As Object,
                                      LbAlertMessage As Label, LbAlertDescription As Label, ChkConfirm As CheckBox,
                                      DpDate As TextBox, ShowDate As Boolean, ShowMotive As Boolean, strStatusId As String)  ' Show Alert Panel Valid

        strDivId.Visible = True

        strDivId.Attributes("Class") = "alert alert-warning alert-dismissable"



        If ShowDate = True Then

            DpDate.Visible = True


        Else

            DpDate.Visible = False


        End If

        If ShowMotive = True Then

            CbForsakeMotives.Visible = True

        Else

            CbForsakeMotives.Visible = False


        End If

        If strStatusId <> 0 Then

            BtnActionOk.Visible = True

            If strStatusId = 2 Then

                Me.CbUserAproval.Visible = True


            End If

        End If



        LbStatusId.Text = strStatusId
        ChkConfirm.Visible = True
        LbTaskDescription.Visible = True
        LbConfirmLabel.Visible = True

        LbAlertMessage.Text = strMessage
        LbAlertDescription.Text = strDescription





    End Sub
    Private Sub ShowAlertPanelValid(strType As String, strDivId As Object, LbValid As Label)  ' Show Alert Panel Valid

        strDivId.Visible = True

        strDivId.Attributes("Class") = "alert alert-success alert-dismissable"

        LbValid.Text = strType


    End Sub

    Private Sub HighlightFieldError(strField As Object, strbol As Boolean)


        If strbol = True Then

            strField.BorderColor = Drawing.Color.Green
            strField.BorderWidth = Unit.Pixel(1)

        Else

            strField.BorderColor = Drawing.Color.Red
            strField.BorderWidth = Unit.Pixel(2)

        End If


    End Sub
    '####################### Insert Validations
    '##### Proposals Task Validations 
    Private Function ValidateInsertTask(strGrid As GridEditableItem
                                        ) As Boolean

        ValidateInsertTask = True


        Dim strFieldName As TextBox

        Dim strlb As String = TryCast(strGrid("Item").FindControl("LbInsertMode"), Label).Text


        '###### Task Description 
        strFieldName = DirectCast(strGrid.FindControl("TxtInsertTaskDescription"), TextBox)
        If strlb = "" Then

            If Len(strFieldName.Text) = 0 Then

                ShowAlertPanelError("A Descrição da tarefa não foi preenchida por favor tente novamente.", DivPropTask, LbTaskMessage)
                HighlightFieldError(strFieldName, False)
                ValidateInsertTask = False
                Exit Function

            Else
                HighlightFieldError(strFieldName, True)

            End If
        End If

        '###### Task Start Date  
        strFieldName = DirectCast(strGrid.FindControl("TxtStartDateTextBox"), TextBox)
        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("A Data de Inicio da tarefa não foi preenchida por favor tente novamente.", DivPropTask, LbTaskMessage)
            HighlightFieldError(strFieldName, False)
            ValidateInsertTask = False
            Exit Function
        Else
            HighlightFieldError(strFieldName, True)


        End If

        '###### Task End Date  
        strFieldName = DirectCast(strGrid.FindControl("TxtEndDateTextBox"), TextBox)
        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("A Data de Fim da tarefa não foi preenchida por favor tente novamente.", DivPropTask, LbTaskMessage)
            HighlightFieldError(strFieldName, False)
            ValidateInsertTask = False
            Exit Function
        Else
            HighlightFieldError(strFieldName, True)


        End If


        '###### Task Start Date < Task End Date
        Dim strStart As Date = Date.Parse(DirectCast(strGrid.FindControl("TxtStartDateTextBox"), TextBox).Text)
        Dim strEnd As Date = Date.Parse(DirectCast(strGrid.FindControl("TxtEndDateTextBox"), TextBox).Text)


        If strEnd > strStart Then
            'HighlightFieldError(strFieldName, True)

        Else

            ShowAlertPanelError("A Data de Inicio é Superior à Data de Fim , por favor tente novamente.", DivPropTask, LbTaskMessage)
            DirectCast(strGrid.FindControl("TxtStartDateTextBox"), TextBox).Text = ""
            HighlightFieldError(DirectCast(strGrid.FindControl("TxtStartDateTextBox"), TextBox), False)
            ValidateInsertTask = False
            Exit Function



        End If
    End Function

    Private Function ValidateEditTask(strGrid As GridEditableItem
                                      ) As Boolean

        ValidateEditTask = True


        Dim strFieldName As TextBox

        '###### Task Description 
        strFieldName = DirectCast(strGrid.FindControl("TxtEditTaskDescription"), TextBox)
        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("A Descrição da tarefa não foi preenchida por favor tente novamente.", DivPropTask, LbTaskMessage)
            HighlightFieldError(strFieldName, False)
            ValidateEditTask = False
            Exit Function
        Else
            HighlightFieldError(strFieldName, True)


        End If

        '###### Task Start Date  
        strFieldName = DirectCast(strGrid.FindControl("TxtEditStartDate"), TextBox)
        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("A Data de Inicio da tarefa não foi preenchida por favor tente novamente.", DivPropTask, LbTaskMessage)
            HighlightFieldError(strFieldName, False)
            ValidateEditTask = False
            Exit Function
        Else
            HighlightFieldError(strFieldName, True)


        End If

        '###### Task End Date  
        strFieldName = DirectCast(strGrid.FindControl("TxtEditEndDate"), TextBox)
        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("A Data de Fim da tarefa não foi preenchida por favor tente novamente.", DivPropTask, LbTaskMessage)
            HighlightFieldError(strFieldName, False)
            ValidateEditTask = False
            Exit Function
        Else
            HighlightFieldError(strFieldName, True)


        End If


        '###### Task Start Date < Task End Date
        Dim strStart As Date = Date.Parse(DirectCast(strGrid.FindControl("TxtEditStartDate"), TextBox).Text)
        Dim strEnd As Date = Date.Parse(DirectCast(strGrid.FindControl("TxtEditEndDate"), TextBox).Text)


        If strEnd > strStart Then
            'HighlightFieldError(strFieldName, True)

        Else

            ShowAlertPanelError("A Data de Inicio é Superior à Data de Fim , por favor tente novamente.", DivPropTask, LbTaskMessage)
            DirectCast(strGrid.FindControl("TxtStartDateTextBox"), TextBox).Text = ""
            HighlightFieldError(DirectCast(strGrid.FindControl("TxtStartDateTextBox"), TextBox), False)
            ValidateEditTask = False
            Exit Function



        End If
    End Function

    Private Function ValidateProduct(strGrid As Object, strType As String) As Boolean


        ValidateProduct = True


        Dim strFieldName As TextBox

        '###### Product Qty 
        If strType = "Insert" Then
            strFieldName = DirectCast(strGrid.FindControl("TxtPropQuantity"), TextBox)
        End If
        If strType = "Edit" Then
            strFieldName = DirectCast(strGrid.FindControl("TxtEditPropQuantity"), TextBox)
        End If



        If Len(strFieldName.Text) = 0 And IsNumeric(strFieldName.Text) = False Then

            ShowAlertPanelError("A Quantidade não foi preenchida por favor tente novamente.", DivProdAlert, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            ValidateProduct = False
            Exit Function

        Else
            HighlightFieldError(strFieldName, True)


        End If


        '###### Product Free Qty 
        'strFieldName = DirectCast(strGrid.FindControl("TxtFreeQty"), TextBox)

        'If Len(strFieldName.Text) = 0 And IsNumeric(strFieldName.Text) = False Then

        '    ShowAlertPanelError("Campo Free Qty não foi preenchida por favor tente novamente.", DivProdAlert, LbProdAlert)
        '    HighlightFieldError(strFieldName, False)
        '    ValidateProduct = False
        '    Exit Function
        'Else
        '    HighlightFieldError(strFieldName, True)


        'End If

        '###### Product Price
        If strType = "Insert" Then
            strFieldName = DirectCast(strGrid.FindControl("TxtPropPrice"), TextBox)
        End If
        If strType = "Edit" Then
            strFieldName = DirectCast(strGrid.FindControl("TxtEditPropPrice"), TextBox)
        End If


        If Len(strFieldName.Text) = 0 And IsNumeric(strFieldName.Text) = False Then

            ShowAlertPanelError("Campo Preço não foi preenchida por favor tente novamente.", DivProdAlert, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            ValidateProduct = False
            Exit Function
        Else
            HighlightFieldError(strFieldName, True)

        End If

        '###### Product Package Qty 
        'strFieldName = DirectCast(strGrid.FindControl("TxtPackageQty"), TextBox)

        'If Len(strFieldName.Text) = 0 And IsNumeric(strFieldName.Text) = False Then

        '    ShowAlertPanelError("Campo Package Qty não foi preenchida por favor tente novamente.", DivProdAlert, LbProdAlert)
        '    HighlightFieldError(strFieldName, False)
        '    ValidateProduct = False
        '    Exit Function
        'Else
        '    HighlightFieldError(strFieldName, True)


        'End If

        '###### Product Discount Qty 
        'strFieldName = DirectCast(strGrid.FindControl("TxtDiscount"), TextBox)

        'If Len(strFieldName.Text) = 0 And IsNumeric(strFieldName.Text) = False Then

        '    ShowAlertPanelError("Campo Discount não foi preenchida por favor tente novamente.", DivProdAlert, LbProdAlert)
        '    HighlightFieldError(strFieldName, False)
        '    ValidateProduct = False
        '    Exit Function
        'Else
        '    HighlightFieldError(strFieldName, True)


        'End If

        '###### Product Discount Qty 
        'strFieldName = DirectCast(strGrid.FindControl("TxtDeliveryMonths"), TextBox)

        'If Len(strFieldName.Text) = 0 And IsNumeric(strFieldName.Text) = False Then

        '    ShowAlertPanelError("Campo Delivery Months não foi preenchida por favor tente novamente.", DivProdAlert, LbProdAlert)
        '    HighlightFieldError(strFieldName, False)
        '    ValidateProduct = False
        '    Exit Function
        'Else
        '    HighlightFieldError(strFieldName, True)


        'End If



        '###### Product Validity 
        If strType = "Insert" Then
            strFieldName = DirectCast(strGrid.FindControl("TxtPropValidity"), TextBox)
        End If
        If strType = "Edit" Then
            strFieldName = DirectCast(strGrid.FindControl("TxtEditPropValidity"), TextBox)
        End If


        If Len(strFieldName.Text) = 0 And IsNumeric(strFieldName.Text) = False Then

            ShowAlertPanelError("Campo Validade não foi preenchida por favor tente novamente.", DivProdAlert, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            ValidateProduct = False
            Exit Function
        Else
            HighlightFieldError(strFieldName, True)

        End If


    End Function

    Private Function ValidateRequest(strGrid As GridEditableItem) As Boolean


        ValidateRequest = True


        Dim strFieldName As TextBox

        '###### Product Qty 
        strFieldName = DirectCast(strGrid.FindControl("RequestMasterTextBox"), TextBox)

        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("Codigo master nao foi preenchido.", DivProdAlert, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            ValidateRequest = False
            Exit Function

        Else
            HighlightFieldError(strFieldName, True)


        End If

        strFieldName = DirectCast(strGrid.FindControl("DescriptionTextBox"), TextBox)

        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("Descrição nao foi preenchida.", DivProdAlert, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            ValidateRequest = False
            Exit Function

        Else
            HighlightFieldError(strFieldName, True)


        End If

        strFieldName = DirectCast(strGrid.FindControl("QuantityTextBox"), TextBox)

        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("Descrição nao foi preenchida.", DivProdAlert, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            ValidateRequest = False
            Exit Function

        Else
            HighlightFieldError(strFieldName, True)


        End If

    End Function
#End Region   '############# ALERTS AND HIGHLIGHT FIELDS 

#Region "REQUESTS AND LOAD RULES"


    Private Sub ProductsInfoTotals(strDataItem As GridDataItem, LbTotalAUE As Label, LbTotalSAP As Label, LbTotalSales As Label)

        Dim strProposalId As String = Request.QueryString("PI")

        Dim strProductId As String = DirectCast(strDataItem.FindControl("LbProductId"), Label).Text

        Dim SqlSumSelect As String = "Select sum(Quantity) as strQtd,Id as OrderRequestId from OrderRequests where ProposalId = " & strProposalId & " and TenantId= " & Getdata.TenantId & " and ProductId = " & strProductId & " Group By ProductId, Id"

        Dim sqlOrderRequest As DataTable = Getdata.retrive_tablelist(SqlSumSelect)

        Dim strOrderRequestId As String
        Dim strOrderRequestSum As Integer = 0
        Dim strInvoiceSum As Integer = 0
        Dim strInvoiceTotal As Decimal = 0

        For Each row In sqlOrderRequest.Rows

            strOrderRequestId = row("OrderRequestId").ToString

            strOrderRequestSum = strOrderRequestSum + row("strQtd").ToString



            SqlSumSelect = "Select sum(InvoiceQuantity) as InvoiceQuantity, sum(InvoiceTotal) as InvoiceTotal from Invoices where TenantId= " & Getdata.TenantId & " and OrderRequestId = " & strOrderRequestId & " and ProposalId = " & strProposalId

            Dim strInvoiceQtd As String = Getdata.retrieve_sql_field_value(SqlSumSelect, "InvoiceQuantity")
            Dim strInvoiceVal As String = Getdata.retrieve_sql_field_value(SqlSumSelect, "InvoiceTotal")

            If strInvoiceQtd = "" Then
                strInvoiceQtd = 0

            End If

            strInvoiceSum = strInvoiceSum + strInvoiceQtd

            If strInvoiceVal = "" Then
                strInvoiceVal = 0

            End If
            strInvoiceTotal = strInvoiceTotal + strInvoiceVal





        Next


        LbTotalAUE.Text = strOrderRequestSum
        LbTotalSAP.Text = strInvoiceSum
        LbTotalSales.Text = Format(strInvoiceTotal, "c")

        'LbTotalAUE.Text = sqlSum


        'SqlSumSelect = ""


        'LbTotalSAP.Text = sqlSum



    End Sub

    Private Sub LabelsOnLoad()

        Dim strPropId As String = Request.QueryString("PI")

        Dim SqlSelectProposal As String = "SELECT * FROM VwProposals WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strPropId

        Dim dt As DataTable = Getdata.retrive_tablelist(SqlSelectProposal)

        For Each row As DataRow In dt.Rows

            'Me.LbPropType.Text = row("OpportunityName").ToString
            Me.LbHeaderStatus.Text = row("TxtPropStatusName").ToString
            Me.LbProposalId.Text = row("TxtProposalId").ToString
            Me.LbCustomerName.Text = row("CustomerName").ToString
            Me.LbPropDescription.Text = row("ProposalName").ToString
            Me.LbCreateDate.text = row("TxtCreateDate").ToString
            Me.LbEndDate.Text = Format(Now().AddDays(CInt(row("ProposalEndDate").ToString)), "yyyy/MM/dd") & " (" & row("ProposalEndDate").ToString & " dia(s)) "
        Next




    End Sub
    Private Function RequestStatus() As String

        Dim strPropId As String = Request.QueryString("PI")

        Dim SqlSelect As String = "SELECT * FROM Proposals WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strPropId & ""

        RequestStatus = Getdata.retrieve_sql_field_value(SqlSelect, "StatusId")


    End Function
    Private Function RequestProposalProducts() As String

        Dim strPropId As String = Request.QueryString("PI")

        Dim SqlSelect As String = "SELECT Count(Id) as PropProductsCount FROM ProposalDetails WHERE TenantId=" & Getdata.TenantId & " AND ProposalId=" & strPropId & ""

        RequestProposalProducts = Getdata.retrieve_sql_field_value(SqlSelect, "PropProductsCount")

    End Function

    Private Function RequestTaskCount(strPropTask As String) As Integer

        Dim strPropId As String = Request.QueryString("PI")

        If strPropTask = 0 Then

            Dim SqlSelect As String = "SELECT Count(Id) as TaskCount FROM ProposalTasks WHERE TenantId=" & Getdata.TenantId & " AND ProposalId=" & strPropId & " AND TaskStatusId=1"

            RequestTaskCount = Getdata.retrieve_sql_field_value(SqlSelect, "TaskCount")

        Else

            Dim SqlSelect As String = "SELECT Count(Id) as TaskCount FROM ProposalTasks WHERE TenantId=" & Getdata.TenantId & " AND ProposalId=" & strPropId & " AND ProposalStatusId=" & strPropTask & " AND TaskStatusId=1"

            RequestTaskCount = Getdata.retrieve_sql_field_value(SqlSelect, "TaskCount")


        End If




    End Function

    Private Function ChangeStatus(strChangeTo As String) As Boolean

        Dim strPropId As String = Request.QueryString("PI")
        Dim SqlUpdate As String = "UPDATE Proposals Set StatusId=" & strChangeTo & " , ModifiedDate=GETDATE(), ModifiedUserId=" & Getdata.UserId & " Where Id=" & strPropId & " AND TenantId=" & Getdata.TenantId & ""
        Dim strInsert As String = ""
        Dim SqlInsertLog As String = "INSERT INTO ProposalStatusLogs (TenantId, ProposalId, OldStatusId, NewStatusId, VariableDate, CreateDate, CreateUserId, ModifiedDate, ModifiedUserId) VALUES (" & _
                                        Getdata.TenantId & "," & strPropId & ""

        Try

            Select Case strChangeTo
                Case 1 ' change to status 2 = Para Validação 

                    strInsert = "," & RequestStatus() & ",2,GETDATE(),GETDATE()," & Getdata.UserId & ",GETDATE()," & Getdata.UserId & ")"

                    SqlInsertLog = SqlInsertLog & strInsert

                    Getdata.run_sql_query(SqlUpdate)
                    Getdata.run_sql_query(SqlInsertLog)

                    ChangeStatus = True



                Case 2 ' change to status 2 = Para Validação 
                    ShowAlertPanelWarning("Pretende alterar o status da proposta para", "VAlIDAÇÃO", AlertDiv, LbAlert, LbConfirmLabel, CbxConfirm, TxtVariableDate, False, False, "2")

                    If CbxConfirm.Checked = True Then

                        strInsert = "," & RequestStatus() & ",2,GETDATE(),GETDATE()," & Getdata.UserId & ",GETDATE()," & Getdata.UserId & ")"

                        SqlInsertLog = SqlInsertLog & strInsert

                        Dim strAprovalId As String = Me.CbUserAproval.SelectedValue

                        Dim SqlUpdateAproval As String = "UPDATE Proposals Set AprovalUserId=" & strAprovalId & " , ModifiedDate=GETDATE(), ModifiedUserId=" & Getdata.UserId & " Where Id=" & strPropId & " AND TenantId=" & Getdata.TenantId & ""

                        Getdata.run_sql_query(SqlUpdate)
                        Getdata.run_sql_query(SqlUpdateAproval)
                        Getdata.run_sql_query(SqlInsertLog)

                        ChangeStatus = True

                    Else

                        ChangeStatus = False

                    End If



                Case 3 ' change to status 3 = Validada 

                    strInsert = "," & RequestStatus() & ",3,GETDATE(),GETDATE()," & Getdata.UserId & ",GETDATE()," & Getdata.UserId & ")"

                    SqlInsertLog = SqlInsertLog & strInsert

                    Getdata.run_sql_query(SqlUpdate)
                    Getdata.run_sql_query(SqlInsertLog)
                    ChangeStatus = True

                Case 4 ' change to status 4 = ANULADA
                    ShowAlertPanelWarning("Pretende alterar o status da proposta para", "ANULADA", AlertDiv, LbAlert, LbConfirmLabel, CbxConfirm, TxtVariableDate, False, False, "4")

                    If CbxConfirm.Checked = True Then

                        strInsert = "," & RequestStatus() & ",4,GETDATE(),GETDATE()," & Getdata.UserId & ",GETDATE()," & Getdata.UserId & ")"

                        SqlInsertLog = SqlInsertLog & strInsert


                        Getdata.run_sql_query(SqlUpdate)
                        Getdata.run_sql_query(SqlInsertLog)

                        ChangeStatus = True

                    Else

                        ChangeStatus = False


                    End If

                Case 5 ' change to status 5 = SUBMETIDA

                    ShowAlertPanelWarning("Pretende alterar o status da proposta para", "SUBMETIDA", AlertDiv, LbAlert, LbConfirmLabel, CbxConfirm, TxtVariableDate, True, False, "5")



                    If CbxConfirm.Checked = True And Len(TxtVariableDate.Text) <> Nothing Then

                        Dim strDate As String = DateTime.Parse(TxtVariableDate.Text)
                        strDate = CDate(strDate).ToString("yyyy-MM-dd")

                        Dim SqlUpdateSubmitUser As String = "UPDATE Proposals Set SubmitDate=CONVERT(datetime, '" & strDate & "', 120), UserSubmit=" & Getdata.UserId & ", ModifiedDate=GETDATE(), ModifiedUserId=" & Getdata.UserId & " Where Id=" & strPropId & " AND TenantId=" & Getdata.TenantId & ""

                        strInsert = "," & RequestStatus() & ",5,GETDATE(),GETDATE()," & Getdata.UserId & ",GETDATE()," & Getdata.UserId & ")"

                        SqlInsertLog = SqlInsertLog & strInsert


                        Getdata.run_sql_query(SqlUpdate)
                        Getdata.run_sql_query(SqlUpdateSubmitUser)
                        Getdata.run_sql_query(SqlInsertLog)
                        ChangeStatus = True

                    Else

                        ChangeStatus = False


                    End If


                Case 6 ' change to status 6 = CONTRATO

                    ShowAlertPanelWarning("Pretende alterar o status da proposta para", "CONTRATO", AlertDiv, LbAlert, LbConfirmLabel, CbxConfirm, TxtVariableDate, False, False, "6")



                    If CbxConfirm.Checked = True Then

                        strInsert = "," & RequestStatus() & ",6,GETDATE(),GETDATE()," & Getdata.UserId & ",GETDATE()," & Getdata.UserId & ")"

                        SqlInsertLog = SqlInsertLog & strInsert
                        Getdata.run_sql_query(SqlUpdate)

                        Getdata.run_sql_query(SqlInsertLog)
                        ChangeStatus = True

                    Else

                        ChangeStatus = False


                    End If



                Case 7 ' change to status 7 = RECUSADA

                    ShowAlertPanelWarning("Pretende alterar o status da proposta para", "RECUSADA", AlertDiv, LbAlert, LbConfirmLabel, CbxConfirm, TxtVariableDate, False, True, "7")


                    If CbxConfirm.Checked = True And CbForsakeMotives.SelectedValue <> "" Then

                        Dim SqlUpdateMotive As String = "UPDATE Proposals Set MotiveId=" & CbForsakeMotives.SelectedValue & ", ModifiedDate=GETDATE(), ModifiedUserId=" & Getdata.UserId & " Where Id=" & strPropId & " AND TenantId=" & Getdata.TenantId & ""


                        strInsert = "," & RequestStatus() & ",7,GETDATE(),GETDATE()," & Getdata.UserId & ",GETDATE()," & Getdata.UserId & ")"

                        SqlInsertLog = SqlInsertLog & strInsert


                        Getdata.run_sql_query(SqlUpdate)
                        Getdata.run_sql_query(SqlUpdateMotive)
                        Getdata.run_sql_query(SqlInsertLog)
                        ChangeStatus = True

                    Else

                        ChangeStatus = False


                    End If

                Case 9

                    ShowAlertPanelWarning("Pretende alterar o status da proposta para", "FECHADA", AlertDiv, LbAlert, LbConfirmLabel, CbxConfirm, TxtVariableDate, False, False, "9")



                    If CbxConfirm.Checked = True Then

                        strInsert = "," & RequestStatus() & ",9,GETDATE(),GETDATE()," & Getdata.UserId & ",GETDATE()," & Getdata.UserId & ")"

                        SqlInsertLog = SqlInsertLog & strInsert
                        Getdata.run_sql_query(SqlUpdate)

                        Getdata.run_sql_query(SqlInsertLog)
                        ChangeStatus = True

                    Else

                        ChangeStatus = False


                    End If




            End Select

        Catch ex As Exception

            ChangeStatus = False


        End Try




    End Function

    Private Sub ProposalStatusRules()

        Dim strStatusId As String = RequestStatus()

        ' BUTTONS VARIABLES
        Dim BtnToEdit As LinkButton = PropToEdit
        Dim BtnToValidate As LinkButton = PropToValidate
        Dim BtnValidate As LinkButton = PropValidate
        Dim BtnSubmit As LinkButton = PropSubmit
        Dim BtnContract As LinkButton = PropContract
        Dim BtnForsake As LinkButton = PropForsake
        Dim BtnInvalid As LinkButton = PropInvalid
        Dim BtnClosed As LinkButton = PropClose


        'GRIDS VARIABLES 
        Dim strGdEditProducts As GridView = GdEditProducts
        Dim strGdContract As GridView = GdProdContract


        Select Case strStatusId

            Case "?"

                ' Hide
                BtnToEdit.Visible = False
                BtnValidate.Visible = False
                BtnSubmit.Visible = False
                BtnContract.Visible = False
                BtnForsake.Visible = False
                BtnToValidate.Visible = False
                BtnInvalid.Visible = False
                BtnClosed.Visible = False


                ActionsTitle.Visible = False


            Case 1 ' Status NOVO

                'Show 
                BtnToValidate.Visible = True
                BtnInvalid.Visible = True


                strGdEditProducts.Visible = True ' GRID GdEditProducts
                strGdContract.DataSource = ""


                ' Hide
                BtnToEdit.Visible = False
                BtnValidate.Visible = False
                BtnSubmit.Visible = False
                BtnContract.Visible = False
                BtnForsake.Visible = False
                BtnClosed.Visible = False

                strGdContract.Visible = False ' Grid GdContract
                strGdContract.DataSourceID = ""

                strGdEditProducts.Columns(8).Visible = False


            Case 2 ' STATUS P/VALIDAÇÃO

                'Show 
                BtnToEdit.Visible = True
                BtnValidate.Visible = True
                BtnInvalid.Visible = True

                strGdEditProducts.Visible = True ' GRID GdEditProducts
                strGdContract.DataSource = ""

                ' Hide
                BtnToValidate.Visible = False
                BtnSubmit.Visible = False
                BtnContract.Visible = False
                BtnForsake.Visible = False
                BtnClosed.Visible = False


                strGdContract.Visible = False ' Grid GdContract
                strGdContract.DataSourceID = ""

                strGdEditProducts.Columns(8).Visible = False


            Case 3 ' STATUS VALIDADA

                'Show 
                BtnToEdit.Visible = True
                BtnSubmit.Visible = True
                BtnInvalid.Visible = True

                ' Hide
                BtnValidate.Visible = False
                BtnToValidate.Visible = False
                BtnContract.Visible = False
                BtnForsake.Visible = False
                BtnClosed.Visible = False

                strGdContract.Visible = False ' Grid GdContract
                strGdContract.DataSourceID = ""

                strGdEditProducts.Columns(8).Visible = False

            Case 4 ' STATUS INVALIDA - Do not show any button

                ' Hide
                BtnToEdit.Visible = False
                BtnSubmit.Visible = False
                BtnInvalid.Visible = False
                BtnValidate.Visible = False
                BtnToValidate.Visible = False
                BtnContract.Visible = False
                BtnForsake.Visible = False
                BtnClosed.Visible = False

                ActionsTitle.Visible = False


                strGdContract.Visible = False ' Grid GdContract
                strGdContract.DataSourceID = ""
                strGdEditProducts.Columns(8).Visible = False

            Case 5 ' STATUS SUBMETIDA
                'Show 

                BtnInvalid.Visible = True
                BtnClosed.Visible = True

                strGdEditProducts.Visible = True ' GRID GdEditProducts
                strGdEditProducts.Columns(8).Visible = True

                ' Hide
                BtnToEdit.Visible = False
                BtnValidate.Visible = False
                BtnToValidate.Visible = False
                BtnSubmit.Visible = False
                BtnContract.Visible = False
                BtnForsake.Visible = False


                strGdContract.Visible = False ' Grid GdContract
                strGdContract.DataSourceID = ""


            Case 6 ' STATUS CONTRACTO

                'Show 
                BtnInvalid.Visible = True
                strGdContract.Visible = True ' Grid GdContract
                BtnClosed.Visible = False

                ' Hide
                BtnToEdit.Visible = False
                BtnValidate.Visible = False
                BtnToValidate.Visible = False
                BtnSubmit.Visible = False
                BtnForsake.Visible = False
                BtnContract.Visible = False
                strGdEditProducts.Visible = False ' GRID GdEditProducts

                strGdEditProducts.Columns(8).Visible = False

            Case 7 ' STATUS RECUSADA

                'Show 
                BtnInvalid.Visible = True
                strGdEditProducts.Visible = True ' GRID GdEditProducts

                ' Hide
                BtnToEdit.Visible = False
                BtnValidate.Visible = False
                BtnToValidate.Visible = False
                BtnSubmit.Visible = False
                BtnForsake.Visible = False
                BtnContract.Visible = False
                strGdContract.Visible = False ' Grid GdContract
                strGdContract.DataSourceID = ""
                BtnClosed.Visible = False


                strGdEditProducts.Columns(8).Visible = False


            Case 9

                BtnInvalid.Visible = True
                strGdContract.Visible = True ' Grid GdContract
                BtnClosed.Visible = False

                ' Hide
                BtnToEdit.Visible = False
                BtnValidate.Visible = False
                BtnToValidate.Visible = False
                BtnSubmit.Visible = False
                BtnForsake.Visible = False
                BtnContract.Visible = False
                strGdEditProducts.Visible = False ' GRID GdEditProducts

                'strGdEditProducts.Columns(8).Visible = False


        End Select

    End Sub

    Private Sub ProposalTaskCounters()

        Dim strPropId As String = Request.QueryString("PI")

        Dim strNowDate As Date = DateTime.Now.ToString("dd-MM-yyyy")

        Dim SqlTaskList As String = "SELECT * FROM VwProposalTasks WHERE TenantId=" & Getdata.TenantId & " AND ProposalId=" & strPropId & ""
        Dim dt As DataTable = Getdata.GetDataTable(SqlTaskList)

        '"Forename ='" & forename & "' and Surname = '" & surname & "'"

        'If strNowDate >= strStartDate And strNowDate <= strEndDate Then
        'If strNowDate > strStartDate And strNowDate > strEndDate Then



        Dim R1() As DataRow = dt.Select("'" & strNowDate & "'<StartDate And TaskStatusId = 1")
        Dim R2() As DataRow = dt.Select("'" & strNowDate & "'>=StartDate AND '" & strNowDate & "'<=EndDate AND TaskStatusId = 1")
        Dim R3() As DataRow = dt.Select("'" & strNowDate & "'>StartDate AND '" & strNowDate & "'>EndDate AND TaskStatusId = 1")
        Dim R4() As DataRow = dt.Select("TaskStatusId = 2")
        Dim R5() As DataRow = dt.Select("TaskStatusId = 3")

        Dim CountScheduled As Integer = R1.Count
        Dim CountOnGoing As Integer = R2.Count
        Dim CountOverdue As Integer = R3.Count
        Dim CountClosed As Integer = R4.Count
        Dim CountInvalid As Integer = R5.Count

        Dim TotalCount As Integer = CountScheduled + CountOnGoing + CountOverdue + CountClosed + CountInvalid

        If TotalCount = 0 Then

            LiTotal.Visible = False
            LiOverdue.Visible = False
            LiScheduled.Visible = False
            LiOnGoing.Visible = False
            LiClosed.Visible = False
            LiInvalid.Visible = False
            TaskCountTitle.Visible = False

        Else

            LbTotal.Text = TotalCount

            If CountScheduled = 0 Then
                LiScheduled.Visible = False
            Else
                LbScheduled.Text = CountScheduled
            End If

            If CountOnGoing = 0 Then
                LiOnGoing.Visible = False
            Else
                LbOnGoing.Text = CountOnGoing
            End If

            If CountOverdue = 0 Then
                LiOverdue.Visible = False
            Else
                LbOverdue.Text = CountOverdue
            End If

            If CountClosed = 0 Then
                LiClosed.Visible = False
            Else
                LbClosed.Text = CountClosed
            End If

            If CountInvalid = 0 Then
                LiInvalid.Visible = False
            Else
                LbInvalid.Text = CountInvalid
            End If


        End If
    End Sub

    Private Sub TaskSearch(strItem As GridEditableItem, strGrid As RadGrid)


        Dim strDescription As TextBox = DirectCast(strItem.FindControl("TxtInsertTaskDescription"), TextBox)

        Dim CbTaskList As DropDownList = DirectCast(strItem.FindControl("CbProposalList"), DropDownList)

        Dim SqlProduct As String = "SELECT * FROM ProposalTaskList WHERE TenantId=" & Getdata.TenantId & "AND Description LIKE '%" & strDescription.Text & "%'"

        Dim dt As DataTable = Getdata.retrive_tablelist(SqlProduct)

        If dt.Rows.Count > 0 Then

            'DirectCast(item.FindControl("UpdProdPanel"), UpdatePanel).Visible = True
            DirectCast(strItem.FindControl("TxtInsertTaskDescription"), TextBox).Visible = False
            DirectCast(strItem.FindControl("BtnTaskSearch"), LinkButton).Visible = False
            'DirectCast(strItem.FindControl("CbProductTypes"), DropDownList).Enabled = False

            CbTaskList.Visible = True

            CbTaskList.DataSource = dt
            CbTaskList.DataTextField = "Description"
            CbTaskList.DataValueField = "Id"
            'ddl.SelectedValue = "Id"
            CbTaskList.DataBind()


        Else

            'DirectCast(item.FindControl("UpdProdPanel"), UpdatePanel).Visible = False
            DirectCast(strItem.FindControl("TxtInsertTaskDescription"), TextBox).Visible = True
            DirectCast(strItem.FindControl("BtnTaskSearch"), LinkButton).Visible = True
            CbTaskList.Visible = False


        End If

    End Sub




#End Region ' ############## REQUESTS AND LOAD RULES

#Region "ACTION BUTTONS"
    Protected Sub BtnActionOk_Click1(sender As Object, e As EventArgs) Handles BtnActionOk.Click

        Dim strPropId As String = Request.QueryString("PI")
        Dim strOppId As String = Request.QueryString("OI")

        Dim strStatusId As String = LbStatusId.Text

        Select Case strStatusId

            Case 0

            Case 2
                If ChangeStatus(2) = True Then

                    Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

                    'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)


                End If


            Case 4
                If ChangeStatus(4) = True Then

                    Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

                    'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)


                End If
            Case 5
                If ChangeStatus(5) = True Then

                    Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

                    'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)


                End If
            Case 6
                If ChangeStatus(6) = True Then

                    Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

                    'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)


                End If
            Case 7
                If ChangeStatus(7) = True Then

                    Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

                    'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)


                End If

            Case 9
                If ChangeStatus(9) = True Then

                    Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

                    'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)


                End If


        End Select





    End Sub
    Protected Sub PropToEdit_Click(sender As Object, e As EventArgs) Handles PropToEdit.Click
        Dim strPropId As String = Request.QueryString("PI")
        Dim strOppId As String = Request.QueryString("OI")

        Dim strStatusTo As String = 1

        If RequestProposalProducts() = 0 Then

            ShowAlertPanelError("A proposta tem que ter pelo menos um produto associado!", AlertDiv, LbAlert)
        Else

            If ChangeStatus(strStatusTo) = True Then

                Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

                'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)

            End If
        End If

    End Sub
    Protected Sub PropToValidate_Click(sender As Object, e As EventArgs) Handles PropToValidate.Click

        Dim strPropId As String = Request.QueryString("PI")
        Dim strOppId As String = Request.QueryString("OI")

        Dim strStatusTo As String = 2

        'If RequestTaskCount(1) > 0 Then

        '    ShowAlertPanelError("Existem tarefas Abertas, para continuar terá que fechar as mesmas!", AlertDiv, LbAlert)

        'Else

        If RequestProposalProducts() = 0 Then

            ShowAlertPanelError("A proposta tem que ter pelo menos um produto associado!", AlertDiv, LbAlert)
        Else

            If ChangeStatus(strStatusTo) = True Then

                Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

                'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)

            End If
        End If
        'End If

    End Sub
    Protected Sub PropValidate_Click(sender As Object, e As EventArgs) Handles PropValidate.Click

        Dim strPropId As String = Request.QueryString("PI")
        Dim strOppId As String = Request.QueryString("OI")

        Dim strStatusTo As String = 3


        'If RequestTaskCount(2) > 0 Then

        '    ShowAlertPanelError("Existem tarefas Abertas, para continuar terá que fechar as mesmas!", AlertDiv, LbAlert)

        'Else

        If ChangeStatus(strStatusTo) = True Then

            Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

            'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)


        End If

        'End If



    End Sub
    Protected Sub PropSubmit_Click(sender As Object, e As EventArgs) Handles PropSubmit.Click

        Dim strPropId As String = Request.QueryString("PI")
        Dim strOppId As String = Request.QueryString("OI")

        Dim strStatusTo As String = 5

        If RequestTaskCount(3) > 0 Then

            ShowAlertPanelError("Existem tarefas Abertas, para continuar terá que fechar as mesmas!", AlertDiv, LbAlert)

        Else

            If ChangeStatus(strStatusTo) = True Then

                Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

                'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)


            End If
        End If



    End Sub
    Protected Sub PropInvalid_Click(sender As Object, e As EventArgs) Handles PropInvalid.Click

        Dim strPropId As String = Request.QueryString("PI")
        Dim strOppId As String = Request.QueryString("OI")

        Dim strStatusTo As String = 4
        'If RequestTaskCount(0) > 0 Then ' ##### Ver como se faz neste caso!!!!!! --> Para Anular! 

        '    ShowAlertPanelError("Existem tarefas Abertas, para continuar terá que fechar as mesmas!", AlertDiv, LbAlert)

        'Else

        If ChangeStatus(strStatusTo) = True Then

            Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

            'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)


        End If

        'End If



    End Sub
    Protected Sub PropContract_Click(sender As Object, e As EventArgs) Handles PropContract.Click

        Dim strPropId As String = Request.QueryString("PI")
        Dim strOppId As String = Request.QueryString("OI")

        Dim strStatusTo As String = 6

        'If RequestTaskCount(5) > 0 Then

        '    ShowAlertPanelError("Existem tarefas Abertas, para continuar terá que fechar as mesmas!", AlertDiv, LbAlert)

        'Else

        If ChangeStatus(strStatusTo) = True Then

            Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

            'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)


        End If

        'End If



    End Sub
    Protected Sub PropForsake_Click(sender As Object, e As EventArgs) Handles PropForsake.Click

        Dim strPropId As String = Request.QueryString("PI")
        Dim strOppId As String = Request.QueryString("OI")

        Dim strStatusTo As String = 7

        'If RequestTaskCount(5) > 0 Then

        '    ShowAlertPanelError("Existem tarefas Abertas, para continuar terá que fechar as mesmas!", AlertDiv, LbAlert)

        'Else

        If ChangeStatus(strStatusTo) = True Then

            Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

            'ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)


        End If

        'End If



    End Sub
    Protected Sub PropClose_Click(sender As Object, e As EventArgs) Handles PropClose.Click

        Dim strPropId As String = Request.QueryString("PI")
        Dim strOppId As String = Request.QueryString("OI")

        Dim strStatusTo As String = 9

        'If RequestTaskCount(5) > 0 Then

        '    ShowAlertPanelError("Existem tarefas Abertas, para continuar terá que fechar as mesmas!", AlertDiv, LbAlert)

        'Else

        If ChangeStatus(strStatusTo) = True Then

            Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)

            ShowAlertPanelValid("Status alterado com Sucesso!", AlertDiv, LbAlert)


        End If

        'End If





    End Sub

#End Region   ' ################################# ACTION BUTTONS - Change Status 

#Region "FORMPROPOSAL" ' ################################# FORM PROPOSAL 

    Protected Sub PropShowCaret_Click(sender As Object, e As EventArgs) Handles PropShowCaret.Click
        Me.ProposalPanel.Visible = True
        Me.PropShowCaret.Visible = False
        Me.PropHideCaret.Visible = True



    End Sub

    Protected Sub PropHideCaret_Click(sender As Object, e As EventArgs) Handles PropHideCaret.Click

        Me.ProposalPanel.Visible = False
        Me.PropShowCaret.Visible = True
        Me.PropHideCaret.Visible = False


    End Sub

#End Region ' ################################# FORM PROPOSAL RULES AND ACTIONS

#Region "FORMOPPORTUNITY"
    'Protected Sub OppShowPanel_Click(sender As Object, e As EventArgs) Handles OppShowPanel.Click

    '    Me.OpportunityPanel.Visible = True
    '    Me.OppShowPanel.Visible = False
    '    Me.OppHidePanel.Visible = True




    'End Sub

    'Protected Sub OppHidePanel_Click(sender As Object, e As EventArgs) Handles OppHidePanel.Click
    '    Me.OpportunityPanel.Visible = False
    '    Me.OppShowPanel.Visible = True
    '    Me.OppHidePanel.Visible = False


    'End Sub
#End Region ' ################################# FORM OPPORTUNITY RULES AND ACTIONS

#Region "GRID PRODUCTS" ' ################################# ACTION BUTTONS - Change Status 

    Protected Sub GdPropProducts_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles GdPropProducts.ItemDataBound

        'If TypeOf e.Item Is GridDataItem Then
        '    If e.Item.OwnerTableView.Name = "parent" Then
        '        ' from parent
        '    ElseIf e.Item.OwnerTableView.Name = "child1" Then
        '        ' from first child
        '    ElseIf e.Item.OwnerTableView.Name = "child2" Then
        '        ' from second child
        '    End If

        'End If


        If TypeOf e.Item Is GridDataItem And e.Item.OwnerTableView.Name = "parent" Then

            Dim dataItem As GridDataItem = CType(e.Item, GridDataItem)

            Dim itemValue As String

            ' BUTTONS VARIABLES
            Dim BtnRowEdit As LinkButton = CType(dataItem.FindControl("BtnRowEdit"), LinkButton)
            Dim BtnAddAUE As Button = CType(dataItem.FindControl("BtnAddAUE"), Button)

            Dim BtnShowChild As LinkButton = CType(dataItem.FindControl("BtnShowChild"), LinkButton)
            Dim BtnHideChild As LinkButton = CType(dataItem.FindControl("BtnHideChild"), LinkButton)
            'Dim BtnHideSecondChild As LinkButton = CType(dataItem.FindControl("BtnHideSecondChild"), LinkButton)

            ' LABELS VARIABLES
            Dim LbProductName As Label
            Dim CbProduct As DropDownList

            If e.Item.IsInEditMode = True Then

                itemValue = CType(dataItem.FindControl("LbPropDetailId"), Label).Text
                CbProduct = CType(dataItem.FindControl("CbProductList"), DropDownList)

            Else

                itemValue = CType(dataItem.FindControl("LbPropDetailId"), Label).Text
                LbProductName = CType(dataItem.FindControl("TxtProductNameLabel"), Label)
                CbProduct = CType(dataItem.FindControl("CbProductList"), DropDownList)

            End If


            Dim LbPropQt As Label = CType(dataItem.FindControl("LbPropQt"), Label)
            Dim LbPropFreeQty As Label = CType(dataItem.FindControl("LbFreeQty"), Label)
            Dim LbPropPkQt As Label = CType(dataItem.FindControl("LbPackageQty"), Label)
            Dim LbPropDiscount As Label = CType(dataItem.FindControl("Lbdiscount"), Label)
            Dim LbPropDevMonths As Label = CType(dataItem.FindControl("LbDeliveryMonths"), Label)
            Dim LbPropPrice As Label = CType(dataItem.FindControl("LbPropPrice"), Label)
            Dim LbPropValidity As Label = CType(dataItem.FindControl("LbPropPrice"), Label)

            ' TEXT BOX ' LABELS VARIABLES

            Dim TxtPropQt As TextBox = CType(dataItem.FindControl("TxtPropQt"), TextBox)
            Dim TxtPropFreeQt As TextBox = CType(dataItem.FindControl("TxtFreeQty"), TextBox)
            Dim TxtPropPkgQty As TextBox = CType(dataItem.FindControl("TxtPackageQty"), TextBox)
            Dim TxtPropDiscount As TextBox = CType(dataItem.FindControl("TxtDiscount"), TextBox)
            Dim TxtPropDevMonths As TextBox = CType(dataItem.FindControl("TxtDeliveryMonths"), TextBox)
            Dim TxtPropPrice As TextBox = CType(dataItem.FindControl("TxtPropPrice"), TextBox)
            Dim TxtPropValidity As TextBox = CType(dataItem.FindControl("TxtPropValidity"), TextBox)

            'Link Button and Product Panel Info

            Dim parentRow As GridDataItem = TryCast(e.Item, GridDataItem)

            Dim strProdInfoPanel As HtmlControl = DirectCast(parentRow.FindControl("ItemProductPanel"), HtmlControl)
            Dim strShowInfoLink As LinkButton = DirectCast(parentRow.FindControl("ItemProductShow"), LinkButton)
            Dim strHideInfoLink As LinkButton = DirectCast(parentRow.FindControl("ItemProductHide"), LinkButton)

            ' PRODUCT FOOTER INFORMATION 
            Dim LbTotalAUE As Label = CType(dataItem.FindControl("LbProdTotalAUE"), Label)
            Dim LbTotalSAP As Label = CType(dataItem.FindControl("LbProdTotalSAP"), Label)
            Dim LbTotalSales As Label = CType(dataItem.FindControl("LbTotalSale"), Label)


            If itemValue = "" Then


                ' HIDE LABELS 
                LbProductName.Visible = False
                LbPropQt.Visible = False
                LbPropFreeQty.Visible = False
                LbPropPkQt.Visible = False
                LbPropDiscount.Visible = False
                LbPropDevMonths.Visible = False

                LbPropPrice.Visible = False
                LbPropValidity.Visible = False

                BtnRowEdit.Visible = False

                LbTotalAUE.Text = 0

                LbTotalSales.Text = 0

                ' SHOW TEXT BOX 
                CbProduct.Visible = True
                TxtPropQt.Visible = True
                TxtPropFreeQt.Visible = True
                TxtPropQt.Visible = True
                TxtPropPkgQty.Visible = True
                TxtPropDiscount.Visible = True
                TxtPropDevMonths.Visible = True
                TxtPropPrice.Visible = True
                TxtPropValidity.Visible = True

                'HIDE INFO PANEL
                strProdInfoPanel.Visible = False
                strShowInfoLink.Visible = True
                strHideInfoLink.Visible = False

            Else
                If e.Item.IsInEditMode = True Then
                    strShowInfoLink.Visible = False






                Else

                    'SHOW LABELS
                    LbProductName.Visible = False
                    LbPropQt.Visible = True
                    LbPropFreeQty.Visible = False
                    'LbPropPkQt.Visible = False
                    LbPropDiscount.Visible = False
                    LbPropDevMonths.Visible = True
                    LbPropPrice.Visible = True
                    LbPropValidity.Visible = True

                    ProductsInfoTotals(dataItem, LbTotalAUE, LbTotalSAP, LbTotalSales)


                    BtnRowEdit.Visible = True

                    ' HIDE TEXT BOX 
                    CbProduct.Visible = False
                    TxtPropQt.Visible = False
                    TxtPropFreeQt.Visible = False

                    TxtPropDiscount.Visible = False
                    TxtPropPkgQty.Visible = False
                    TxtPropDevMonths.Visible = False
                    TxtPropPrice.Visible = False
                    TxtPropValidity.Visible = False

                    'HIDE INFO PANEL
                    strProdInfoPanel.Visible = False
                    strShowInfoLink.Visible = True
                    strHideInfoLink.Visible = False



                    Dim strStatus As String = RequestStatus()

                    Select Case strStatus

                        Case 1

                            BtnAddAUE.Visible = False

                        Case 2

                            BtnAddAUE.Visible = False

                        Case 3

                            BtnAddAUE.Visible = False

                        Case 4

                            BtnAddAUE.Visible = False

                        Case 5

                            BtnAddAUE.Visible = False

                        Case 6

                            BtnAddAUE.Visible = True
                            BtnShowChild.Visible = True
                            BtnHideChild.Visible = False
                            'BtnHideSecondChild.Visible = False

                        Case 7
                            BtnAddAUE.Visible = False


                    End Select


                End If
            End If
        End If



        If TypeOf e.Item Is GridDataItem And e.Item.OwnerTableView.Name = "FirstChild" Then

            Dim dataItem As GridDataItem = CType(e.Item, GridDataItem)

            Dim BtnShowSecondChild As LinkButton = CType(dataItem.FindControl("BtnShowSecondChild"), LinkButton)
            Dim BtnHideSecondChild As LinkButton = CType(dataItem.FindControl("BtnHideSecondChild"), LinkButton)

            Dim detailTable As GridTableView = CType(e.Item.OwnerTableView, GridTableView)


            If e.Item.IsInEditMode = True Then

                detailTable.GetColumn("ExpandChild").Visible = False
                detailTable.GetColumn("RequestInfo").Visible = False
                detailTable.GetColumn("RequestMasterId").Visible = True
                detailTable.GetColumn("RequestSlaveId").Visible = True
                detailTable.GetColumn("RequestDescription").Visible = True
                detailTable.GetColumn("RequestQuantity").Visible = True


            Else

                If detailTable.IsItemInserted = True Then

                    detailTable.GetColumn("ExpandChild").Visible = False
                    detailTable.GetColumn("RequestInfo").Visible = False
                    detailTable.GetColumn("RequestMasterId").Visible = True
                    detailTable.GetColumn("RequestSlaveId").Visible = True
                    detailTable.GetColumn("RequestDescription").Visible = True
                    detailTable.GetColumn("RequestQuantity").Visible = True


                Else

                    BtnHideSecondChild.Visible = False
                    detailTable.GetColumn("RequestInfo").Visible = True
                    detailTable.GetColumn("RequestMasterId").Visible = False
                    detailTable.GetColumn("RequestSlaveId").Visible = False
                    detailTable.GetColumn("RequestDescription").Visible = False
                    detailTable.GetColumn("RequestQuantity").Visible = False

                End If




            End If



        End If





    End Sub

    Protected Sub GdPropProducts_DataBound(sender As Object, e As EventArgs) Handles GdPropProducts.DataBound

        Dim strStatusId As String = RequestStatus()

        Dim stredititems As String = GdPropProducts.EditItems.Count

        For Each item As GridDataItem In GdPropProducts.MasterTableView.Items

            Dim itemValue As String = CType(item.FindControl("LbPropDetailId"), Label).Text
            Dim strShowInfoLink As LinkButton = DirectCast(item.FindControl("ItemProductShow"), LinkButton)
            Dim strHideInfoLink As LinkButton = DirectCast(item.FindControl("ItemProductHide"), LinkButton)
            Dim BtnAddAUE As Button = CType(item.FindControl("BtnAddAUE"), Button)

            Dim CommandItem As GridCommandItem = TryCast(GdPropProducts.MasterTableView.GetItems(GridItemType.CommandItem)(0), GridCommandItem)
            Dim BtnAdd As Button = CType(CommandItem.FindControl("BtnProdAdd"), Button)
            Dim BtnClean As Button = CType(CommandItem.FindControl("BtnClean"), Button)

            Dim BtnRowEdit As LinkButton = CType(item.FindControl("BtnRowEdit"), LinkButton)

            Dim BtnShowChild As LinkButton = CType(item.FindControl("BtnShowChild"), LinkButton)
            Dim BtnHideChild As LinkButton = CType(item.FindControl("BtnHideChild"), LinkButton)



            If item.IsInEditMode = True Then
                BtnAdd.Visible = False
                BtnClean.Visible = False



            Else

                If stredititems > 0 Then

                    For i As Integer = 0 To item.Controls.Count - 1
                        BtnRowEdit.Visible = False
                        BtnAddAUE.Visible = False
                        TryCast(item.Controls(i), GridTableCell).Enabled = False
                    Next

                Else


                    Select Case strStatusId


                        Case "1"

                            If itemValue <> "" Then
                                BtnRowEdit.Visible = True
                                BtnAddAUE.Visible = False
                            Else
                                BtnRowEdit.Visible = False
                                BtnAddAUE.Visible = False
                            End If

                            GdPropProducts.MasterTableView.GetColumn("ExpandParent").Visible = False

                        Case "2"
                            If itemValue = "" Then
                                item.Display = False  '//hide the row
                            End If

                            BtnRowEdit.Visible = False
                            BtnAddAUE.Visible = False

                            For i As Integer = 0 To item.Controls.Count - 1

                                TryCast(item.Controls(i), GridTableCell).Enabled = False
                            Next
                            BtnAdd.Visible = False
                            BtnClean.Visible = False
                            GdPropProducts.MasterTableView.GetColumn("ExpandParent").Visible = False

                        Case "3"
                            If itemValue = "" Then
                                item.Display = False  '//hide the row
                            End If

                            BtnRowEdit.Visible = False
                            BtnAddAUE.Visible = False

                            For i As Integer = 0 To item.Controls.Count - 1

                                TryCast(item.Controls(i), GridTableCell).Enabled = False
                            Next

                            BtnAdd.Visible = False
                            BtnClean.Visible = False
                            GdPropProducts.MasterTableView.GetColumn("ExpandParent").Visible = False
                        Case "4"
                            If itemValue = "" Then
                                item.Display = False  '//hide the row
                            End If

                            BtnRowEdit.Visible = False
                            BtnAddAUE.Visible = False

                            For i As Integer = 0 To item.Controls.Count - 1

                                TryCast(item.Controls(i), GridTableCell).Enabled = False
                            Next

                            BtnAdd.Visible = False
                            BtnClean.Visible = False
                            GdPropProducts.MasterTableView.GetColumn("ExpandParent").Visible = False
                        Case "5"
                            If itemValue = "" Then
                                item.Display = False  '//hide the row
                            End If

                            BtnRowEdit.Visible = False
                            BtnAddAUE.Visible = False

                            For i As Integer = 0 To item.Controls.Count - 1

                                TryCast(item.Controls(i), GridTableCell).Enabled = False
                            Next

                            BtnAdd.Visible = False
                            BtnClean.Visible = False
                            GdPropProducts.MasterTableView.GetColumn("ExpandParent").Visible = False
                        Case "6"

                            If itemValue <> "" Then

                                BtnRowEdit.Visible = False
                                BtnAddAUE.Visible = True
                                BtnAddAUE.Enabled = True

                            Else
                                item.Display = False  '//hide the row
                                For i As Integer = 0 To item.Controls.Count - 1
                                    BtnAddAUE.Visible = False
                                    TryCast(item.Controls(i), GridTableCell).Enabled = False
                                Next
                            End If
                            BtnAdd.Visible = False
                            BtnClean.Visible = False




                        Case "7"


                            BtnRowEdit.Visible = False
                            BtnAddAUE.Visible = False

                            For i As Integer = 0 To item.Controls.Count - 1

                                TryCast(item.Controls(i), GridTableCell).Enabled = False
                            Next

                            BtnAdd.Visible = False
                            BtnClean.Visible = False
                            GdPropProducts.MasterTableView.GetColumn("ExpandParent").Visible = False
                    End Select






                End If

            End If


        Next





    End Sub

    Protected Sub GdPropProducts_UpdateCommand(sender As Object, e As GridCommandEventArgs) Handles GdPropProducts.UpdateCommand

        Dim item As GridEditableItem = GdPropProducts.EditItems.Item(0)

        If ValidateProduct(item, "Edit") = False Then

            e.Canceled = True

        Else

            Dim strPropId As String = Request.QueryString("PI")

            ' TEXT BOX ' LABELS VARIABLES
            Dim strRowId As String = item.GetDataKeyValue("PropDetailId")

            Dim strProdId As String = CType(item.FindControl("CbProductList"), DropDownList).SelectedValue
            Dim strPropQt As String = CType(item.FindControl("TxtPropQt"), TextBox).Text
            If strPropQt = "" Then strPropQt = 0

            Dim strPropFreeQt As String = CType(item.FindControl("TxtFreeQty"), TextBox).Text
            If strPropFreeQt = "" Then strPropFreeQt = 0

            Dim strPropPkgQty As String = CType(item.FindControl("TxtPackageQty"), TextBox).Text
            If strPropPkgQty = "" Then strPropPkgQty = 0

            Dim strPropDiscount As String = CType(item.FindControl("TxtDiscount"), TextBox).Text
            If strPropDiscount = "" Then strPropDiscount = 0

            Dim strPropDevMonths As String = CType(item.FindControl("TxtDeliveryMonths"), TextBox).Text
            If strPropDevMonths = "" Then strPropDevMonths = 0

            Dim strPropPrice As String = CType(item.FindControl("TxtPropPrice"), TextBox).Text
            If strPropPrice = "" Then strPropPrice = 0

            Dim strPropValidity As String = CType(item.FindControl("TxtPropValidity"), TextBox).Text
            If strPropValidity = "" Then strPropValidity = 0


            Dim sqlUpdate As String = "UPDATE ProposalDetails SET Quantity=" & strPropQt & ", FreeQty=" & strPropFreeQt & ", PackageQty=" & strPropPkgQty & ", Discount=" & strPropDiscount & ", DeliveryMonths=" & strPropDevMonths & ",Price=" & Replace(strPropPrice, ",", ".") & ",Validity=" & strPropValidity & ", ModifiedDate=GETDATE(), ModifiedUserId=" & Getdata.UserId & " WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strRowId & ""

            Getdata.run_sql_query(sqlUpdate)

            Me.DivProdAlert.Visible = False

            srcPropProducts.DataBind()
            GdPropProducts.DataBind()
            GdPropProducts.Rebind()



        End If
    End Sub
    Protected Sub srcPropProducts_Updated(sender As Object, e As SqlDataSourceStatusEventArgs) Handles srcPropProducts.Updated

        srcPropProducts.DataBind()
        GdPropProducts.DataBind()


    End Sub

    Protected Sub GdPropProducts_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles GdPropProducts.ItemCommand

        If e.CommandName = "PerformInsert" And e.Item.OwnerTableView.Name = "parent" Then

            Dim strPropId As String = Request.QueryString("PI")
            Dim strOppId As String = Request.QueryString("OI")
            Dim sqlInsert As String = ""
            Dim i As Integer = 0

            For Each row As GridDataItem In GdPropProducts.Items
                ' TEXT BOX ' LABELS VARIABLES

                Dim strRowId As String = row.GetDataKeyValue("OppDetailId")

                Dim strProdLb As String = CType(row.FindControl("TxtProductNameLabel"), Label).Text

                Dim strProdId As String = CType(row.FindControl("LbProductId"), Label).Text
                Dim strPropProd As String = CType(row.FindControl("LbPropDetailId"), Label).Text


                Dim strPropQt As String = CType(row.FindControl("TxtPropQt"), TextBox).Text
                If strPropQt = "" Then strPropQt = 0


                Dim strPropFreeQt As String = CType(row.FindControl("TxtFreeQty"), TextBox).Text
                If strPropFreeQt = "" Then strPropFreeQt = 0

                Dim strPropPkgQty As String = CType(row.FindControl("TxtPackageQty"), TextBox).Text
                If strPropPkgQty = "" Then strPropPkgQty = 0

                Dim strPropDiscount As String = CType(row.FindControl("TxtDiscount"), TextBox).Text
                If strPropDiscount = "" Then strPropDiscount = 0

                Dim strPropDevMonths As String = CType(row.FindControl("TxtDeliveryMonths"), TextBox).Text
                If strPropDevMonths = "" Then strPropDevMonths = 0

                Dim strPropPrice As String = CType(row.FindControl("TxtPropPrice"), TextBox).Text
                If strPropPrice = "" Then strPropPrice = 0

                Dim strPropValidity As String = CType(row.FindControl("TxtPropValidity"), TextBox).Text
                If strPropValidity = "" Then strPropValidity = 0

                If strPropProd = "" Then
                    If ValidateProduct(row, "Insert") = False Then

                        If i > 0 Then

                            Exit For

                        Else

                            e.Canceled = True
                            sqlInsert = ""
                            Exit For

                        End If



                    Else
                        ' insert command 

                        sqlInsert &= "INSERT INTO ProposalDetails (TenantId, ProposalId, OpportunityDetailId, ProductId, Quantity, FreeQty, PackageQty, Discount, Vat, DeliveryMonths,Price,Validity, CreateDate, CreateUserId, ModifiedDate, ModifiedUserId) VALUES (" & _
                            Getdata.TenantId & "," & strPropId & "," & strRowId & "," & strProdId & "," & strPropQt & "," & strPropFreeQt & "," & strPropPkgQty & "," & strPropDiscount & ",1," & strPropDevMonths & "," & Replace(strPropPrice, ",", ".") & "," & strPropValidity & ",GETDATE()," & Getdata.UserId & ",GETDATE()," & Getdata.UserId & ")" & vbCrLf

                        strProdId = ""
                        strPropQt = ""
                        strRowId = ""
                        strPropFreeQt = ""
                        strPropPkgQty = ""
                        strPropDiscount = ""
                        strPropDevMonths = ""
                        strPropPrice = ""
                        strPropValidity = ""

                        i = i + 1

                    End If

                End If

            Next
            If sqlInsert = "" Then
            Else

                Me.DivProdAlert.Visible = False
                Getdata.run_sql_query(sqlInsert)
                GdPropProducts.Rebind()
            End If

        Else


        End If



        If e.CommandName = "AddNewChild" Then

            Dim parentRow As GridDataItem = TryCast(e.Item, GridDataItem)

            Dim str As GridTableView = DirectCast(e.Item, GridDataItem).ChildItem.NestedTableViews(0)

            str.IsItemInserted = True

            TryCast(e.Item, GridDataItem).Expanded = True

            'Dim nestedView As GridTableView = parentRow.ChildItem.NestedTableViews(0)



        End If

        If e.CommandName = "ShowChild" Then
            Dim parentRow As GridDataItem = TryCast(e.Item, GridDataItem)
            DirectCast(parentRow.FindControl("BtnShowChild"), LinkButton).Visible = False
            DirectCast(parentRow.FindControl("BtnHideChild"), LinkButton).Visible = True

            TryCast(e.Item, GridDataItem).Expanded = True



        End If

        If e.CommandName = "HideChild" Then
            Dim parentRow As GridDataItem = TryCast(e.Item, GridDataItem)
            DirectCast(parentRow.FindControl("BtnShowChild"), LinkButton).Visible = True
            DirectCast(parentRow.FindControl("BtnHideChild"), LinkButton).Visible = False

            TryCast(e.Item, GridDataItem).Expanded = False



        End If

        If e.CommandName = "ShowSecondChild" And e.Item.OwnerTableView.Name = "FirstChild" Then
            Dim parentRow As GridDataItem = TryCast(e.Item, GridDataItem)
            DirectCast(parentRow.FindControl("BtnShowSecondChild"), LinkButton).Visible = False
            DirectCast(parentRow.FindControl("BtnHideSecondChild"), LinkButton).Visible = True

            TryCast(e.Item, GridDataItem).Expanded = True



        End If


        If e.CommandName = "HideSecondChild" And e.Item.OwnerTableView.Name = "FirstChild" Then
            Dim parentRow As GridDataItem = TryCast(e.Item, GridDataItem)
            DirectCast(parentRow.FindControl("BtnShowSecondChild"), LinkButton).Visible = True
            DirectCast(parentRow.FindControl("BtnHideSecondChild"), LinkButton).Visible = False

            TryCast(e.Item, GridDataItem).Expanded = False



        End If


        If e.CommandName = "PerformInsert" And e.Item.OwnerTableView.Name = "FirstChild" Then
            Dim item As GridEditableItem = TryCast(e.Item, GridEditableItem)
            If ValidateRequest(item) = True Then

                Dim parentItem As GridDataItem = DirectCast(item.OwnerTableView.ParentItem, GridDataItem)

                Dim strProposalId As String = Request.QueryString("PI")

                Dim strProductId As String = DirectCast(parentItem.FindControl("LbProductId"), Label).Text


                Dim strMasterId As String = DirectCast(item.FindControl("RequestMasterTextBox"), TextBox).Text

                Dim strSlaveId As String = DirectCast(item.FindControl("RequestSlaveTextBox"), TextBox).Text
                If strSlaveId = "" Then
                    strSlaveId = 0
                Else


                End If
                Dim strDescription As String = DirectCast(item.FindControl("DescriptionTextBox"), TextBox).Text
                Dim strQuantity As String = DirectCast(item.FindControl("QuantityTextBox"), TextBox).Text

                Dim SqlInsert As String = "INSERT INTO OrderRequests (TenantId, ProductId, ProposalId, RequestMaster, RequestSlave, RequestTypeId, Description, Quantity, CreateDate, CreateUserId, ModifiedDate, ModifiedUserId) VALUES (" & _
                            Getdata.TenantId & "," & strProductId & "," & strProposalId & "," & strMasterId & "," & strSlaveId & ",1,'" & strDescription & "'," & strQuantity & ",GETDATE()," & Getdata.UserId & ",GETDATE()," & Getdata.UserId & ")"


                Getdata.run_sql_query(SqlInsert)

                e.Item.OwnerTableView.Rebind()

            Else

                e.Canceled = True

            End If

        End If

        If e.CommandName = "ShowProductInfo" And e.Item.OwnerTableView.Name = "parent" Then

            Dim parentRow As GridDataItem = TryCast(e.Item, GridDataItem)

            DirectCast(parentRow.FindControl("ItemProductPanel"), HtmlControl).Visible = True
            DirectCast(parentRow.FindControl("ItemProductShow"), LinkButton).Visible = False
            DirectCast(parentRow.FindControl("ItemProductHide"), LinkButton).Visible = True


        End If
        If e.CommandName = "HideProductInfo" And e.Item.OwnerTableView.Name = "parent" Then

            Dim parentRow As GridDataItem = TryCast(e.Item, GridDataItem)

            DirectCast(parentRow.FindControl("ItemProductPanel"), HtmlControl).Visible = False
            DirectCast(parentRow.FindControl("ItemProductShow"), LinkButton).Visible = True
            DirectCast(parentRow.FindControl("ItemProductHide"), LinkButton).Visible = False


        End If





    End Sub
    Protected Sub GdPropProducts_InsertCommand(sender As Object, e As GridCommandEventArgs) Handles GdPropProducts.InsertCommand


        e.Canceled = True
        GdPropProducts.Rebind()



    End Sub

    Protected Sub GdPropProducts_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles GdPropProducts.NeedDataSource

        Dim SqlProp As String = "SELECT * FROM VwProposalDetails WHERE (TenantId = " & Getdata.TenantId & ") AND (OppId = " & Request.QueryString("OI") & ") AND (PropId IS NULL OR PropId =" & Request.QueryString("PI") & ")"

        Dim dt As DataTable = Getdata.GetDataTable(SqlProp)

        GdPropProducts.DataSourceID = String.Empty

        GdPropProducts.DataSource = dt
        GdPropProducts.DataBind()



    End Sub




#End Region ' ################################# GRID PRODUCTS RULES AND ACTIONS 

#Region "GRIDTASKS" ' ################################# GRID TASKS RULES AND ACTIONS 
    Protected Sub GdPropTask_InsertCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles GdPropTask.InsertCommand



        e.Canceled = True
        GdPropTask.Rebind()



    End Sub

    Protected Sub BtnAddTask_Click(sender As Object, e As EventArgs)

        GdPropTask.MasterTableView.IsItemInserted = True
        ' Check for the !IsPostBack also for initial page load
        srcProposalTasks.DataBind()
        GdPropTask.DataBind()

    End Sub

    Protected Sub GdPropTask_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles GdPropTask.ItemDataBound

        If TypeOf e.Item Is GridDataItem Then
            Dim dataItem As GridDataItem = CType(e.Item, GridDataItem)

            Dim itemValue As String
            ' LABELS VARIABLES
            Dim LbStatus As Label
            Dim lbStatusTask As String
            Dim strStartDate As Date
            Dim strEndDate As Date
            Dim strNowDate As Date = DateTime.Now.ToString("dd-MM-yyyy")

            Dim strBtnEdit As Button
            Dim strBtnClose As Button
            Dim strBtnInvalid As Button
            Dim strSelectRow As CheckBox


            If e.Item.IsInEditMode = True Then

                If GdPropTask.MasterTableView.IsItemInserted = True Then

                    strSelectRow = CType(dataItem.FindControl("ChSelectTask"), CheckBox)
                    strSelectRow.Visible = False


                Else

                    LbStatus = CType(dataItem.FindControl("LbStatus"), Label)
                    lbStatusTask = DirectCast(dataItem.FindControl("TaskStatusIdLabel"), Label).Text
                    strStartDate = Date.Parse(DirectCast(dataItem.FindControl("TxtEditStartDate"), TextBox).Text)
                    strEndDate = Date.Parse(DirectCast(dataItem.FindControl("TxtEditEndDate"), TextBox).Text)
                    strSelectRow = CType(dataItem.FindControl("ChSelectTask"), CheckBox)
                    strSelectRow.Visible = False

                    If lbStatusTask = 1 Then

                        If strStartDate > strNowDate Then
                            LbStatus.Text = "AGENDADA"
                            LbStatus.CssClass = "label label-default"
                        End If

                        If strNowDate >= strStartDate And strNowDate <= strEndDate Then
                            LbStatus.Text = "EM EXECUÇÃO"
                            LbStatus.CssClass = "label label-primary"
                        End If

                        If strNowDate > strStartDate And strNowDate > strEndDate Then
                            LbStatus.Text = "OVERDUE"
                            LbStatus.CssClass = "label label-warning"
                        End If


                    End If


                    If lbStatusTask = 2 Then

                        LbStatus.Text = "FECHADA"
                        LbStatus.CssClass = "label label-success"



                    End If

                    If lbStatusTask = 3 Then

                        LbStatus.Text = "ANULADA"
                        LbStatus.CssClass = "label label-danger"


                    End If

                    DirectCast(dataItem.FindControl("TxtEditStartDate"), TextBox).Text = strStartDate.ToString("yyyy-MM-dd")
                    DirectCast(dataItem.FindControl("TxtEditEndDate"), TextBox).Text = strEndDate.ToString("yyyy-MM-dd")


                End If
            Else

                LbStatus = CType(dataItem.FindControl("LbStatus"), Label)
                lbStatusTask = DirectCast(dataItem.FindControl("TaskStatusIdLabel"), Label).Text
                strStartDate = Date.Parse(DirectCast(dataItem.FindControl("LbStartDate"), Label).Text)
                strEndDate = Date.Parse(DirectCast(dataItem.FindControl("LbEndDate"), Label).Text)


                strBtnClose = CType(dataItem.FindControl("BtnClose"), Button)
                strBtnInvalid = CType(dataItem.FindControl("BtnInvalid"), Button)
                strBtnEdit = CType(dataItem.FindControl("BtnViewEdit"), Button)
                strSelectRow = CType(dataItem.FindControl("ChSelectTask"), CheckBox)


                If lbStatusTask = 1 Then

                    If strStartDate > strNowDate Then
                        LbStatus.Text = "AGENDADA"
                        LbStatus.CssClass = "label label-default"
                    End If

                    If strNowDate >= strStartDate And strNowDate <= strEndDate Then
                        LbStatus.Text = "EM EXECUÇÃO"
                        LbStatus.CssClass = "label label-primary"
                    End If

                    If strNowDate > strStartDate And strNowDate > strEndDate Then
                        LbStatus.Text = "OVERDUE"
                        LbStatus.CssClass = "label label-warning"
                    End If

                End If

                If lbStatusTask = 2 Then

                    LbStatus.Text = "FECHADA"
                    LbStatus.CssClass = "label label-success"

                    strBtnClose.Visible = False
                    strBtnInvalid.Visible = False
                    strBtnEdit.Visible = False
                    strSelectRow.Visible = False

                End If

                If lbStatusTask = 3 Then

                    LbStatus.Text = "ANULADA"
                    LbStatus.CssClass = "label label-danger"

                    strBtnClose.Visible = False
                    strBtnInvalid.Visible = False
                    strBtnEdit.Visible = False
                    strSelectRow.Visible = False

                End If
            End If
        End If
    End Sub

    Protected Sub GdPropTask_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles GdPropTask.ItemCommand

        If e.CommandName = "PerformInsert" Then

            Dim item As GridEditableItem = DirectCast(e.Item, GridEditableItem)

            If ValidateInsertTask(item) = False Then

                e.Canceled = True

            Else

                Dim strProposalId As String = Request.QueryString("PI")

                Dim strDescription As String = DirectCast(item.FindControl("TxtTaskDescriptionTextBox"), TextBox).Text

                Dim CbTaskList As DropDownList = DirectCast(item.FindControl("CbProposalList"), DropDownList)

                Dim strCbTaskList As String

                If CbTaskList.SelectedValue = "" Then

                    strCbTaskList = ""

                Else

                    strCbTaskList = CbTaskList.SelectedValue


                End If


                Dim strStart As String = DateTime.Parse(DirectCast(item.FindControl("TxtStartDateTextBox"), TextBox).Text)
                strStart = CDate(strStart).ToString("yyyy-MM-dd")

                'CONVERT(datetime, '" & strDate & "', 120)

                Dim strEnd As String = DateTime.Parse(DirectCast(item.FindControl("TxtEndDateTextBox"), TextBox).Text)
                strEnd = CDate(strEnd).ToString("yyyy-MM-dd")


                Dim sqlPropStatus As String = "SELECT StatusId  From Proposals WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strProposalId

                Dim strPropStatusId As String = Getdata.retrieve_sql_field_value(sqlPropStatus, "StatusId")


                Dim SqlInsert As String = "INSERT INTO ProposalTasks (TenantId,ProposalStatusId, ProposalId, Description,TaskListId, TaskStatusId, StartDate, EndDate, CreateDate, CreateUserId, ModifiedDate,ModifiedUserId)" & _
                                            "VALUES (" & Getdata.TenantId & "," & strPropStatusId & "," & strProposalId & ",'" & strDescription & "'," & strCbTaskList & ",1, CONVERT(datetime, '" & strStart & "', 120), CONVERT(datetime, '" & strEnd & "', 120),GETDATE(),1, GETDATE(),1)"


                Getdata.run_sql_query(SqlInsert)

                srcProposalTasks.DataBind()
                GdPropTask.DataBind()
                Me.DivPropTask.Visible = False

            End If




        End If


        If e.CommandName = "CloseTask" Then

            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)

            Dim strRowId As String = item.GetDataKeyValue("Id")

            Dim strDescription As String = DirectCast(item.FindControl("LbDescription"), Label).Text

            Dim strProposalId As String = Request.QueryString("PI")




            ShowAlertPanelWarning("Pretende fechar a tarefa, ", strDescription & "?", DivPropTask, LbTaskMessage, LbTaskDescription, ChkConfirmTask, TxtVariableDate, False, False, "0")

            Dim strCheckConfirm As CheckBox = CType(DivPropTask.FindControl("ChkConfirmTask"), CheckBox)


            If strCheckConfirm.Checked = True Then

                Dim sqlUpdate As String = "UPDATE ProposalTasks SET TaskStatusId=2  WHERE TenantId=" & Getdata.TenantId & "AND Id=" & strRowId & "AND ProposalId=" & strProposalId & ""

                Getdata.run_sql_query(sqlUpdate)

                srcProposalTasks.DataBind()
                GdPropTask.DataBind()
                strCheckConfirm.Checked = False
                Me.DivPropTask.Visible = False
            Else


            End If

        End If

        If e.CommandName = "InvalidTask" Then

            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)

            Dim strRowId As String = item.GetDataKeyValue("Id")

            Dim strDescription As String = DirectCast(item.FindControl("LbDescription"), Label).Text

            Dim strProposalId As String = Request.QueryString("PI")




            ShowAlertPanelWarning("Pretende anular a tarefa, ", strDescription & "?", DivPropTask, LbTaskMessage, LbTaskDescription, ChkConfirmTask, TxtVariableDate, False, False, "0")

            Dim strCheckConfirm As CheckBox = CType(DivPropTask.FindControl("ChkConfirmTask"), CheckBox)


            If strCheckConfirm.Checked = True Then

                Dim sqlUpdate As String = "UPDATE ProposalTasks SET TaskStatusId=3  WHERE TenantId=" & Getdata.TenantId & "AND Id=" & strRowId & "AND ProposalId=" & strProposalId & ""

                Getdata.run_sql_query(sqlUpdate)

                srcProposalTasks.DataBind()
                GdPropTask.DataBind()
                strCheckConfirm.Checked = False
                Me.DivPropTask.Visible = False
            Else


            End If

        End If

        If e.CommandName = "Edit" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            e.Canceled = True
            Dim strid As String = item.GetDataKeyValue("Id").ToString()
            Dim index As Integer = GdPropTask.MasterTableView.FindItemByKeyValue("Id", CType(strid, Integer)).ItemIndex
            GdPropTask.EditIndexes.Add(index)
            GdPropTask.Rebind()

        End If


        If e.CommandName = "ListSearch" Then

            Dim strGrid As RadGrid = GdPropTask
            Dim item As GridEditableItem = DirectCast(e.Item, GridEditableItem)
            DirectCast(item.FindControl("LbInsertMode"), Label).Text = "1"

            TaskSearch(item, strGrid)

        End If

    End Sub

    Protected Sub srcProposalDetails_Inserted(sender As Object, e As SqlDataSourceStatusEventArgs) Handles srcProposalDetails.Inserted

        Dim strOppId As String = Request.QueryString("OI")

        Dim sqlSelect As String = "SELECT TOP 1 * FROM Proposals Where TenantId=" & Getdata.TenantId & "AND OpportunityId=" & strOppId & "AND CreateUserId=" & Getdata.UserId & " ORDER BY 1 DESC"

        Dim strPropId As String = Getdata.retrieve_sql_field_value(sqlSelect, "Id")

        Dim sqlUpdateOpp As String = "UPDATE Opportunities SET IsEditable=0 WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strOppId

        Getdata.run_sql_query(sqlUpdateOpp)


        Response.Redirect("ProposalDetails.aspx?PI=" & strPropId & "&OI=" & strOppId)






    End Sub

    Protected Sub GdPropTask_UpdateCommand(sender As Object, e As GridCommandEventArgs) Handles GdPropTask.UpdateCommand

        Dim item As GridEditableItem = GdPropTask.EditItems.Item(0)

        If ValidateEditTask(item) = False Then

            e.Canceled = True

        Else

            Dim strPropId As String = Request.QueryString("PI")

            ' TEXT BOX ' LABELS VARIABLES
            Dim strRowId As String = item.GetDataKeyValue("Id")

            Dim strTaskDescription As String = CType(item.FindControl("TxtEditTaskDescription"), TextBox).Text

            Dim strTaskStartDate As String = CType(item.FindControl("TxtEditStartDate"), TextBox).Text
            strTaskStartDate = CDate(strTaskStartDate).ToString("yyyy-MM-dd")

            Dim strTaskEndDate As String = CType(item.FindControl("TxtEditEndDate"), TextBox).Text
            strTaskEndDate = CDate(strTaskEndDate).ToString("yyyy-MM-dd")



            Dim sqlUpdate As String = "UPDATE ProposalTasks SET Description='" & strTaskDescription & "', StartDate=CONVERT(datetime, '" & strTaskStartDate & "', 120), EndDate=CONVERT(datetime, '" & strTaskEndDate & "', 120), ModifiedDate=GETDATE(), ModifiedUserId=" & Getdata.UserId & " WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strRowId & " AND ProposalId=" & strPropId & ""


            Getdata.run_sql_query(sqlUpdate)

            Me.DivPropTask.Visible = False

            srcProposalTasks.DataBind()
            GdPropTask.DataBind()
            GdPropTask.Rebind()



        End If
    End Sub

    Protected Sub GdPropTask_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles GdPropTask.NeedDataSource

        Dim strPropId As String = Request.QueryString("PI")

        Dim sqlSelect As String = "SELECT * FROM [VwProposalTasks] WHERE (([TenantId] = " & Getdata.TenantId & ") AND ([ProposalId] = " & strPropId & "))"

        Dim dt As DataTable = Getdata.GetDataTable(sqlSelect)

        GdPropTask.DataSourceID = String.Empty

        GdPropTask.DataSource = dt
        'GdPropTask.DataBind()


    End Sub

    Protected Sub GdPropTask_DataBound(sender As Object, e As EventArgs) Handles GdPropTask.DataBound

        Dim stredititems As String = GdPropTask.EditItems.Count

        If stredititems > 0 Then
            For Each item As GridDataItem In GdPropTask.MasterTableView.Items

                If item.IsInEditMode = True Then

                    item.Edit = True

                Else


                End If
            Next

        End If


    End Sub

    Protected Sub GdPropTask_ItemCreated(sender As Object, e As GridItemEventArgs) Handles GdPropTask.ItemCreated
        If (TypeOf e.Item Is GridCommandItem) Then
            Dim commandItem As GridCommandItem = CType(e.Item, GridCommandItem)

            If (TypeOf commandItem.NamingContainer Is GridTHead) Then

                If RequestStatus() = 2 Or RequestStatus() = 7 Or RequestStatus() = 4 Then


                    commandItem.FindControl("DivNewTask").Visible = False


                End If




            End If

        End If
    End Sub

#End Region ' ################################# GRID TASKS RULES AND ACTIONS 



    Protected Sub GridView2_RowDataBound(sender As Object, e As GridViewRowEventArgs)

        'If e.Row.RowType = DataControlRowType.DataRow Then

        '    Dim customerId As String = e.Row.
        '    Dim gvOrders As GridView = TryCast(e.Row.FindControl("GridView3"), GridView)

        '    Dim strdt As String = "SELECT * FROM Invoices WHERE TenantId = " & Getdata.TenantId & " AND ProposalId = " & Request.QueryString("PI") & " AND OrderRequestId =" & customerId

        '    Dim dt As DataTable = Getdata.retrive_tablelist(strdt)


        '    gvOrders.DataSource = dt
        '    gvOrders.DataBind()
        'End If


        

    End Sub

    Protected Sub GdEditProducts_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GdEditProducts.RowDataBound


        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim strRowId As String = GdEditProducts.DataKeys(e.Row.RowIndex).Values("PropDetailId").ToString()

            Dim PropCheck As String = DirectCast(e.Row.FindControl("LbOppPropDetailId"), Label).Text


            '####### LABELS Variables
            Dim strLbQuantity As Label = DirectCast(e.Row.FindControl("LbPropQuantity"), Label)
            Dim strLbPrice As Label = DirectCast(e.Row.FindControl("LbPropPrice"), Label)
            Dim strLbValidity As Label = DirectCast(e.Row.FindControl("LbPropValidity"), Label)

            '####### TEXTBOX Variables
            Dim strTxtQuantity As TextBox = DirectCast(e.Row.FindControl("TxtPropQuantity"), TextBox)
            Dim strTxtPrice As TextBox = DirectCast(e.Row.FindControl("TxtPropPrice"), TextBox)
            Dim strTxtValidity As TextBox = DirectCast(e.Row.FindControl("TxtPropValidity"), TextBox)

            ' ####### BUTTONS VARIABLES 

            Dim strBtnEdit As LinkButton = DirectCast(e.Row.FindControl("BtnEdit"), LinkButton)


            If PropCheck = "" Then

                'HIDE LABELS 

                strLbQuantity.Visible = False
                strLbPrice.Visible = False
                strLbValidity.Visible = False

                ' SHOW TEXTBOX

                strTxtQuantity.Visible = True
                strTxtPrice.Visible = True
                strTxtValidity.Visible = True

                ' HIDE BUTTONS
                strBtnEdit.Visible = False

            Else

                If e.Row.RowState = DataControlRowState.Edit Then

                Else


                    'SHOW LABELS 

                    strLbQuantity.Visible = True
                    strLbPrice.Visible = True
                    strLbValidity.Visible = True


                    ' HIDE TEXTBOX

                    strTxtQuantity.Visible = False
                    strTxtPrice.Visible = False
                    strTxtValidity.Visible = False

                    'SHOW BUTTONS
                    strBtnEdit.Visible = True

                    ' FALTA FAZER A COMPONENTE DE MOSTRAR A LABEL DO STATUS CONTRATO OU NAO CONTRATO!
                    Dim LbStatusRow As Label = DirectCast(e.Row.FindControl("LbDetailStatusDesc"), Label)
                    Dim CbStatusRow As DropDownList = DirectCast(e.Row.FindControl("CbDetailStatus"), DropDownList)


                    Dim strRowStatusId As String = Getdata.retrieve_sql_field_value("SELECT StatusId from ProposalDetails WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strRowId & "", "StatusId")


                    If LbStatusRow.Text = "" Then

                        CbStatusRow.Visible = True
                        LbStatusRow.Visible = False


                    Else


                        LbStatusRow.Visible = True
                        CbStatusRow.Visible = False


                    End If


                End If

            End If
        End If

    End Sub

    Protected Sub GdEditProducts_DataBound(sender As Object, e As EventArgs) Handles GdEditProducts.DataBound

        Dim strStatusId As String = RequestStatus()

        Dim strEditRowsCount As String = ""

        Dim sqlCountRowStatus As String = "SELECT COUNT(Id) as Total From ProposalDetails WHERE TenantId=" & Getdata.TenantId & " AND StatusId IS NULL AND ProposalId=" & Request.QueryString("PI")

        Dim strRowStatusCount As String = Getdata.retrieve_sql_field_value(sqlCountRowStatus, "Total")

        Dim sqlCountRowsAdd As String = "SELECT COUNT(OppDetailId) as Total From VwProposalDetails WHERE TenantId=" & Getdata.TenantId & " AND PropDetailId IS NULL AND PropId=" & Request.QueryString("PI")

        Dim strCountRowsAdd As String = Getdata.retrieve_sql_field_value(sqlCountRowsAdd, "Total")

        For Each row In GdEditProducts.Rows

            If row.RowState = DataControlRowState.Edit Then
                strEditRowsCount = "1"

            End If
        Next

        For Each row In GdEditProducts.Rows

            Dim strCollumButtons As Object = GdEditProducts.Columns(9)
            Dim strCollumStatus As Object = GdEditProducts.Columns(8)


            Dim PropCheck As String = CType(row.FindControl("LbOppPropDetailId"), Label).Text
            'BUTTONS Variables 
            Dim BtnAdd As LinkButton = CType(GdEditProducts.FooterRow.FindControl("BtnSave"), LinkButton)
            Dim BtnClean As LinkButton = CType(GdEditProducts.FooterRow.FindControl("BtnClean"), LinkButton)
            Dim BtnEdit As LinkButton = CType(row.FindControl("BtnEdit"), LinkButton)

            Dim BtnRowSave As LinkButton = CType(GdEditProducts.FooterRow.FindControl("BtnBulkRowSave"), LinkButton)
            Dim BtnRowCancel As LinkButton = CType(GdEditProducts.FooterRow.FindControl("BtnCancelRow"), LinkButton)


            If strEditRowsCount = "1" Then
                BtnAdd.Visible = False
                BtnClean.Visible = False


                If row.RowState = DataControlRowState.Edit Then



                Else

                    For i As Integer = 0 To row.Controls.Count - 1

                        TryCast(row.Controls(i), DataControlFieldCell).Enabled = False
                    Next



                End If


            Else

                Select Case strStatusId


                    Case "1"

                        BtnRowSave.Visible = False
                        BtnRowCancel.Visible = False
                        strCollumStatus.Visible = False

                        If strCountRowsAdd = 0 Then

                            BtnAdd.Visible = False
                            BtnClean.Visible = False


                        End If


                    Case "2"

                        If PropCheck = "" Then
                            row.visible = False  '//hide the row
                        End If

                        BtnAdd.Visible = False
                        BtnClean.Visible = False
                        BtnEdit.Visible = False

                        BtnRowSave.Visible = False
                        BtnRowCancel.Visible = False


                        For i As Integer = 0 To row.Controls.Count - 1

                            TryCast(row.Controls(i), DataControlFieldCell).Enabled = False
                        Next

                        strCollumStatus.Visible = False




                    Case "3"
                        If PropCheck = "" Then
                            row.visible = False  '//hide the row
                        End If

                        BtnAdd.Visible = False
                        BtnClean.Visible = False
                        BtnEdit.Visible = False

                        BtnRowSave.Visible = False
                        BtnRowCancel.Visible = False

                        For i As Integer = 0 To row.Controls.Count - 1

                            TryCast(row.Controls(i), DataControlFieldCell).Enabled = False
                        Next

                        strCollumStatus.Visible = False



                    Case "4"

                        If PropCheck = "" Then
                            row.visible = False  '//hide the row
                        End If

                        BtnAdd.Visible = False
                        BtnClean.Visible = False

                        For i As Integer = 0 To row.Controls.Count - 1

                            TryCast(row.Controls(i), DataControlFieldCell).Enabled = False
                        Next

                        BtnRowSave.Visible = False
                        BtnRowCancel.Visible = False

                        strCollumStatus.Visible = False



                    Case "5"

                        If PropCheck = "" Then
                            row.visible = False  '//hide the row
                        End If

                        BtnAdd.Visible = False
                        BtnClean.Visible = False
                        BtnEdit.Visible = False

                        'For i As Integer = 0 To row.Controls.Count - 1

                        '    TryCast(row.Controls(i), DataControlFieldCell).Enabled = False
                        'Next

                        BtnRowSave.Visible = True
                        BtnRowCancel.Visible = True

                        strCollumButtons.Visible = False

                        strCollumStatus.Visible = True

                        If strRowStatusCount = 0 Then

                            BtnRowSave.Visible = False
                            BtnRowCancel.Visible = False
                        
                        Else
                            

                            BtnRowSave.Visible = True
                            BtnRowCancel.Visible = True

                        End If


                    Case "6"

                        If PropCheck = "" Then
                            row.visible = False  '//hide the row
                        End If

                        BtnAdd.Visible = False
                        BtnClean.Visible = False
                        BtnEdit.Visible = False

                        For i As Integer = 0 To row.Controls.Count - 1

                            TryCast(row.Controls(i), DataControlFieldCell).Enabled = False
                        Next

                        BtnRowSave.Visible = False
                        BtnRowCancel.Visible = False

                        strCollumStatus.Visible = True



                    Case "7"

                        If PropCheck = "" Then
                            row.visible = False  '//hide the row
                        End If

                        BtnAdd.Visible = False
                        BtnClean.Visible = False
                        BtnEdit.Visible = False

                        For i As Integer = 0 To row.Controls.Count - 1

                            TryCast(row.Controls(i), DataControlFieldCell).Enabled = False
                        Next

                        BtnRowSave.Visible = False
                        BtnRowCancel.Visible = False

                        strCollumStatus.Visible = False



                End Select

            End If

        Next


    End Sub

    Protected Sub GdEditProducts_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GdEditProducts.RowCommand


        If e.CommandName = "Cancel" Then

            Me.DivProdAlert.Visible = False

        End If

        If e.CommandName = "BulkSave" Then


            Dim strPropId As String = Request.QueryString("PI")
            Dim strOppId As String = Request.QueryString("OI")
            Dim sqlInsert As String = ""
            Dim i As Integer = 0
            Dim iQuantity As Integer = 0

            For Each row In GdEditProducts.Rows
                ' TEXT BOX ' LABELS VARIABLES


                Dim strRowId As String = GdEditProducts.DataKeys(row.RowIndex).Value.ToString()

                Dim strProdId As String = CType(row.FindControl("LbProductId"), Label).Text
                Dim strPropDetailId As String = CType(row.FindControl("LbOppPropDetailId"), Label).Text


                Dim strPropQt As String = CType(row.FindControl("TxtPropQuantity"), TextBox).Text

                'Dim strPropFreeQt As String = CType(row.FindControl("TxtFreeQty"), TextBox).Text
                'If strPropFreeQt = "" Then strPropFreeQt = 0

                'Dim strPropPkgQty As String = CType(row.FindControl("TxtPackageQty"), TextBox).Text
                'If strPropPkgQty = "" Then strPropPkgQty = 0

                'Dim strPropDiscount As String = CType(row.FindControl("TxtDiscount"), TextBox).Text
                'If strPropDiscount = "" Then strPropDiscount = 0

                'Dim strPropDevMonths As String = CType(row.FindControl("TxtDeliveryMonths"), TextBox).Text
                'If strPropDevMonths = "" Then strPropDevMonths = 0

                Dim strPropPrice As String = CType(row.FindControl("TxtPropPrice"), TextBox).Text

                Dim strPropValidity As String = CType(row.FindControl("TxtPropValidity"), TextBox).Text


                If strPropDetailId = "" Then

                    If Len(CType(row.FindControl("TxtPropQuantity"), TextBox).Text) > 0 Then

                        If ValidateProduct(row, "Insert") = False Then

                            CType(row.FindControl("RowImgProblem"), HtmlControl).Visible = True

                            CType(row.FindControl("RowImgOk"), HtmlControl).Visible = False

                            If i > 0 Then

                                sqlInsert = ""
                                Exit For

                            Else

                                sqlInsert = ""
                                Exit For

                            End If



                        Else
                            ' insert command 
                            If strPropQt = "" Then strPropQt = 0
                            If strPropPrice = "" Then strPropPrice = 0
                            If strPropValidity = "" Then strPropValidity = 0

                            sqlInsert &= "INSERT INTO ProposalDetails (TenantId, ProposalId, OpportunityDetailId, ProductId, Quantity, FreeQty, PackageQty, Discount, Vat, DeliveryMonths,Price,Validity, CreateDate, CreateUserId, ModifiedDate, ModifiedUserId) VALUES (" & _
                                Getdata.TenantId & "," & strPropId & "," & strRowId & "," & strProdId & "," & strPropQt & ",0,0,0,1,0," & Replace(strPropPrice, ",", ".") & "," & strPropValidity & ",GETDATE()," & Getdata.UserId & ",GETDATE()," & Getdata.UserId & ")" & vbCrLf

                            strProdId = ""
                            strPropQt = ""
                            strRowId = ""
                            'strPropFreeQt = ""
                            'strPropPkgQty = ""
                            'strPropDiscount = ""
                            'strPropDevMonths = ""
                            strPropPrice = ""
                            strPropValidity = ""

                            i = i + 1

                            CType(row.FindControl("RowImgOk"), HtmlControl).Visible = True
                            CType(row.FindControl("RowImgProblem"), HtmlControl).Visible = False
                            iQuantity = 0

                        End If
                    Else

                        iQuantity = iQuantity + 1


                    End If


                End If

            Next


            If sqlInsert = "" Then

                If iQuantity = 0 Then

                Else

                    ShowAlertPanelError("Quantidade não introduzida!", DivProdAlert, LbProdAlert)

                End If

            Else

                Me.DivProdAlert.Visible = False
                Getdata.run_sql_query(sqlInsert)
                GdEditProducts.DataBind()

            End If
        End If

        If e.CommandName = "BulkStatusSave" Then

            'Dim strPropId As String = Request.QueryString("PI")
            'Dim strOppId As String = Request.QueryString("OI")
            Dim sqlUpdate As String = ""

            Dim i As Integer = 0
            Dim strCountRows As Integer = GdEditProducts.Rows.Count()

            For Each row In GdEditProducts.Rows

                Dim strCbRowStatus As DropDownList = DirectCast(row.Findcontrol("CbDetailStatus"), DropDownList)
                'Dim strTxtRefusalPrice As TextBox = DirectCast(row.Findcontrol("TxtRefusalPrice"), TextBox)
                'Dim strTxtPrice As String = strTxtRefusalPrice.Text

                Dim strRowId As String = GdEditProducts.DataKeys(row.RowIndex).Values("PropDetailId").ToString()

                If strCbRowStatus.SelectedValue = 0 Then


                Else

                    If strCbRowStatus.SelectedValue = 2 Then

                        sqlUpdate &= "UPDATE ProposalDetails SET StatusId = 2, StatusDate=GETDATE(), StatusUserId=" & Getdata.UserId & " WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strRowId & "" & vbCrLf

                        i = i + 1

                    End If

                    If strCbRowStatus.SelectedValue = 1 Then

                        sqlUpdate &= "UPDATE ProposalDetails SET StatusId = 1, StatusDate=GETDATE(), StatusUserId=" & Getdata.UserId & " WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strRowId & "" & vbCrLf


                        i = i + 1

                    End If


                End If


            Next

            If sqlUpdate = "" Then


            Else



                If strCountRows = i Then
                    

                    Getdata.run_sql_query(sqlUpdate)
                    GdEditProducts.DataBind()


                    If ChangeStatus(9) = True Then



                    End If


                    
                Else

                    Getdata.run_sql_query(sqlUpdate)
                    GdEditProducts.DataBind()


                End If


            End If




        End If


    End Sub

    Protected Sub BtnAddRowStatus_Click(sender As Object, e As EventArgs) ' ROW EDIT STATUS!

        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent,  _
                                        GridViewRow)
        Dim sqlUpdate As String = ""


        Dim strPropDetailId As String = GdEditProducts.DataKeys(gvRow.RowIndex).Values("PropDetailId").ToString()
        Dim strCbStatus As DropDownList = DirectCast(gvRow.FindControl("CbDetailStatus"), DropDownList)

        If strCbStatus.SelectedValue = 0 Then

        Else


            sqlUpdate = "UPDATE ProposalDetails SET Status=" & strCbStatus.SelectedValue & " WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strPropDetailId


            Getdata.run_sql_query(sqlUpdate)
            GdEditProducts.DataBind()


        End If

    End Sub
    Protected Sub GdEditProducts_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles GdEditProducts.RowUpdating

        Dim index As Integer = GdEditProducts.EditIndex
        Dim row As GridViewRow = GdEditProducts.Rows(index)



        If ValidateProduct(row, "Edit") = False Then

            'e.Canceled = True


        Else
            Dim strRowId As String = GdEditProducts.DataKeys(row.RowIndex).Values("PropDetailId").ToString()

            Dim strEditPropQt As String = CType(row.FindControl("TxtEditPropQuantity"), TextBox).Text
            Dim strEditPropPrice As String = CType(row.FindControl("TxtEditPropPrice"), TextBox).Text


            Dim strEditPropValidity As String = CType(row.FindControl("TxtEditPropValidity"), TextBox).Text


            Dim sqlUpdate As String = "UPDATE ProposalDetails SET Quantity=" & strEditPropQt & ",Price=" & Replace(strEditPropPrice, ",", ".") & ",Validity=" & strEditPropValidity & ", ModifiedDate=GETDATE(), ModifiedUserId=" & Getdata.UserId & " WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strRowId & ""

            Getdata.run_sql_query(sqlUpdate)

            Me.DivProdAlert.Visible = False


            row.RowState = DataControlRowState.Normal

            e.Cancel = True
            GdEditProducts.EditIndex = -1
            If GdEditProducts.Rows(e.RowIndex).RowState = DataControlRowState.Edit Then
                GdEditProducts.Rows(e.RowIndex).RowState = DataControlRowState.Normal
            End If
            GdEditProducts.DataBind()



        End If





    End Sub

    Private Function ContractValuesOut(strRow As Object, strRowStatus As String)

        Dim strProductId As String = GdProdContract.DataKeys(strRow.RowIndex).Values("TxtOppProductId").ToString()


        '############### LABELS AND BLOCKS
        Dim strTotalRequest As Label = CType(strRow.FindControl("LbTotalRequests"), Label) ' total OrderRequests AUE
        Dim strTotalInvoices As Label = CType(strRow.FindControl("LbTotalOut"), Label) ' Total Invoices in DB - Saidas 
        Dim strTotalOutProgress As Label = CType(strRow.FindControl("LbTotalOutRequests"), Label) ' Total Out Vs OrderRequests Progress Bar Label 
        Dim strTotalStock As Label = CType(strRow.FindControl("LbStockRequests"), Label) ' total in Stock of OrderRequests

        Dim strProgressBar1 As HtmlControl = CType(strRow.FindControl("ProgressBar1"), HtmlControl)
        Dim strProgress1 As HtmlControl = CType(strRow.FindControl("Progress1"), HtmlControl)

        Dim strRequestBlock As HtmlControl = CType(strRow.FindControl("RequestBlock"), HtmlControl)
        Dim strBlockStock As HtmlControl = CType(strRow.FindControl("BlockStock"), HtmlControl)
        Dim strInvoiceBlock As HtmlControl = CType(strRow.FindControl("InvoiceBlock"), HtmlControl)




        Select Case strRowStatus

            Case 1
                '############### DATATABLES 


                Dim sqlRequestSelect As String = "SELECT * FROM OrderRequests WHERE TenantId=" & Getdata.TenantId & "AND ProposalId=" & Request.QueryString("PI") & "AND ProductId=" & strProductId

                Dim DtRequests As DataTable = Getdata.GetDataTable(sqlRequestSelect)

                Dim sqlInvoicesSelect As String = "SELECT * FROM Invoices WHERE TenantId=" & Getdata.TenantId & "AND ProposalId=" & Request.QueryString("PI") & "AND ProductId=" & strProductId

                Dim DtInvoices As DataTable = Getdata.GetDataTable(sqlInvoicesSelect)


                '############### RULES




                If DtRequests.Rows.Count = 0 Then
                    ' Doesn't exist any orderRequest so we do not have value to compare with the Invoices
                    strRequestBlock.Visible = False
                    strBlockStock.Visible = False
                    strProgressBar1.Visible = False
                    strProgress1.Visible = False


                    If DtInvoices.Rows.Count = 0 Then
                        strInvoiceBlock.Visible = False

                    Else

                        strInvoiceBlock.Visible = True
                        strBlockStock.Visible = False

                        Dim TotalInvoice As String = DtInvoices.Compute("Sum(InvoiceQuantity)", String.Empty).ToString()
                        If TotalInvoice = "" Then
                            TotalInvoice = "0"
                        End If

                        strTotalInvoices.Text = TotalInvoice


                    End If

                Else

                    Dim TotalRequest As String = DtRequests.Compute("Sum(Quantity)", String.Empty).ToString
                    strRequestBlock.Visible = True

                    strTotalRequest.Text = TotalRequest



                    If DtInvoices.Rows.Count = 0 Then
                        strInvoiceBlock.Visible = False
                        strBlockStock.Visible = False
                        strProgressBar1.Visible = False
                        strProgress1.Visible = False

                    Else

                        strInvoiceBlock.Visible = True
                        strBlockStock.Visible = True


                        If TotalRequest = "" Then
                            TotalRequest = 1
                        End If

                        Dim TotalInvoice As String = DtInvoices.Compute("Sum(InvoiceQuantity)", String.Empty).ToString()
                        If TotalInvoice = "" Then
                            TotalInvoice = 1
                        End If


                        strTotalStock.Text = TotalRequest - TotalInvoice
                        strTotalRequest.Text = TotalRequest
                        strTotalInvoices.Text = TotalInvoice
                        strTotalOutProgress.Text = TotalInvoice

                        strProgressBar1.Visible = True
                        strProgressBar1.Attributes("aria-valuenow") = TotalInvoice
                        Dim strprogresscalc As String = Replace(FormatPercent((TotalInvoice / TotalRequest)), ",", ".")

                        strProgressBar1.Attributes("style") = "width: " & strprogresscalc & ";"

                        strProgressBar1.Attributes("aria-valuemax") = TotalRequest




                    End If



                 


                End If

                DtRequests = Nothing
                DtInvoices = Nothing

            Case 2
                Dim strDivProdOut As HtmlControl = CType(strRow.Findcontrol("DivProdOut"), HtmlControl)


                strDivProdOut.Visible = False






        End Select


   



    End Function

    Private Function ContractValues(strRow As Object, strRowStatus As String)

        Dim strProductId As String = GdProdContract.DataKeys(strRow.RowIndex).Values("TxtOppProductId").ToString()
        Dim strProductDetailId As String = GdProdContract.DataKeys(strRow.RowIndex).Values("PropDetailId").ToString()

        Dim strLbOppTotal As Label = CType(strRow.FindControl("LbOppTotal"), Label) ' Total of Oportunitity (QtOpp * ProductPriceSAP) (€)
        Dim strLbPropTotal As Label = CType(strRow.FindControl("LbPropTotal"), Label) ' Total of Proposal (QtProp * ProductPriceProp) (€)

        Dim strLbOutTotal As Label = CType(strRow.FindControl("LbPropOutTotal"), Label) ' Total of Invoices (sum Invoices) 

        Dim strOppVsProp As Label = CType(strRow.FindControl("LbOppVsProp"), Label) ' Margem 
        Dim strPropVsOpp As Label = CType(strRow.FindControl("LbPropVsOut"), Label) ' Diferença 


        Dim strProgressBar2 As HtmlControl = CType(strRow.FindControl("ProgressBar2"), HtmlControl)
        Dim strProgress2 As HtmlControl = CType(strRow.FindControl("Progress2"), HtmlControl)


        Select Case strRowStatus


            Case 1

                Dim SqlLineTotals As String = "SELECT * FROM VwProposalDetails WHERE TenantId=" & Getdata.TenantId & " AND PropId=" & Request.QueryString("PI") & " AND ProductId=" & strProductId

                Dim strTotalOpp As Decimal = Getdata.retrieve_sql_field_value(SqlLineTotals, "TxtTotalOpp")
                If strTotalOpp = 0 Then
                    strTotalOpp = 1
                End If
                Dim strTotalProp As Decimal = Getdata.retrieve_sql_field_value(SqlLineTotals, "TxtTotalProp")


                Dim sqlInvoicesSelect As String = "SELECT * FROM Invoices WHERE TenantId=" & Getdata.TenantId & " AND ProposalId=" & Request.QueryString("PI") & " AND ProductId=" & strProductId

                Dim DtInvoices As DataTable = Getdata.GetDataTable(sqlInvoicesSelect)

                If DtInvoices.Rows.Count = 0 Then


                    strLbOppTotal.Text = strTotalOpp
                    strLbPropTotal.Text = strTotalProp
                    strOppVsProp.Text = Replace(FormatPercent((strTotalOpp - strTotalProp) / strTotalOpp), ",", ".")

                    strPropVsOpp.Text = strTotalOpp - strTotalProp

                    strLbOutTotal.Text = 0

                    strProgressBar2.Visible = False
                    strProgress2.Visible = False


                Else

                    Dim TotalInvoice As Decimal = DtInvoices.Compute("Sum(InvoiceTotal)", String.Empty).ToString

                    strLbOppTotal.Text = strTotalOpp
                    strLbPropTotal.Text = strTotalProp


                    strProgressBar2.Visible = True
                    strProgress2.Visible = True
                    strProgressBar2.Attributes("aria-valuemax") = strTotalProp
                    strProgressBar2.Attributes("aria-valuenow") = TotalInvoice
                    Dim strprogresscalc As String = Replace(FormatPercent((TotalInvoice / strTotalProp)), ",", ".")

                    strProgressBar2.Attributes("style") = "width: " & strprogresscalc & ";"


                    strLbOutTotal.Text = TotalInvoice
                    strOppVsProp.Text = Replace(FormatPercent((strTotalOpp - strTotalProp) / strTotalOpp), ",", ".")
                    strPropVsOpp.Text = strTotalOpp - strTotalProp


                End If



            Case 2

                Dim strDivProdValues As HtmlControl = CType(strRow.Findcontrol("DivProdValues"), HtmlControl)
                strDivProdValues.Visible = False

        End Select




      









    End Function

    Private Function RefusalRow(strRow As Object, strRowStatus As String)

        Dim strProductId As String = GdProdContract.DataKeys(strRow.RowIndex).Values("TxtOppProductId").ToString()
        Dim strProductDetailId As String = GdProdContract.DataKeys(strRow.RowIndex).Values("PropDetailId").ToString()

        Dim strDivRefusal As HtmlControl = CType(strRow.FindControl("DivRefusalInfo"), HtmlControl)

        Dim strLbRefusalPrice As Label = CType(strRow.Findcontrol("LbRefusalPrice"), Label)
        Dim strTxtRefusalPrice As TextBox = CType(strRow.Findcontrol("TxtRefusalPrice"), TextBox)
        Dim strBtnSavePrice As LinkButton = CType(strRow.Findcontrol("BtnRefusalSave"), LinkButton)
        Dim strBtnEditPrice As LinkButton = CType(strRow.Findcontrol("BtnEditPrice"), LinkButton)

        Dim strBtnAddRequest As LinkButton = CType(strRow.Findcontrol("BtnNewRequest"), LinkButton)

        Select Case strRowStatus

            Case 1 ' CONTRATO
                strDivRefusal.Visible = False
                strBtnAddRequest.Visible = True


            Case 2 ' RECUSADO 
                strDivRefusal.Visible = True
                strBtnAddRequest.Visible = False


                If strLbRefusalPrice.Text = "" Then

                    strTxtRefusalPrice.Visible = True
                    strBtnSavePrice.Visible = True
                    strLbRefusalPrice.Visible = False

                Else
                    strTxtRefusalPrice.Visible = False
                    strBtnSavePrice.Visible = False


                End If





        End Select




    End Function

    Private Sub RowStatus(strRow As Object, strRowStatus As String)

        Dim strProductDetailId As String = GdProdContract.DataKeys(strRow.RowIndex).Values("PropDetailId").ToString()

        Dim sqlProposalSelect As String = "SELECT * FROM ProposalDetails WHERE TenantId=" & Getdata.TenantId & "AND Id=" & strProductDetailId

        Dim DtProposal As DataTable = Getdata.GetDataTable(sqlProposalSelect)

        For Each row As DataRow In DtProposal.Rows

            Dim strStatus As String = row("StatusId").ToString()

            If strStatus = "" Then

                CType(strRow.FindControl("CbDetailStatus"), DropDownList).Visible = True
                'CType(strRow.FindControl("BtnAddRowStatus"), LinkButton).Visible = True

            Else

                CType(strRow.FindControl("CbDetailStatus"), DropDownList).Visible = False
                'CType(strRow.FindControl("BtnAddRowStatus"), LinkButton).Visible = False

            End If






        Next






    End Sub

    Protected Sub GdProdContract_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GdProdContract.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim ProductId As String = GdProdContract.DataKeys(e.Row.RowIndex).Value.ToString()
            Dim gvOrders As GridView = TryCast(e.Row.FindControl("GdProdRequests"), GridView)

            Dim strPropRowId As String = GdProdContract.DataKeys(e.Row.RowIndex).Values("PropDetailId").ToString()


            Dim strdt As String = "SELECT * FROM VwProductDetails WHERE TenantId = " & Getdata.TenantId & " AND ProposalId = " & Request.QueryString("PI") & " AND ProductId = " & ProductId & "Order By sort_line"

            Dim dt As DataTable = Getdata.retrive_tablelist(strdt)

            gvOrders.DataSource = dt

            gvOrders.DataBind()

            Dim SqlRowStatus = "SELECT * FROM VwProposalDetails WHERE TenantId=" & Getdata.TenantId & " AND PropDetailId= " & strPropRowId

            Dim strRowStatus As String = Getdata.retrieve_sql_field_value(SqlRowStatus, "PropDetailStatusId")


            ContractValuesOut(e.Row, strRowStatus)

            'ContractValues(e.Row, strRowStatus)

            RefusalRow(e.Row, strRowStatus)



        End If




    End Sub

    Protected Sub srcProposalDetails_Inserting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles srcProposalDetails.Inserting


        Dim strOppId As String = Request.QueryString("OI")

        Dim sqlOpp As String = "SELECT * FROM Opportunities WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strOppId

        Dim strCustomerId As String = Getdata.retrieve_sql_field_value(sqlOpp, "CustomerId")
        Dim strCustomerGroupId As String = Getdata.retrieve_sql_field_value(sqlOpp, "CustomerGroupId")


        If strCustomerId = "" And strCustomerGroupId <> "?" Then


            e.Command.Parameters("@CustomerGroupId").Value = strCustomerGroupId

        Else


            e.Command.Parameters("@CustomerId").Value = strCustomerId



        End If


    End Sub

    Protected Sub GdProdContract_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GdProdContract.RowCommand

        If e.CommandName = "RowSave" Then

            Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)

            Dim strRowId As String = GdProdContract.DataKeys(row.RowIndex).Values("PropDetailId").ToString()

            Dim str As TextBox = DirectCast(row.FindControl("TxtRefusalPrice"), TextBox)


            Dim a As String = str.Text

            Dim sqlUpdate As String = "UPDATE ProposalDetails SET RefusalPrice=" & Replace(str.Text, ",", ".") & " WHERE TenantId=" & Getdata.TenantId & " AND Id=" & strRowId


            Getdata.run_sql_query(sqlUpdate)
            GdProdContract.DataBind()



        End If





    End Sub

    Protected Sub BtnEditPrice_Click(sender As Object, e As EventArgs)


        Dim row As GridViewRow = DirectCast(DirectCast(sender, LinkButton).NamingContainer, GridViewRow)



        Dim strTxtPrice As TextBox = DirectCast(row.FindControl("TxtRefusalPrice"), TextBox)
        Dim strLbPrice As Label = DirectCast(row.FindControl("LbRefusalPrice"), Label)
        Dim strBtnEdit As LinkButton = DirectCast(row.FindControl("BtnEditPrice"), LinkButton)
        Dim strBtnSave As LinkButton = DirectCast(row.FindControl("BtnRefusalSave"), LinkButton)

        strTxtPrice.Visible = True
        strBtnSave.Visible = True

        strBtnEdit.Visible = False
        strLbPrice.Visible = False


    End Sub

    Protected Sub BtnNewRequest_Click(sender As Object, e As EventArgs)

        Dim row As GridViewRow = DirectCast(DirectCast(sender, LinkButton).NamingContainer, GridViewRow)
        Dim gvOrders As GridView = TryCast(row.FindControl("GdProdRequests"), GridView)

        Dim ProductId As String = GdProdContract.DataKeys(row.RowIndex).Value.ToString()


        Dim strPropRowId As String = GdProdContract.DataKeys(row.RowIndex).Values("PropDetailId").ToString()

        Dim strProductId As String = GdProdContract.DataKeys(row.RowIndex).Values("TxtOppProductId").ToString()


        Dim strdt As String = "SELECT * FROM VwProductDetails WHERE TenantId = " & Getdata.TenantId & " AND ProposalId = " & Request.QueryString("PI") & " AND ProductId = " & ProductId & " Order By sort_line"

        Dim dt As DataTable = Getdata.retrive_tablelist(strdt)

        If dt.Rows.Count = 0 Then


            gvOrders.DataSource = ReturnEmptyDataTable()

            gvOrders.DataBind()

            'For Each RequestRow As GridViewRow In gvOrders.Rows
            '    row.Visible = False

            'Next

            gvOrders.FooterRow.Visible = True

            CType(row.FindControl("BtnNewRequest"), LinkButton).Visible = False


        Else



            gvOrders.DataSource = dt

            

            gvOrders.DataBind()

            'For Each RequestRow As GridViewRow In gvOrders.Rows
            '    row.Visible = False
            'Next

            gvOrders.FooterRow.Visible = True

            CType(row.FindControl("BtnNewRequest"), LinkButton).Visible = False

        End If








    End Sub

    Private Function ValidateRequestInsert(strGrid As Object) As Boolean
        ValidateRequestInsert = True

        Dim strFieldName As TextBox

        strFieldName = DirectCast(strGrid.Findcontrol("TxtRequestMaster"), TextBox)

        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("Dados Incorrectos.", DivProdAlert, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            ValidateRequestInsert = False
            Exit Function

        Else
            HighlightFieldError(strFieldName, True)


        End If


        'strFieldName = DirectCast(strGrid.Findcontrol("TxtRequestSlave"), TextBox)

        'If Len(strFieldName.Text) = 0 Then

        '    ShowAlertPanelError("Dados Incorrectos.", DivProdAlert, LbProdAlert)
        '    HighlightFieldError(strFieldName, False)
        '    ValidateRequestInsert = False
        '    Exit Function

        'Else
        '    HighlightFieldError(strFieldName, True)


        'End If

        strFieldName = DirectCast(strGrid.Findcontrol("TxtRequestDescription"), TextBox)

        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("Dados Incorrectos.", DivProdAlert, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            ValidateRequestInsert = False
            Exit Function

        Else
            HighlightFieldError(strFieldName, True)


        End If

        strFieldName = DirectCast(strGrid.Findcontrol("TxtRequestQuantity"), TextBox)

        If Len(strFieldName.Text) = 0 Then

            ShowAlertPanelError("Dados Incorrectos.", DivProdAlert, LbProdAlert)
            HighlightFieldError(strFieldName, False)
            ValidateRequestInsert = False
            Exit Function

        Else
            HighlightFieldError(strFieldName, True)


        End If



    End Function

    Protected Sub GdProdRequests_RowCommand(sender As Object, e As GridViewCommandEventArgs)

        If e.CommandName = "InsertRequest" Then

            Dim row As GridViewRow = DirectCast(DirectCast(sender, GridView).NamingContainer, GridViewRow)
            Dim strProductId As String = GdProdContract.DataKeys(row.RowIndex).Values("TxtOppProductId").ToString()

            Dim strPropId As String = Request.QueryString("PI")
            Dim strOppId As String = Request.QueryString("OI")
            Dim strRequestRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)

            Dim strRequestMaster As TextBox = CType(strRequestRow.FindControl("TxtRequestMaster"), TextBox)
            'Dim strRequestSlave As TextBox = CType(strRequestRow.FindControl("TxtRequestSlave"), TextBox)
            Dim strRequestDescription As TextBox = CType(strRequestRow.FindControl("TxtRequestDescription"), TextBox)
            Dim strRequestQuantity As TextBox = CType(strRequestRow.FindControl("TxtRequestQuantity"), TextBox)

            If ValidateRequestInsert(strRequestRow) = False Then


            Else

                Dim SqlInsert As String


                SqlInsert = "INSERT INTO OrderRequests (TenantId, ProductId, ProposalId, RequestMaster, RequestSlave, RequestTypeId, Description, Quantity, CreateDate, CreateUserId, ModifiedDate, ModifiedUserId) VALUES (" & _
                                Getdata.TenantId & "," & strProductId & "," & strPropId & "," & strRequestMaster.Text & ",0,1,'" & strRequestDescription.Text & "'," & strRequestQuantity.Text & ",GETDATE()," & Getdata.UserId & ",GETDATE()," & Getdata.UserId & ")"


                Getdata.run_sql_query(SqlInsert)

                GdProdContract.DataBind()




            End If



        End If


        If e.CommandName = "Cancel" Then

            Dim row As GridViewRow = DirectCast(DirectCast(sender, GridView).NamingContainer, GridViewRow)
            Dim gvOrders As GridView = TryCast(row.FindControl("GdProdRequests"), GridView)
            gvOrders.EditIndex = -1
            gvOrders.DataBind()
            GdProdContract.DataBind()


        End If



    End Sub

    Protected Sub GdProdRequests_DataBound(sender As Object, e As EventArgs)




        'Dim ProductId As String = GdProdContract.DataKeys(e.Row.RowIndex).Value.ToString()
        'Dim gvOrders As GridView = TryCast(e.Row.FindControl("GdProdRequests"), GridView)

        'Dim strPropRowId As String = GdProdContract.DataKeys(e.Row.RowIndex).Values("PropDetailId").ToString()


        'Dim strdt As String = "SELECT * FROM VwProductDetails WHERE TenantId = " & Getdata.TenantId & " AND ProposalId = " & Request.QueryString("PI") & " AND ProductId = " & ProductId & "Order By sort_line"

        'Dim dt As DataTable = Getdata.retrive_tablelist(strdt)

        'gvOrders.DataSource = dt

        'gvOrders.DataBind()



    End Sub

    Public Function ReturnEmptyDataTable() As DataTable
        Dim dtMenu As New DataTable()
        'declaringa datatable
        Dim dcMenuID As New DataColumn("lineType", GetType(System.String))
        Dim dcMenuID2 As New DataColumn("RequestMaster", GetType(System.String))
        Dim dcMenuID3 As New DataColumn("RequestDescription", GetType(System.String))
        Dim dcMenuID4 As New DataColumn("CustomerName", GetType(System.String))
        Dim dcMenuID5 As New DataColumn("RequestSlave", GetType(System.String))
        Dim dcMenuID6 As New DataColumn("Description", GetType(System.String))
        Dim dcMenuID7 As New DataColumn("Quantity", GetType(System.String))
        Dim dcMenuID8 As New DataColumn("TxtDate", GetType(System.String))

        'creating a column in the same
        'Name of column available in the sql server
        dtMenu.Columns.Add(dcMenuID)
        dtMenu.Columns.Add(dcMenuID2)
        dtMenu.Columns.Add(dcMenuID3)
        dtMenu.Columns.Add(dcMenuID4)
        dtMenu.Columns.Add(dcMenuID5)
        dtMenu.Columns.Add(dcMenuID6)
        dtMenu.Columns.Add(dcMenuID7)
        dtMenu.Columns.Add(dcMenuID8)

        ' Adding column to the datatable

        Dim datatRow As DataRow = dtMenu.NewRow()

        'Inserting a new row,datatable .newrow creates a blank row
        dtMenu.Rows.Add(datatRow)
        'adding row to the datatable
        Return dtMenu
    End Function

    Protected Sub GdProdRequests_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs)


        Dim row As GridViewRow = DirectCast(DirectCast(sender, GridView).NamingContainer, GridViewRow)
        Dim gvOrders As GridView = TryCast(row.FindControl("GdProdRequests"), GridView)
        gvOrders.EditIndex = -1
        gvOrders.DataBind()
        GdProdContract.DataBind()




    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As EventArgs)


        Dim strOppId As String = Request.QueryString("OI")
        Response.Redirect("OpportunityDetails.aspx?OI=" & strOppId)

    End Sub
End Class