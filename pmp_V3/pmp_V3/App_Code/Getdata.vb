﻿Imports Microsoft.VisualBasic
Imports System.Data

Public Class Getdata

    Public Shared Function UserId() As String

        UserId = HttpContext.Current.Session("UserId")

    End Function
    Public Shared Function TenantId() As String

        TenantId = HttpContext.Current.Session("TenantId")

    End Function
    Public Shared Function retrive_tablelist(ByVal strSql As String) As DataTable
        Dim conn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("pmpConnectionString").ToString)
        Dim adapter As Data.SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter
        adapter.SelectCommand = New Data.SqlClient.SqlCommand(strSql, conn)
        conn.Open()
        Dim myDataTable As New DataTable
        Try
            adapter.Fill(myDataTable)

            retrive_tablelist = myDataTable
        Finally
        End Try
        adapter = Nothing
        conn.Close()
        conn = Nothing
        'comm = Nothing
    End Function

    Public Shared Function retrieve_sql_field_value(ByVal strSql As String, ByVal field_name As String) As String
        Dim conn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("pmpConnectionString").ToString)
        Dim comm As New Data.SqlClient.SqlCommand(strSql, conn)
        Dim reader As Data.SqlClient.SqlDataReader
        conn.Open()
        reader = comm.ExecuteReader
        reader.Read()
        If reader.HasRows Then

            If IsDBNull(reader(field_name)) Then

                retrieve_sql_field_value = ""
            Else
                retrieve_sql_field_value = reader(field_name)

            End If
        Else
            retrieve_sql_field_value = "?"
        End If
        reader = Nothing
        conn.Close()
        conn = Nothing
        comm = Nothing

    End Function


    Public Shared Sub run_sql_query(ByVal strSql As String)

        Dim conn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("pmpConnectionString").ToString)
        Dim comm As New Data.SqlClient.SqlCommand(strSql, conn)
        conn.Open()
        comm.ExecuteNonQuery()
        conn.Close()
        conn = Nothing
        comm = Nothing

    End Sub
    Public Shared Function GetDataTable(ByVal query As String) As DataTable

        Dim conn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("pmpConnectionString").ToString)

        Dim comm As New Data.SqlClient.SqlCommand(query, conn)
        Dim adapter As New Data.SqlClient.SqlDataAdapter

        Dim table1 As New DataTable

        adapter.SelectCommand = comm

        conn.Open()

        Try
            adapter.Fill(table1)

        Finally

            conn.Close()

        End Try

        Return table1

    End Function

End Class
