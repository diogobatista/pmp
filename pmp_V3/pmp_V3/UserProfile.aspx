﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ChildMaster.master" CodeBehind="UserProfile.aspx.vb" Inherits="pmp_V3.UserProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Utilizador</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <asp:FormView ID="FormView1" runat="server" DataSourceID="srcUserProfile" Width="100%">
            <ItemTemplate>
                       <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            UserId</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">
                                <%# Eval("Id")%>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            TenantId</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">
                                <%# Eval("TenantId")%>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Conta</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">
                                <%# Eval("TxtTenantName")%>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Utilizador</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">
                                <%# Eval("Name")%>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Activo</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">
                                <%# Eval("IsActive")%>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Data Inicio</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">
                                <%# Eval("TxtCreateDate")%>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
            </ItemTemplate>
        </asp:FormView>

    </div>
    <asp:SqlDataSource ID="srcUserProfile" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [VwUsers] WHERE (([TenantId] = @TenantId) AND ([Id] = @Id))">
        <SelectParameters>
            <asp:SessionParameter Name="TenantId" SessionField="TenantId" Type="Int32" />
            <asp:SessionParameter Name="Id" SessionField="UserId" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>
