﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="pmp_V3._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12">
             <%--<h3 class="page-header">PMP365 | Procurement Management Portal</h3>--%>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="MasterHeader">
        <div class="col-lg-12">
            <div class="col-md-2">
                <div class="well well-sm text-center">
                    <div class="panel-body">
                        <span>
                            <strong>
                                <small>
                                    <asp:Label ID="Label4" runat="server" Text="OPORTUNIDADES" CssClass="h4"></asp:Label>
                                </small>
                            </strong>

                        </span>
                        <br />
                        <span>
                            <strong>

                                <asp:Label ID="lblOpportunities" runat="server" CssClass="h2"></asp:Label>

                            </strong>
                        </span>
                    </div>

                </div>

            </div>
            <div class="col-md-2">
                <div class="well well-sm text-center">
                    <div class="panel-body">

                        <span>
                            <strong>
                                <small>
                                    <asp:Label ID="Label3" runat="server" Text="PROPOSTAS" CssClass="h4"></asp:Label>
                                </small>
                            </strong>

                        </span>
                        <br />
                        <span>
                            <strong>
                                <asp:Label ID="lblProposals" runat="server" CssClass="h2"></asp:Label>

                            </strong>

                        </span>



                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="well well-sm text-center">
                    <div class="panel-body">
                        <span>
                            <strong>
                                <small>
                                    <asp:Label ID="Label2" runat="server" Text="CONTRATOS" CssClass="h4"></asp:Label>
                                </small>
                            </strong>

                        </span>
                        <br />
                        <span>
                            <strong>
                                <asp:Label ID="lblContracts" runat="server" CssClass="h2"></asp:Label>
                            </strong>

                        </span>

                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="well well-sm text-center">
                    <div class="panel-body">

                        <span>
                            <strong>
                                <small>
                                    <asp:Label ID="Label5" runat="server" Text="CLIENTES" CssClass="h4"></asp:Label>
                                </small>
                            </strong>

                        </span>
                        <br />
                        <span>
                            <strong>

                                <asp:Label ID="lblCustomers" runat="server" CssClass="h2"></asp:Label>

                            </strong>

                        </span>





                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="well well-sm text-center">
                    <div class="panel-body">

                        <span>
                            <strong>
                                <small>

                                    <asp:Label ID="Label6" runat="server" Text="PRODUTOS" CssClass="h4"></asp:Label>
                                </small>
                            </strong>

                        </span>
                        <br />
                        <span>
                            <strong>

                                <asp:Label ID="lblProducts" runat="server" CssClass="h2"></asp:Label>

                            </strong>

                        </span>





                    </div>
                </div>

            </div>
            <div class="col-md-2">
                <div class="well well-sm text-center">
                    <div class="panel-body">
                        <span>
                            <strong>
                                <small>

                                    <asp:Label ID="Label7" runat="server" Text="TAREFAS" CssClass="h4"></asp:Label>
                                </small>
                            </strong>

                        </span>
                        <br />
                        <span>
                            <strong>


                                <asp:Label ID="lblTasks" runat="server" CssClass="h2"></asp:Label>

                            </strong>

                        </span>




                    </div>
                </div>
            </div>

            <!-- /.col-lg-12 -->
        </div>
    </div>

    <div class="row">

        <div class="col-md-6">
            <div class="panel panel-default">

                <div class="panel-body" style="min-height: 330px;">

                    <asp:Image ID="Image1" runat="server" Width="100%" ImageUrl="~/images/1.jpg" />


                    <%-- <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                            <span class="sr-only">40% Complete (success)</span>
                        </div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                            <span class="sr-only">20% Complete</span>
                        </div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                            <span class="sr-only">60% Complete (warning)</span>
                        </div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                            <span class="sr-only">80% Complete</span>
                        </div>
                    </div>--%>
                </div>

            </div>



        </div>
        <div class="col-md-6">

            <div class="panel panel-default">

                <div class="panel-body" style="min-height: 330px;">

                    <div class="alert alert-danger" role="alert">
                        <asp:Label ID="Label8" runat="server" CssClass="alert-danger" Text=""><span class="glyphicon glyphicon-ban-circle"></span> 2 propostas requerem aprovação </asp:Label>
                    </div>
                    <div class="alert alert-warning" role="alert">
                        <asp:Label ID="Label1" runat="server" CssClass="alert-warning" Text=""><span class="glyphicon glyphicon-exclamation-sign"></span> 3 contratos a expirar </asp:Label>
                    </div>
                    <div class="alert alert-warning" role="alert">
                        <asp:Label ID="Label9" runat="server" CssClass="alert-warning" Text=""><span class="glyphicon glyphicon-exclamation-sign"></span> 1 Oportunidade sem proposta a expirar </asp:Label>
                    </div>
                    <div class="alert alert-warning" role="alert">
                        <asp:Label ID="Label10" runat="server" CssClass="alert-warning" Text=""><span class="glyphicon glyphicon-exclamation-sign"></span> 1 Cliente aguarda contacto </asp:Label>
                    </div>



                </div>
            </div>

            <%--    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table list-group" DataSourceID="srcGraphTopCustomers" GridLines="None" Width="100%">
                <Columns>
                    <asp:BoundField DataField="CustomerName" HeaderText="CustomerName" SortExpression="CustomerName" />
                    <asp:BoundField DataField="total_line" HeaderText="total_line" ReadOnly="True" SortExpression="total_line" />
                </Columns>
            </asp:GridView>



            <ul class="list-group">
                <li class="list-group-item">
                    <span class="badge">2.500 €</span>
                    C. SAUDE BARCELOS E ESPOSENDE
                </li>
                <li class="list-group-item">
                    <span class="badge">14</span>
                    Cras justo odio
                </li>
                <li class="list-group-item">
                    <span class="badge">14</span>
                    Cras justo odio
                </li>
            </ul>--%>
            <%--<asp:Chart ID="Chart1" runat="server" DataSourceID="srcGraphTopProducts" Height="500px" Width="500px">
                <Series>
                    <asp:Series ChartType="Pie" CustomProperties="PieLabelStyle=Outside" Name="Series1" XValueMember="ProductName" YValueMembers="total_line">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>--%>
        </div>
        <%--        <div class="col-md-3">

            <ul class="list-group">
                <li class="list-group-item">Cras justo odio</li>
                <li class="list-group-item">Dapibus ac facilisis in</li>
                <li class="list-group-item">Morbi leo risus</li>
                <li class="list-group-item">Porta ac consectetur ac</li>
                <li class="list-group-item">Vestibulum at eros</li>
            </ul>



        </div>--%>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
        </div>



    </div>


    <div class="pmpcontainer">



        <asp:SqlDataSource ID="srcGraphTopProducts" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT TOP 5 ProductId, ProductName, sum(quantity*price) as total_line FROM VwProposalProducts WHERE TenantId=@TenantId
    GROUP BY ProductId, ProductName
    ORDER BY total_line DESC">
            <SelectParameters>
                <asp:SessionParameter Name="TenantId" SessionField="TenantId" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="srcGraphTopCustomers" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT CustomerId, CustomerName, sum(quantity*price) as total_line FROM VwProposalProducts  WHERE TenantId=@TenantId
    group by CustomerId, CustomerName
    order by total_line desc">
            <SelectParameters>
                <asp:SessionParameter Name="TenantId" SessionField="TenantId" />
            </SelectParameters>
        </asp:SqlDataSource>

        <%--    <div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Getting Started</h2>
            <p>
                ASP.NET Web Forms lets you build dynamic websites using a familiar drag-and-drop, event-driven model.
            A design surface and hundreds of controls and components let you rapidly build sophisticated, powerful UI-driven sites with data access.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301948">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Get more libraries</h2>
            <p>
                NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301949">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web Hosting</h2>
            <p>
                You can easily find a web hosting company that offers the right mix of features and price for your applications.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301950">Learn more &raquo;</a>
            </p>
        </div>
    </div>--%>
    </div>

</asp:Content>
