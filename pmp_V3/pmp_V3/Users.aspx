﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ChildMaster.master" CodeBehind="Users.aspx.vb" Inherits="pmp_V3.WebForm5" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
      <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Utilizadores</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <telerik:RadGrid ID="GdUsersList" CssClass="table table-striped table-bordered table-hover" runat="server" AutoGenerateColumns="False" DataSourceID="srcUsersList" EnableEmbeddedSkins="False" Width="100%" EnableAjaxSkinRendering="False" EnableEmbeddedBaseStylesheet="False" EnableEmbeddedScripts="False" EnableTheming="True">
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="srcUsersList">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter Id column" HeaderText="Id" ReadOnly="True" SortExpression="Id" UniqueName="Id" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TenantId" DataType="System.Int32" FilterControlAltText="Filter TenantId column" HeaderText="TenantId" SortExpression="TenantId" UniqueName="TenantId" Visible="False">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Name" FilterControlAltText="Filter Name column" HeaderText="Name" SortExpression="Name" UniqueName="Name">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Email" FilterControlAltText="Filter Email column" HeaderText="Email" SortExpression="Email" UniqueName="Email">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Password" FilterControlAltText="Filter Password column" HeaderText="Password" SortExpression="Password" UniqueName="Password">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn DataField="IsActive" DataType="System.Boolean" FilterControlAltText="Filter IsActive column" HeaderText="IsActive" SortExpression="IsActive" UniqueName="IsActive">
                                    </telerik:GridCheckBoxColumn>
                                </Columns>

                                <EditFormSettings>
                                    <EditColumn InsertImageUrl="Update.gif" UpdateImageUrl="Update.gif" CancelImageUrl="Cancel.gif"></EditColumn>
                                </EditFormSettings>
                            </MasterTableView>

                            <FilterMenu EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" EnableEmbeddedBaseStylesheet="False"></FilterMenu>

                            <HeaderContextMenu EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" EnableEmbeddedBaseStylesheet="False"></HeaderContextMenu>
                        </telerik:RadGrid>

                        <asp:SqlDataSource ID="srcUsersList" runat="server" ConnectionString="<%$ ConnectionStrings:pmpConnectionString %>" SelectCommand="SELECT * FROM [Users] WHERE ([TenantId] = @TenantId)">
                            <SelectParameters>
                                <asp:SessionParameter DefaultValue="" Name="TenantId" SessionField="TenantId" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
