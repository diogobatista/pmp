﻿Public Class Dashboard
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then


            gvTopCustomers.Visible = True
            gvTopProducts.Visible = True
        Else

            If ddlCustomers.SelectedValue <> "0" Then
                gvTopCustomers.Visible = False
            Else
                gvTopCustomers.Visible = True
            End If

            If ddlProducts.SelectedValue <> "0" Then
                gvTopProducts.Visible = False
            Else
                gvTopProducts.Visible = True
            End If

        End If




    End Sub

    Private Sub gvMaster_DataBound(sender As Object, e As EventArgs) Handles gvMaster.DataBound
        'gvMaster.Columns.Item(0).Visible = False
        'gvMaster.Columns.Item(1).Visible = False
    End Sub

    Private Sub gvMaster_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvMaster.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim s As SqlDataSource = CType(e.Row.FindControl("srcProductDetails"), SqlDataSource)
            s.SelectParameters(1).DefaultValue = e.Row.Cells(0).Text
            s.SelectParameters(2).DefaultValue = e.Row.Cells(1).Text
        End If
    End Sub


End Class